#pragma once

#include "Resource.h"

// CSelectPricelistDlg dialog

class CSelectPricelistDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectPricelistDlg)

	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CListBox m_wndLBox1;
	CComboBox m_wndCBox1;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	UCFunctions m_recUCFunctions;
	vecUCFunctions m_vecExchangeFunc;

	CTransaction_pricelist m_recPricelist;
	vecTransactionPricelist m_vecPricelists;

	CString m_sSelectedExch;
	CString m_sSelectedPrl;
	int m_nSelectedPrlTypeOf;
public:
	CSelectPricelistDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectPricelistDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	void setPricelists(vecTransactionPricelist &vec)
	{
		m_vecPricelists = vec;
	}

	CString getSelectedExch(void)	{ return m_sSelectedExch; }
	CString getSelectedPrl(void)	{ return m_sSelectedPrl; }
	//Lagt till funktion f�r att h�mta vilken typ av prislista som �r vald.
	int getSelectedPrlTypeOf(void)	{return m_nSelectedPrlTypeOf;}

protected:
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnBnClickedOk();
};
