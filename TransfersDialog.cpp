// TransfersDialog.cpp : implementation file
//

#include "stdafx.h"
#include "TransfersDialog.h"

#include "ResLangFileReader.h"

// CTransfersDialog dialog

IMPLEMENT_DYNAMIC(CTransfersDialog, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CTransfersDialog, CXTResizeDialog)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON1, &CTransfersDialog::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTransfersDialog::OnBnClickedButton2)
	ON_BN_CLICKED(IDOK, &CTransfersDialog::OnBnClickedOk)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_CUSTOM1, OnReportTransValueChanged)
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, IDC_CUSTOM1, OnReportSelChanged)
END_MESSAGE_MAP()

CTransfersDialog::CTransfersDialog(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CTransfersDialog::IDD, pParent)
{
	m_pParent = pParent;
	// Direct to CSpecieDataFormView class; 070810 p�d
	pView = dynamic_cast<CSpecieDataFormView *>(m_pParent);

}

CTransfersDialog::~CTransfersDialog()
{
}

void CTransfersDialog::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP, m_wndGrp);
	DDX_Control(pDX, IDC_CUSTOM1, m_wndReport);
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnAddTransfer);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtnRemoveTransfer);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

int CTransfersDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Need this to show window; 070810 p�d
	HINSTANCE hRCthisDLL = AfxFindResourceHandle(MAKEINTRESOURCE(IDD), RT_DIALOG);     
	m_wndReport.RegisterWindowClass(hRCthisDLL);

	m_wndReport.ShowWindow(SW_NORMAL);

	m_bIsDirty = FALSE;

	return 0;
}

BOOL CTransfersDialog::OnInitDialog()
{
	CString sWindowMsg;
	CXTResizeDialog::OnInitDialog();
	CXTPReportColumn *pCol = NULL;

	m_sLangAbrev = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{	
			sWindowMsg.Format(_T("%s : %s"),(xml->str(IDS_STRING207)),pView->getSpecieName());
			SetWindowText(sWindowMsg);

			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING2070)),50));
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;
			pCol->GetEditOptions()->AddComboButton();

			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING2071)),50));
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;
			pCol->GetEditOptions()->AddComboButton();

			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING2072)),20));
			pCol->SetEditable( TRUE );
			pCol->SetHeaderAlignment( DT_CENTER );
			pCol->SetAlignment( DT_CENTER );
			pCol->SetVisible(FALSE);

			pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING2073)),20));
			pCol->SetEditable( TRUE );
			pCol->SetHeaderAlignment( DT_CENTER );
			pCol->SetAlignment( DT_CENTER );

			populateData();

			m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
			m_wndReport.SetMultipleSelection( FALSE );
			m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
			m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
			m_wndReport.FocusSubItems(TRUE);
			m_wndReport.AllowEdit(TRUE);
			m_wndReport.SetFocus();

			m_wndBtnAddTransfer.SetWindowText((xml->str(IDS_STRING208)));
			m_wndBtnRemoveTransfer.SetWindowText((xml->str(IDS_STRING209)));

			m_wndBtnOK.SetWindowText((xml->str(IDS_STRING210)));
			m_wndBtnCancel.SetWindowText((xml->str(IDS_STRING211)));
		}
		delete xml;
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	
	return TRUE;
}


void CTransfersDialog::populateData(void)
{
	if (m_vecTransactionTemplateTransfers.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionTemplateTransfers.size();i++)
		{
			addConstraintsToReport();
			m_wndReport.AddRecord(new CTransferReportRec(i+1,m_vecTransactionTemplateTransfers[i]));
		}	// for (UINT i = 0;i < m_vecTransactionTemplateTransfers.size();i++)
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}	// if (m_vecTransactionTemplateTransfers.size() > 0)
}

void CTransfersDialog::addConstraintsToReport(int nColumn)
{
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport.GetColumns();
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pView != NULL && pColumns != NULL)
	{
		if(nColumn == -1 || nColumn == 0)	// -1 = all, 0 = source column
		{
			// Add species in Pricelist to Combobox in Column 1; 070509 p�d
			CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_0);
			if (pFromCol != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pFromCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					pCons->RemoveAll();
				}	// if (pCons != NULL)

				// If there's species, add to Specie column in report; 070509 p�d
				if (pView->getAssortmentsInPricelist().size() > 0)
				{
					for (UINT i = 0;i < pView->getAssortmentsInPricelist().size();i++)
					{
						if (pView->getAssortmentsInPricelist()[i].getSpcID() == pView->getSpecieID())
						{
							pFromCol->GetEditOptions()->AddConstraint((pView->getAssortmentsInPricelist()[i].getAssortName()),i);
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
			}	// if (pFromCol != NULL)
		}
		
		if(nColumn == -1 || nColumn == 1)	// -1 = all, 1 = destination column
		{
			CTransferReportRec *pRec = NULL;
			CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
			CString csSource;
			if (pRow)
			{
				pRec = (CTransferReportRec *)pRow->GetRecord();
				csSource = pRec->getColumnText(COLUMN_0);	// get selected assortment in source column
			}

			// Add species in Pricelist to Combobox in Column 1; 070509 p�d
			CXTPReportColumn *pToCol = pColumns->Find(COLUMN_1);
			if (pToCol != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pToCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					pCons->RemoveAll();
				}	// if (pCons != NULL)

				// do not populate the destination column if there are no value in the source column
				if(csSource == _T("")) return;

				// If there's species, add to Specie column in report; 070509 p�d
				if (pView->getAssortmentsInPricelist().size() > 0)
				{
					// loop the assortments to get current source
					BOOL bAddOnlyPulp = FALSE;
					for (UINT i = 0;i < pView->getAssortmentsInPricelist().size();i++)
					{
						if( pView->getAssortmentsInPricelist()[i].getAssortName().CompareNoCase( csSource ) == 0 )	// get current source
						{
							// is the source assortment pulpwood?
							bAddOnlyPulp = (pView->getAssortmentsInPricelist()[i].getPulpType() > 0);
						}
					}

					// loop the assortments to populate the destination dropdown (constraint)
					for (UINT i = 0;i < pView->getAssortmentsInPricelist().size();i++)
					{
						if (pView->getAssortmentsInPricelist()[i].getSpcID() == pView->getSpecieID())
						{
							if( nColumn == -1 )
							{
								pToCol->GetEditOptions()->AddConstraint((pView->getAssortmentsInPricelist()[i].getAssortName()),i);
							}
							else if( pView->getAssortmentsInPricelist()[i].getAssortName().CompareNoCase( csSource ) != 0 )	// only add assortments that are not selected in the source coulumn
							{
								// prevent the user from setting up transfers with the same source and destionation assortment
								// go through all transfers and do not add the destinations that are used already with the same source as the current
								BOOL bDoNotAdd = FALSE;
								for( int nRow = 0; nRow<pRows->GetCount(); nRow++ )
								{
									// check that it is not the same row as current,
									// that the source is the same and
									// that the destination is the same as assortment[i]
									// if so, do not add this destination assortment
									if( nRow != pRow->GetIndex() )
									{
										if( ((CTransferReportRec*)m_wndReport.GetRows()->GetAt(nRow)->GetRecord())->getColumnText(COLUMN_0) == csSource &&
										pView->getAssortmentsInPricelist()[i].getAssortName().CompareNoCase( ((CTransferReportRec*)m_wndReport.GetRows()->GetAt(nRow)->GetRecord())->getColumnText(COLUMN_1) ) == 0 )
										{
											bDoNotAdd = TRUE;
											break;
										}
									}

								}

								// make sure that we do not have pulpwood as source and timber as destination
								if( bAddOnlyPulp && pView->getAssortmentsInPricelist()[i].getPulpType() != 1 && pView->getAssortmentsInPricelist()[i].getPulpType() != 2 )
								{
									bDoNotAdd = TRUE;
								}

								// add assortment to the destination constraints
								if( bDoNotAdd == FALSE )
									pToCol->GetEditOptions()->AddConstraint((pView->getAssortmentsInPricelist()[i].getAssortName()),i);
							}
						}
					}	// for (UINT i = 0;i < m_vecAssortmentsPerSpc.size();i++)
				}	// if (m_vecAssortmentsPerSpc.size() > 0)
			}	// if (pFromCol != NULL)
		}

	}	// if (pView != NULL && pColumns != NULL)
}

int CTransfersDialog::getFromAssortID(LPCTSTR assort_name)
{
	for (UINT i = 0;i < pView->getAssortmentsInPricelist().size(); i++)
	{
		if( pView->getAssortmentsInPricelist()[i].getAssortName() == assort_name)
		{
			return i;
		}
	}

	return -1;
}

int CTransfersDialog::getToAssortID(LPCTSTR assort_name)
{
	for (UINT i = 0;i < pView->getAssortmentsInPricelist().size(); i++)
	{
		if( pView->getAssortmentsInPricelist()[i].getAssortName() == assort_name)
		{
			return i;
		}
	}

	return -1;
}

// CTransfersDialog message handlers
// Add row for Transfers
void CTransfersDialog::OnBnClickedButton1()
{
	addConstraintsToReport();

	m_wndReport.AddRecord(new CTransferReportRec());

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	m_bIsDirty = TRUE;
}

// Remove row for Transfers
void CTransfersDialog::OnBnClickedButton2()
{
	m_bIsDirty = FALSE;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
		if (pRow != NULL)
		{
			pRecs->RemoveAt(pRow->GetIndex());
			m_bIsDirty = TRUE;
		}	
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CTransfersDialog::OnBnClickedOk()
{
	int nFromAssID;
	int nToAssID;
	int nNumOfRecords = m_wndReport.GetRecords()->GetCount();
	CString csFromAs, csToAs;

	m_vecTransactionTemplateTransfers.clear();
	// Check if there's any Transfers made for this specie; 070810 p�d
	if (nNumOfRecords > 0) 
	{
		for (int i = 0;i < nNumOfRecords;i++)
		{
			CTransferReportRec *pRec = (CTransferReportRec*)m_wndReport.GetRecords()->GetAt(i);

			nFromAssID = getFromAssortID(pRec->getColumnText(COLUMN_0));
			nToAssID = getToAssortID(pRec->getColumnText(COLUMN_1));

			// do not add transfer if from or to assortment is empty
			if( nFromAssID != -1 && nToAssID != -1 )
			{
				m_vecTransactionTemplateTransfers.push_back(CTransaction_template_transfers(
										nFromAssID,
										pRec->getColumnText(COLUMN_0),
										nToAssID,
										pRec->getColumnText(COLUMN_1),
										pRec->getColumnFloat(COLUMN_2),
										pRec->getColumnFloat(COLUMN_3),
										_T("")));
			}

		}	// for (int i = 0;i < m_wndReport.GetRecords()->GetCount();i++)
	}	// if (m_wndReport.GetRecords()->GetCount() > 0) 

	CXTResizeDialog::OnOK();
}

void CTransfersDialog::OnReportTransValueChanged(NMHDR * pNotifyStruct, LRESULT * result)
{
	CXTPReportColumn *pCol = NULL;
	CTransferReportRec *pRec = NULL;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		pRec = (CTransferReportRec*)pItemNotify->pRow->GetRecord();
		pCol = pItemNotify->pColumn;
		if (pCol && pRec)
		{
			if (pCol->GetItemIndex() == COLUMN_0)	// source assortment
			{
				// clear the destination column if it is the same assortment as the source
				if( pRec->getColumnText(COLUMN_0) == pRec->getColumnText(COLUMN_1) )
				{
					pRec->setColumnText(COLUMN_1, _T(""));
				}

				// repopulate the destination column
				addConstraintsToReport(1);
			}
		}
	}
}

void CTransfersDialog::OnReportSelChanged(NMHDR* pNMHDR, LRESULT* /*result*/)
{
	// repopulate the constraints for the destination column
	addConstraintsToReport(1);
}
