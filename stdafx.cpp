// stdafx.cpp : source file that includes just the standard includes
// UMTemplates.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "ResLangFileReader.h"

void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

void _DoCollapseExpand(CXTPPropertyGridItems* pItems, ITEMFUNCTIONPTR pFunction)
{
	for (int i = 0; i < pItems->GetCount(); i++)
	{
		CXTPPropertyGridItem* pItem = pItems->GetAt(i);
		if (pItem->HasChilds())
		{
			(pItem->*pFunction)();
			_DoCollapseExpand(pItem->GetChilds(), pFunction);
		}
	}
}

void getTemplateSpecies(LPCTSTR xml_file,vecTransactionSpecies &vec)
{
	TemplateParser *pars = new TemplateParser();
	if (pars != NULL)
	{
		if (pars->LoadFromBuffer(xml_file))
		{
			pars->getSpeciesInTemplateFile(vec);
		}	// if (pars->LoadFromBuffer(xml_file))
		delete pars;
	}
}

void getTemplateFunctionsPerSpcecie(int spc_id,LPCTSTR xml_file,vecUCFunctionList &vec,double *m3fub_m3to,double *m3sk_m3ub_const)
{
	TemplateParser *pars = new TemplateParser();
	if (pars != NULL)
	{
		if (pars->LoadFromBuffer(xml_file))
		{
			pars->getFunctionsForSpecie(spc_id,vec,m3fub_m3to,m3sk_m3ub_const);
		}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		delete pars;
	}
}

void getTemplateMiscFunctionsPerSpcecie(int spc_id,LPCTSTR xml_file,int *qdesc_index,LPTSTR qdesc,
																				double *sk_fub,double *fub_to,double *h25,
																				int *transpdist1,int *transpdist2,double *gcrown)
{
	TemplateParser *pars = new TemplateParser();
	if (pars != NULL)
	{
		if (pars->LoadFromBuffer(xml_file))
		{
			pars->getMiscDataForSpecie(spc_id,qdesc_index,qdesc,sk_fub,fub_to,h25,transpdist1,transpdist2,gcrown);
		}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		delete pars;
	}
}


void getTemplateGrotFunctionsPerSpcecie(int spc_id,LPCTSTR xml_file,int *grot_index,double *percent,double *price,double *cost)
{
	TemplateParser *pars = new TemplateParser();
	if (pars != NULL)
	{
		if (pars->LoadFromBuffer(xml_file))
		{
			pars->getGrotDataForSpecie(spc_id,grot_index,percent,price,cost);
		}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		delete pars;
	}
}

void getTemplateTransfersPerSpcecie(int spc_id,LPCTSTR xml_file,vecTransactionTemplateTransfers &vec)
{
	TemplateParser *pars = new TemplateParser();
	if (pars != NULL)
	{
		if (pars->LoadFromBuffer(xml_file))
		{
			pars->getTransfersForSpecie(spc_id,vec);
		}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		delete pars;
	}
}

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
//					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
//						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}


// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString S;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!db_name.IsEmpty())
					_tcscpy_s(sDBName,127,db_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,table_name))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}

void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}
