//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMTemplates.rc
//
#define IDC_CUSTOM1                     1000
#define IDD_FORMVIEW                    5100
#define IDD_DIALOGBAR                   5101
#define IDD_FORMVIEW1                   5101
#define IDD_FORMVIEW2                   5102
#define IDI_FORMVIEW                    7000
#define IDC_EDIT1                       7001
#define IDC_EDIT2                       7002
#define IDC_EDIT3                       7003
#define IDB_BITMAP1                     7004
#define IDD_DIALOG1                     7005
#define IDB_TAB_ICONS                   7006
#define IDC_GROUP1                      7007
#define IDC_GROUP2                      7008
#define IDC_LBL1                        7009
#define IDC_LBL2                        7010
#define IDC_LBL3                        7011
#define IDC_LBL4                        7012
#define IDC_LBL5                        7013
#define IDD_DIALOG2                     7013
#define IDC_GROUP                       7014
#define IDR_TOOLBAR1                    7014
#define IDC_BUTTON1                     7015
#define IDC_BUTTON2                     7016
#define IDD_DIALOG3                     7016
#define IDC_EDIT4                       7017
#define IDC_COMBO1                      7018
#define IDC_LIST1                       7019
#define IDC_LBL_EXCHANGE1               7020
#define IDC_LBL_EXCH1                   7022
#define IDC_LBL_PRL1                    7023
#define IDC_LBL_INFO1                   7024
#define IDC_LBL_INPDLG                  7025
#define IDC_EDIT_INPDLG                 7026
#define ID_TBBTN_IMPORT                 32771
#define ID_TBBTN_EXPORT                 32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7017
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         7027
#define _APS_NEXT_SYMED_VALUE           7000
#endif
#endif
