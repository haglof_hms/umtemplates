// SpecieDataFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SpecieDataFormView.h"
#include "MDITemplateFormView.h"

#include "ResLangFileReader.h"

#include "TransfersDialog.h"

//////////////////////////////////////////////////////////////////////////
// CInplaceButton

BEGIN_MESSAGE_MAP(CInplaceButton, CXTButton)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
END_MESSAGE_MAP()

void CInplaceButton::OnClicked()
{
	m_pItem->OnValueChanged(m_pItem->GetValue());
	
	SetChecked(m_pItem->m_bValue);
}

//////////////////////////////////////////////////////////////////////////
// CCustomItemButton

CCustomItemButton::CCustomItemButton(CString strCaption, BOOL bValue,CWnd *parent)
	: CXTPPropertyGridItem(strCaption)
{
	m_wndButton.m_pItem = this;
	m_nFlags = 0;
	m_bValue = bValue;
	m_strButtonText = "...";
	m_pParent = parent;


	m_wndFont.CreateFont(12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _T("Tahoma"));
}

BOOL CCustomItemButton::GetBool()
{
	return m_bValue;
}

void CCustomItemButton::SetBool(BOOL bValue)
{
	m_bValue = bValue;

	if (m_wndButton.GetSafeHwnd())
		m_wndButton.SetCheck(bValue);
}

BOOL CCustomItemButton::IsValueChanged()
{
	return m_bValue;
}

void CCustomItemButton::OnValueChanged(CString strValue)
{
	// Direct to CSpecieDataFormView class; 070810 p�d
	CSpecieDataFormView *pView = dynamic_cast<CSpecieDataFormView *>(m_pParent);
	if (pView != NULL)
	{
		pView->handleTransfers();
	}
	CXTPPropertyGridItem::OnValueChanged(strValue);
}

void CCustomItemButton::CreateButton()
{
	if (IsVisible())
	{	
		CRect rc;
//		rc = GetItemRect();
//		rc.DeflateRect( m_nIndent * 14, 0, 0, 1);

		rc = GetValueRect();
//		rc.left = rc.right - rc.Height();

		if (!m_wndButton.m_hWnd)
		{
			m_wndButton.Create(m_strButtonText, WS_CHILD|BS_FLAT|BS_VCENTER|BS_NOTIFY|WS_VISIBLE, rc, (CWnd*)m_pGrid, 100);
			m_wndButton.SetFont(&m_wndFont);
			m_wndButton.SetTheme(new CXTButtonThemeOfficeXP(TRUE));
		}
	
		if (m_wndButton.GetChecked() != m_bValue) 
			m_wndButton.SetChecked(m_bValue);
		
		m_wndButton.MoveWindow(rc);
		m_wndButton.Invalidate(FALSE);
	}
	else
	{
		m_wndButton.DestroyWindow();
	}
}


void CCustomItemButton::SetVisible(BOOL bVisible)
{
	CXTPPropertyGridItem::SetVisible(bVisible);
	CreateButton();
}

void CCustomItemButton::OnIndexChanged()
{
	CreateButton();	
}

BOOL CCustomItemButton::OnDrawItemValue(CDC& /*dc*/, CRect /*rcValue*/)
{
	CreateButton();
	return FALSE;
}

// CSpecieDataFormView

IMPLEMENT_DYNCREATE(CSpecieDataFormView, CXTResizeFormView)


BEGIN_MESSAGE_MAP(CSpecieDataFormView, CXTResizeFormView)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CSpecieDataFormView::CSpecieDataFormView()
	: CXTResizeFormView(CSpecieDataFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bIsDirty = FALSE;
	m_nHgtFunctionIdentifer = -1;
	m_nVolFunctionIdentifer = -1;
	m_nBarkFunctionIdentifer = -1;
	m_nVolUBFunctionIdentifer = -1;
}

CSpecieDataFormView::~CSpecieDataFormView()
{
}

void CSpecieDataFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

int CSpecieDataFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	
		if (!m_wndPropertyGrid.m_hWnd)
		{
			m_wndPropertyGrid.Create(CRect(0, 0, 0, 0), this, ID_PROPERTY_GRID2);
			m_wndPropertyGrid.SetOwner(this);
			m_wndPropertyGrid.ShowHelp( FALSE );
			m_wndPropertyGrid.SetViewDivider(0.35);
			m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		}		

	}

	return 0;
}

void CSpecieDataFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (!m_bInitialized)
	{
	
		CXTPPropertyGridItem* pSettings  = NULL;
		//===============================================================================
		// CATEGORY; Exchange "Utbytesber�kning"
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				m_sYes = xml->str(IDS_STRING204);
				m_sNo = xml->str(IDS_STRING205);

				m_sSpcErrorMsg = xml->str(IDS_STRING110);
				m_sErrHgt = xml->str(IDS_STRING111);
				m_sErrVolume = xml->str(IDS_STRING112);
				m_sErrBark = xml->str(IDS_STRING113);
				m_sErrVolumeUnderBark = xml->str(IDS_STRING114);
				m_sErrM3SkToM3Fub = xml->str(IDS_STRING115);
				m_sErrM3FubToM3To = xml->str(IDS_STRING116);
				m_sErrQualDesc = xml->str(IDS_STRING117);
				m_sErrH25 = xml->str(IDS_STRING118);
				m_sMsgCap = xml->str(IDS_STRING214);

				m_sHeight = xml->str(IDS_STRING2010);
				m_sCalcHgtAs = xml->str(IDS_STRING2011);
				m_sVolume = xml->str(IDS_STRING2012);
				m_sCalcVolAs = xml->str(IDS_STRING2013);
				m_sVolumeUB = xml->str(IDS_STRING2016);
				m_sCalcVolUBAs = xml->str(IDS_STRING2017);
				m_sBark = xml->str(IDS_STRING2014);
				m_sBarkSerie = xml->str(IDS_STRING2015);
				m_sCalcGrotAs = xml->str(IDS_STRING2600);
				m_sGrotPercent = xml->str(IDS_STRING2601);
				m_sGrotPrice = xml->str(IDS_STRING2602);
				m_sGrotCost = xml->str(IDS_STRING2603);

				m_sTransfersAs = xml->str(IDS_STRING2060);

				m_sQDesc = xml->str(IDS_STRING2020);
				
				m_sSk_Fub = xml->str(IDS_STRING2030);
				m_sFub_To = xml->str(IDS_STRING2031);
				m_sSk_Ub = xml->str(IDS_STRING2017);
				m_sAtCoast = xml->str(IDS_STRING2032);
				m_sSouthEast = xml->str(IDS_STRING2033);
				m_sRegion5 = xml->str(IDS_STRING2034);
				m_sPartOfPlot = xml->str(IDS_STRING2035);
				m_sH25 = xml->str(IDS_STRING2036);
				m_sGreenCrown = xml->str(IDS_STRING2037);

				m_sNoTransfers = xml->str(IDS_STRING212);
				m_sIsTransfers = xml->str(IDS_STRING213);

				m_sTransp1 = xml->str(IDS_STRING2210);
				m_sTransp2 = xml->str(IDS_STRING2211);

				m_sarrValidateMsg.Add(xml->str(IDS_STRING2410));
				m_sarrValidateMsg.Add(xml->str(IDS_STRING2411));
				m_sarrValidateMsg.Add(xml->str(IDS_STRING2412));
				m_sarrValidateMsg.Add(xml->str(IDS_STRING2413));
				m_sarrValidateMsg.Add(xml->str(IDS_STRING2414));

				// Get XML-file for this template; 081205 p�d
				CMDITemplateTraktFormView *pView = (CMDITemplateTraktFormView *)getFormViewByID(IDD_FORMVIEW);
				if (pView != NULL)
				{
					if (pView->getActiveTemplate(m_recActiveTmpl))
					{
						m_sTemplateXMLFile = m_recActiveTmpl.getTemplateFile();
					}	// if (pView->getActiveTemplate(m_recActiveTmpl))
				}	// if (pView != NULL)

				double fDummy;
				getTemplateFunctionsPerSpcecie(m_nSpecieID,m_sTemplateXMLFile,m_vecVolUBFuncList_spc,&fDummy,&fDummy);

				if (m_wndPropertyGrid.m_hWnd)
				{

					pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING201)));
					setupTypeOfHeightfunctionsInPropertyGrid(pSettings);

					setupTypeofVolumefunctionsInPropertyGrid(pSettings);

					setupTypeOfBarkfunctionsInPropertyGrid(pSettings);

					setupTypeofVolumefunctionsUBInPropertyGrid(pSettings);

					pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING260)));
					setupTypeofGROTInPropertyGrid(pSettings);

					pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING202)));
					setupQualityDescInPropertyGrid(pSettings);

					pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING206)));
					setupTransfersInPropertyGrid(pSettings);
					
					pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING221)));
					setupTranspDataInPropertyGrid(pSettings);

					pSettings  = m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING203)));
					setupMiscDataInPropertyGrid(pSettings);

				}	// if (m_wndPropertyGrid.m_hWnd)

			}	// if (xml->Load(sLangFN))
			delete xml;
		}	// if (fileExists(sLangFN))

		_DoCollapseExpand(m_wndPropertyGrid.GetCategories(),&CXTPPropertyGridItem::Expand);

		m_bInitialized = TRUE;
	}


}

BOOL CSpecieDataFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CSpecieDataFormView diagnostics

#ifdef _DEBUG
void CSpecieDataFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSpecieDataFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSpecieDataFormView message handlers

void CSpecieDataFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType,cx,cy);

	if (m_wndPropertyGrid.GetSafeHwnd())
	{
		setResize(&m_wndPropertyGrid,0,0,cx,cy);
	}
}

BOOL CSpecieDataFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

LRESULT CSpecieDataFormView::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	if (wParam == XTP_PGN_ITEMVALUE_CHANGED)
	{
		if (pItem->GetID() == ID_HGT_TYPEOF_FUNC_CB)
		{
			setupHeightfunctionsInPropertyGrid(pItem);
		}
		if (pItem->GetID() == ID_VOL_TYPEOF_FUNC_CB)
		{
			setupVolumefunctionsInPropertyGrid(pItem);
		}
		if (pItem->GetID() == ID_BARK_TYPEOF_FUNC_CB)
		{
			setupBarkseriesInPropertyGrid(pItem);
		}
		if (pItem->GetID() == ID_VOL_UB_TYPEOF_FUNC_CB)
		{
			setupVolumefunctionsUBInPropertyGrid(pItem);
		}


		if (pItem->GetID() == ID_M3SK_TO_M3FUB ||
			  pItem->GetID() == ID_M3FUB_TO_M3TO ||
			  pItem->GetID() == ID_ATCOAST ||
			  pItem->GetID() == ID_SOUTHEAST ||
			  pItem->GetID() == ID_REGION5 ||
			  pItem->GetID() == ID_PART_OF_PLOT ||
				pItem->GetID() == ID_TRANSP_DIST1 ||
				pItem->GetID() == ID_TRANSP_DIST2)
		{
		}
	}
	return FALSE;
}

void CSpecieDataFormView::setupTypeOfHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo;
	// get height functions from UCCalculate.dll modules in
	// ..\Module directory; 070413 p�d
	getHeightFunctions(m_vecHgtFunc,m_vecHgtFuncList);
	if (m_vecHgtFunc.size() > 0)
	{
		CXTPPropertyGridItem *pHgtItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sHeight + _T("*")));
		if (pHgtItem != NULL)
		{
			pHgtItem->BindToString(&m_sHgtTypeOfBindStr);

			for (UINT j = 0;j < m_vecHgtFunc.size();j++)
			{
				UCFunctions flist = m_vecHgtFunc[j];
				sInfo.Format(_T("%s"),flist.getName());
				pHgtItem->GetConstraints()->AddConstraint((sInfo),j);
			}	// for (UINT j = 0;j < func_list.size();j++)
			pHgtItem->SetFlags(xtpGridItemHasComboButton);
			pHgtItem->SetConstraintEdit( FALSE );
			pHgtItem->SetID(ID_HGT_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
			setupHeightfunctionsInPropertyGrid(pHgtItem);
		}	// if (pHgtItem != NULL)
	}	// if (m_vecHgtFunc.size() > 0)

}

// This function is called from OnDockingPaneNotify
void CSpecieDataFormView::setupTypeofVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
		CString sInfo;
		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getVolumeFunctions(m_vecVolFunc,m_vecVolFuncList);
		if (m_vecVolFunc.size() > 0)
		{
			CXTPPropertyGridItem *pVolItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sVolume + _T("*")));
			if (pVolItem != NULL)
			{
				pVolItem->BindToString(&m_sVolTypeOfBindStr);

				for (UINT j = 0;j < m_vecVolFunc.size();j++)
				{
					UCFunctions flist = m_vecVolFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pVolItem->GetConstraints()->AddConstraint((sInfo),j);
				}	// for (UINT j = 0;j < func_list.size();j++)
				pVolItem->SetFlags(xtpGridItemHasComboButton);
				pVolItem->SetConstraintEdit( FALSE );
				pVolItem->SetID(ID_VOL_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
				setupVolumefunctionsInPropertyGrid(pVolItem);
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnDockingPaneNotify
void CSpecieDataFormView::setupTypeofVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem)
{
		CString sInfo;
		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getVolumeFunctions_ub(m_vecVolUBFunc,m_vecVolUBFuncList);
		if (m_vecVolUBFunc.size() > 0)
		{
			CXTPPropertyGridItem *pVolUBItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sVolumeUB + _T("*")));
			if (pVolUBItem != NULL)
			{
				pVolUBItem->BindToString(&m_sVolUBTypeOfBindStr);

				for (UINT j = 0;j < m_vecVolUBFunc.size();j++)
				{
					UCFunctions flist = m_vecVolUBFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pVolUBItem->GetConstraints()->AddConstraint((sInfo),j);
				}	// for (UINT j = 0;j < func_list.size();j++)
				pVolUBItem->SetFlags(xtpGridItemHasComboButton);
				pVolUBItem->SetConstraintEdit( FALSE );
				pVolUBItem->SetID(ID_VOL_UB_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
				setupVolumefunctionsUBInPropertyGrid(pVolUBItem);
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)
}


void CSpecieDataFormView::setupTypeofGROTInPropertyGrid(CXTPPropertyGridItem *pItem)
{
		CString sInfo;
		UINT nIndex;
		int nActiveFuncIndex;
		UCFunctions flist;

		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getGROTFunctions(m_vecGROTFunc);
		if (m_vecGROTFunc.size() > 0)
		{
			CXTPPropertyGridItem *pGrotItem = NULL;
			pGrotItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalcGrotAs)));
			if (pGrotItem != NULL)
			{
				pGrotItem->BindToString(&m_sGrotFuncBindStr);

				for (UINT j = 0;j < m_vecGROTFunc.size();j++)
				{
					flist = m_vecGROTFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pGrotItem->GetConstraints()->AddConstraint(sInfo,j);
				}	// for (UINT j = 0;j < func_list.size();j++)
				pGrotItem->SetFlags(xtpGridItemHasComboButton);
				pGrotItem->SetConstraintEdit( FALSE );
				pGrotItem->SetID(ID_GROT_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
			}	// if (pVolItem != NULL)
		}	// if (func_list.size() > 0)

		m_fGrotPercent = 0.0;
		CXTPPropertyGridItemDouble *pGrotPerc = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sGrotPercent),0.0,_T("%.0f")));
		pGrotPerc->SetID(ID_GROT_PERCENT);
		pGrotPerc->BindToDouble(&m_fGrotPercent);

		m_fGrotPrice = 0.0;
		CXTPPropertyGridItemDouble *pGrotPrice = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sGrotPrice),0.0,_T("%.0f")));
		pGrotPrice->SetID(ID_GROT_PRICE);
		pGrotPrice->BindToDouble(&m_fGrotPrice);

		m_fGrotCost = 0.0;
		CXTPPropertyGridItemDouble *pGrotCost = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sGrotCost),0.0,_T("%.0f")));
		pGrotCost->SetID(ID_GROT_COST);
		pGrotCost->BindToDouble(&m_fGrotCost);

}

// This function is called from OnDockingPaneNotify
void CSpecieDataFormView::setupTypeOfBarkfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
		CString sInfo;
		// get volume functions from UCCalculate.dll modules in
		// ..\Module directory; 070413 p�d
		getBarkFunctions(m_vecBarkFunc,m_vecBarkFuncList);
		if (m_vecBarkFunc.size() > 0)
		{
			CXTPPropertyGridItem *pBarkFuncItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sBark + _T("*")));
			if (pBarkFuncItem != NULL)
			{
				pBarkFuncItem->BindToString(&m_sBarkTypeOfBindStr);

				for (UINT j = 0;j < m_vecBarkFunc.size();j++)
				{
					UCFunctions flist = m_vecBarkFunc[j];
					sInfo.Format(_T("%s"),flist.getName());
					pBarkFuncItem->GetConstraints()->AddConstraint((sInfo),j);
				}	// for (UINT j = 0;j < func_list.size();j++)
				pBarkFuncItem->SetFlags(xtpGridItemHasComboButton);
				pBarkFuncItem->SetConstraintEdit( FALSE );
				pBarkFuncItem->SetID(ID_BARK_TYPEOF_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
				setupBarkseriesInPropertyGrid(pBarkFuncItem);
			}	// if (pBarkFuncItem != NULL)
		}	// if (func_list.size() > 0)
}

// This function is called from OnGridNotify
void CSpecieDataFormView::setupHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pHgtItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pHgtItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sCalcHgtAs + _T("*")));
		pHgtItem->BindToString(&m_sHgtBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pHgtItem = pGridChilds->GetAt(0);
			m_sHgtBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pHgtItem != NULL)
	{
		pItem->Expand();
		pHgtItem->GetConstraints()->RemoveAll();

		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecHgtFunc.size())
		{
			m_nHgtFunctionIdentifer = m_vecHgtFunc[nIndex].getIdentifer();
		}
		else if (m_vecTemplateFunctionsList.size() == 4 /* Number of functions: Height,Volume,Bark and VolumeUB */)
		{
			// Height function's first in the function list
			m_nHgtFunctionIdentifer = m_vecTemplateFunctionsList[0].getID();	
		}

		if (m_vecHgtFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecHgtFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecHgtFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == m_nHgtFunctionIdentifer)
				{
					sInfo.Format(_T("%s , %s"),list.getSpcName(),list.getFuncArea());
/*
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}
*/
					pHgtItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pHgtItem->SetFlags(xtpGridItemHasComboButton);
		pHgtItem->SetConstraintEdit( FALSE );
		pHgtItem->SetID(ID_HGT_FUNC_CB);
//		m_sHgtBindStr = getActiveFunction(ID_HGT_FUNC_CB);

	} // 	if (pHgtItem != NULL)

}

// This function is called from OnGridNotify
void CSpecieDataFormView::setupVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pVolItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)

	if (!pItem->HasChilds())
	{
		pVolItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sCalcVolAs + _T("*")));
		pVolItem->BindToString(&m_sVolBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pVolItem = pGridChilds->GetAt(0);
			m_sVolBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pVolItem != NULL)
	{
		pItem->Expand();
		pVolItem->GetConstraints()->RemoveAll();
		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecVolFunc.size())
		{
			m_nVolFunctionIdentifer = m_vecVolFunc[nIndex].getIdentifer();
		}
		else if (m_vecTemplateFunctionsList.size() == 4 /* Number of functions: Height,Volume,Bark and VolumeUB */)
		{
			// Volume function's second in the function list
			m_nVolFunctionIdentifer = m_vecTemplateFunctionsList[1].getID();	
		}

		if (m_vecVolFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecVolFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == m_nVolFunctionIdentifer)
				{
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T("%s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}

					pVolItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pVolItem->SetFlags(xtpGridItemHasComboButton);
		pVolItem->SetConstraintEdit( FALSE );
		pVolItem->SetID(ID_VOL_FUNC_CB);
//		m_sVolBindStr = getActiveFunction(ID_VOL_FUNC_CB);
	} // 	if (pBarkItem != NULL)

}

// This function is called from OnGridNotify
void CSpecieDataFormView::setupVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	BOOL bFuncChanged = FALSE;
	CString sInfo,sTmp;
	CString sActiveFuncName;
	CString sLastActiveFuncName;
	UINT nIndex;
	CXTPPropertyGridItem *pVolUBItem = NULL;
	CMyPropGridItemDouble_validate *pM3Sk_M3Fub = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;
	int nFunctionIdentifer = -1;

	if (pItem == NULL) return;

	if (!pItem->HasChilds())
	{
			pVolUBItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sCalcVolUBAs + _T("*")));
			pVolUBItem->SetID(ID_VOL_UB_FUNC_CB);
			pVolUBItem->BindToString(&m_sVolUBBindStr);
			pVolUBItem->SetFlags(xtpGridItemHasComboButton);
			pVolUBItem->SetConstraintEdit( FALSE );
			
			pM3Sk_M3Fub = (CMyPropGridItemDouble_validate*)pItem->AddChildItem(new CMyPropGridItemDouble_validate(m_sSk_Ub + _T("*"),_T("%.2f"),m_sMsgCap,m_sarrValidateMsg,1.0,VALUE_LE,TRUE,TRUE));
			pM3Sk_M3Fub->SetID(ID_M3SK_TO_M3UB);
			pM3Sk_M3Fub->BindToDouble(&m_fBindToSkUb);
	}

	// Get Volume function set; 081204 p�d
	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL) 
	{
		nIndex = pItemConstraints->GetCurrent();
	}

	if (m_vecVolUBFuncList_spc.size() == 4)
		m_recVolUBFunc = m_vecVolUBFuncList_spc[3];

	// Get identifer of Function selected; 070416 p�d
	if ((nIndex >= 0 && nIndex < m_vecVolUBFunc.size()) || (nIndex == -1))
	{
		if (nIndex >= 0 && nIndex < m_vecVolUBFunc.size())
		{
			sActiveFuncName = pItemConstraints->GetAt(nIndex);
			nFunctionIdentifer = m_vecVolUBFunc[nIndex].getIdentifer();
//			S.Format(_T(">= 0\nnIndex %d\nnFunctionIdentifer %d\nsLastActiveFuncName %s\nsActiveFuncName %s"),nIndex,nFunctionIdentifer,sLastActiveFuncName,sActiveFuncName);
		}
		else
		{
			sActiveFuncName.Empty();
			nFunctionIdentifer = m_recVolUBFunc.getID();
//			S.Format(_T("<= -1\nnIndex %d\nnFunctionIdentifer %d\nm_recVolUBFunc.getID() %d\nsActiveFuncName %s"),nIndex,nFunctionIdentifer,m_recVolUBFunc.getID(),sActiveFuncName);
		}

//		AfxMessageBox(S);

		// Check if user's changed the function; 081205 p�d
//		bFuncChanged =	(nFunctionIdentifer != m_recVolUBFunc.getID());
		bFuncChanged =	(sActiveFuncName.CompareNoCase(sLastActiveFuncName) != 0);

		CXTPPropertyGridItems *pChilds = pItem->GetChilds();
		if (pChilds != NULL)
		{
			if (nFunctionIdentifer == ID_BRANDELS_UB || 
				  nFunctionIdentifer == ID_NASLUNDS_UB ||
					nFunctionIdentifer == ID_HAGBERG_MATERN_UB)
			{
				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL)
				{
					pVolFuncUBFound->GetConstraints()->RemoveAll();
					if (m_vecVolUBFuncList.size() > 0)
					{
						for (UINT i = 0;i < m_vecVolUBFuncList.size();i++)
						{
							sInfo.Empty();
							UCFunctionList list = m_vecVolUBFuncList[i];
							// Check the Function identifer; 070416 p�d
							if (list.getID() == nFunctionIdentifer)
//							if (sActiveFuncName.CompareNoCase(list.getFuncType()) == 0)
							{

								if (_tcslen(list.getSpcName()) > 0)
								{
									sTmp.Format(_T("%s"),list.getSpcName());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncType()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncType());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncArea()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncArea());					
									sInfo += sTmp;
								}

								if (_tcslen(list.getFuncDesc()) > 0)
								{
									sTmp.Format(_T(", %s"),list.getFuncDesc());					
									sInfo += sTmp;
								}

								pVolFuncUBFound->GetConstraints()->AddConstraint((sInfo),i);			
							}	// if (list.getID() == nFunctionIdentifer)
						}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
					}	// if (m_vecVolFuncList.size() > 0)
					//if (bFuncChanged)
					//{
						m_sVolUBBindStr = _T(""); // Empty string (Empty CBox edit); 070413 p�d
						pItem->GetGrid()->Refresh();
					//}

					pVolFuncUBFound->SetHidden(FALSE);
				}	// if (pVolFuncUBFound != NULL)

				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(TRUE);
			}
			else if (nFunctionIdentifer == ID_OMF_FACTOR_UB)
			{
				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(FALSE);

				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL) pVolFuncUBFound->SetHidden(TRUE);
			}
			// If nFunctionIdentifer = -1, it's probably a new Template; 081205 p�d
			else
			{
				CXTPPropertyGridItem *pVolFuncUBFound = pChilds->FindItem(ID_VOL_UB_FUNC_CB);
				if (pVolFuncUBFound != NULL) pVolFuncUBFound->SetHidden(FALSE);

				CXTPPropertyGridItem *pM3SkM3UbItem = pChilds->FindItem(ID_M3SK_TO_M3UB);
				if (pM3SkM3UbItem != NULL) pM3SkM3UbItem->SetHidden(TRUE);
			}

		}
		pItem->Expand();
		sLastActiveFuncName = sActiveFuncName;
	}
}

// This function is called from OnGridNotify
void CSpecieDataFormView::setupBarkseriesInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,sTmp;
	UINT nIndex;
	CXTPPropertyGridItem *pBarkItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pBarkItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sBarkSerie + _T("*")));
		pBarkItem->BindToString(&m_sBarkBindStr);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pBarkItem = pGridChilds->GetAt(0);
			m_sBarkBindStr = ""; // Empty string (Empty CBox edit); 070413 p�d
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pBarkItem != NULL)
	{
		pItem->Expand();
		pBarkItem->GetConstraints()->RemoveAll();

		// Get identifer of Function selected; 070416 p�d
		if (nIndex >= 0 && nIndex < m_vecBarkFunc.size() )
		{
			m_nBarkFunctionIdentifer = m_vecBarkFunc[nIndex].getIdentifer();
		}
		else if (m_vecTemplateFunctionsList.size() == 4 /* Number of functions: Height,Volume,Bark and VolumeUB */)
		{
			// Volume function's third in the function list
			m_nBarkFunctionIdentifer = m_vecTemplateFunctionsList[2].getID();	
		}

		if (m_vecBarkFuncList.size() > 0)
		{
			for (UINT i = 0;i < m_vecBarkFuncList.size();i++)
			{
				sInfo.Empty();
				UCFunctionList list = m_vecBarkFuncList[i];
				// Check the Function identifer; 070416 p�d
				if (list.getID() == m_nBarkFunctionIdentifer)
				{
/*
					if (_tcslen(list.getSpcName()) > 0)
					{
						sTmp.Format(_T("%s"),list.getSpcName());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncType()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncType());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncArea()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncArea());					
						sInfo += sTmp;
					}

					if (_tcslen(list.getFuncDesc()) > 0)
					{
						sTmp.Format(_T(", %s"),list.getFuncDesc());					
						sInfo += sTmp;
					}
*/
					sInfo.Format(_T("%s, %s"),
											list.getSpcName(),
											list.getFuncArea());

					pBarkItem->GetConstraints()->AddConstraint((sInfo),i);			
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pBarkItem->SetFlags(xtpGridItemHasComboButton);
		pBarkItem->SetConstraintEdit( FALSE );
		pBarkItem->SetID(ID_BARK_FUNC_CB);
//		m_sBarkBindStr = getActiveFunction(ID_BARK_FUNC_CB);
	} // 	if (pBarkItem != NULL)

}

void CSpecieDataFormView::setupTransfersInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CCustomItemButton* pCustom = (CCustomItemButton*)pItem->AddChildItem(new CCustomItemButton((m_sTransfersAs), TRUE, this));
	pCustom->BindToString(&m_sBindToTransfers);
	pCustom->SetID(ID_TRANSFERS);
}

void CSpecieDataFormView::setupTranspDataInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItemNumber *pTransp1 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sTransp1)));
	pTransp1->SetID(ID_TRANSP_DIST1);
	pTransp1->BindToNumber(&m_nBindToTranspDist1);

	CXTPPropertyGridItemNumber *pTransp2 = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber((m_sTransp2)));
	pTransp2->SetID(ID_TRANSP_DIST2);
	pTransp2->BindToNumber(&m_nBindToTranspDist2);

}

void CSpecieDataFormView::setupMiscDataInPropertyGrid(CXTPPropertyGridItem *pItem)
{

//	CXTPPropertyGridItemDouble *pSkFub = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sSk_Fub + _T("*"),0.0,_T("%.2f")));
	// Added 081126 p�d
/*
	CXTPPropertyGridItemDouble *pSkFub = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CMyPropGridItemDouble_validate(m_sSk_Fub + _T("*"),_T("%.2f"),
																																																												   m_sMsgCap,m_sarrValidateMsg,1.0,VALUE_LE));
	pSkFub->SetID(ID_M3SK_TO_M3FUB);
	pSkFub->BindToDouble(&m_fBindToSkFub);
*/
	m_fBindToSkFub = 0.0;
//	CXTPPropertyGridItemDouble *pFubTo = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sFub_To + _T("*"),0.0,_T("%.2f")));
	// Added 081126 p�d
	CXTPPropertyGridItemDouble *pFubTo = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CMyPropGridItemDouble_validate(m_sFub_To + _T("*"),_T("%.2f"),
	m_sMsgCap,m_sarrValidateMsg,1.0,VALUE_LE,TRUE,TRUE));
	pFubTo->SetID(ID_M3FUB_TO_M3TO);
	pFubTo->BindToDouble(&m_fBindToFubTo);

	CXTPPropertyGridItemDouble *pH25 = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sH25 + _T("*"),0.0,_T("%.1f")));
	pH25->SetID(ID_H25);
	pH25->BindToDouble(&m_fBindToH25);

	CXTPPropertyGridItemDouble *pGreenCrown = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sGreenCrown,0.0,_T("%.0f")));
	pGreenCrown->SetID(ID_GREENCROWN);
	pGreenCrown->BindToDouble(&m_fBindToGreenCrown);

}

void CSpecieDataFormView::setupQualityDescInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	int nQDescIndex = -1;
/*
	CString S;
	S.Format("CSpecieDataFormView::setupQualityDescInPropertyGrid\nm_listQDesc.GetCount() %d",
		m_listQDesc.GetCount());
	AfxMessageBox(S);
*/
	if (m_listQDesc.GetCount() > 0)
	{
		CXTPPropertyGridItem *pQDescItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sQDesc + _T("*")));
		if (pQDescItem != NULL)
		{
			pQDescItem->BindToString(&m_sBindToQualDesc);

			for (int i = 0;i < m_listQDesc.GetCount();i++)
			{
					pQDescItem->GetConstraints()->AddConstraint((m_listQDesc[i]),i);
			}	// for (UINT j = 0;j < func_list.size();j++)
			pQDescItem->SetFlags(xtpGridItemHasComboButton);
			pQDescItem->SetConstraintEdit( FALSE );
			pQDescItem->SetID(ID_QDESC_FUNC_CB);	// Set ID in PropertyGrid; 070521 p�d
		}
		// Make sure we're inside the array; 070521 p�d
		if (nQDescIndex >= 0 && nQDescIndex < m_listQDesc.GetCount())
		{
			m_sBindToQualDesc = m_listQDesc[nQDescIndex];
		}
	}
}

int CSpecieDataFormView::getGrotIndex()
{
	if (m_vecGROTFunc.size() > 0)
	{
		for (UINT i = 0;i < m_vecGROTFunc.size();i++)
		{
			if (m_sGrotFuncBindStr.Compare(m_vecGROTFunc[i].getName()) == 0)
			{
				return m_vecGROTFunc[i].getIdentifer();	// Function identifer
			}	//
		}	// 
	}	// 
	return 0;	// No description found; 100317 p�d
}

void CSpecieDataFormView::setGrotIndex(int index)
{
	if (m_vecGROTFunc.size() > 0)
	{
		for (UINT i = 0;i < m_vecGROTFunc.size();i++)
		{
			if (index == m_vecGROTFunc[i].getIdentifer())
			{
				m_sGrotFuncBindStr = m_vecGROTFunc[i].getName();
				return;
			}	//
		}	// 
	}	// 
}


int CSpecieDataFormView::getQDescIndex(LPCTSTR v)
{
	if (m_listQDesc.GetCount() > 0)
	{
		for (int i = 0;i < m_listQDesc.GetCount();i++)
		{
			if (_tcscmp(v,m_listQDesc[i]) == 0)
			{
				return i;	// <= Index of Qualitydescription; 070906 p�d
			}	// if (_tcscmp(v,m_listQDesc[i]) == 0)
		}	// for (int i = 0;i < m_listQDesc.GetCount();i++)
	}	// if (m_listQDesc.GetCount() > 0)
	return -1;	// No description found; 070906 p�d
}

void CSpecieDataFormView::getHeightFunction(UCFunctions &hgt_func,UCFunctionList &func_list)
{	
	CString sInfo;
	UINT i,j;

	if (m_vecHgtFunc.size() > 0)
	{
		for (i = 0;i < m_vecHgtFunc.size();i++)
		{
			UCFunctions func = m_vecHgtFunc[i];
			if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
			{
				hgt_func = func;
				break;
			}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
		}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)

		if (m_vecHgtFuncList.size() > 0)
		{
			for (j = 0;j < m_vecHgtFuncList.size();j++)
			{
				UCFunctionList flist = m_vecHgtFuncList[j];
				sInfo.Format(_T("%s , %s"),flist.getSpcName(),flist.getFuncArea());

				if (m_sHgtBindStr.Compare(sInfo) == 0)
				{
					func_list = flist;
					break;
				}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
			}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)
		}	// if (m_vecHgtFuncList.size() > 0)
	}	// if (m_vecHgtFunc.size() > 0)
}

void CSpecieDataFormView::getVolumeFunction(UCFunctions &vol_func,UCFunctionList &func_list)
{	

	CString sInfo;
	UINT i,j;

	if (m_vecVolFunc.size() > 0)
	{
		for (i = 0;i < m_vecVolFunc.size();i++)
		{
			UCFunctions func = m_vecVolFunc[i];
			if (m_sVolTypeOfBindStr.Compare(func.getName()) == 0)
			{
				vol_func = func;
				break;
			}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
		}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)


		if (m_vecVolFuncList.size() > 0)
		{
			for (j = 0;j < m_vecVolFuncList.size();j++)
			{
				UCFunctionList flist = m_vecVolFuncList[j];
				sInfo.Format(_T("%s, %s, %s, %s"),
											flist.getSpcName(),
											flist.getFuncType(),
											flist.getFuncArea(),
											flist.getFuncDesc());

				if (m_sVolBindStr.Compare(sInfo) == 0)
				{
					func_list = flist;
					break;
				}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
			}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)
		}	// if (m_vecVolFuncList.size() > 0)
	}	// if (m_vecVolFunc.size() > 0)
}


void CSpecieDataFormView::getBarkFunction(UCFunctions &bark_func,UCFunctionList &func_list)
{	
	CString sInfo;
	UINT i,j;

	if (m_vecBarkFunc.size() > 0)
	{
		for (i = 0;i < m_vecBarkFunc.size();i++)
		{
			UCFunctions func = m_vecBarkFunc[i];
			if (m_sBarkTypeOfBindStr.Compare(func.getName()) == 0)
			{
				bark_func = func;
				break;
			}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
		}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)


		if (m_vecBarkFuncList.size() > 0)
		{
			for (j = 0;j < m_vecBarkFuncList.size();j++)
			{
				UCFunctionList flist = m_vecBarkFuncList[j];
				sInfo.Format(_T("%s, %s"),
											flist.getSpcName(),
											flist.getFuncArea());
				if (m_sBarkBindStr.Compare(sInfo) == 0)
				{
					func_list = flist;
					break;
				}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
			}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)
		}	// if (m_vecBarkFuncList.size() > 0)
	}	// if (m_vecBarkFunc.size() > 0)
}

void CSpecieDataFormView::getVolumeUBFunction(UCFunctions &vol_func,UCFunctionList &func_list,double *m3sk_m3ub)
{	
	int nFuncIdentifer;
	CString sInfo,S;
	UINT i,j;
	*m3sk_m3ub = 0.0;

	if (m_vecVolUBFunc.size() > 0)
	{
		for (i = 0;i < m_vecVolUBFunc.size();i++)
		{
			UCFunctions func = m_vecVolUBFunc[i];
			if (m_sVolUBTypeOfBindStr.Compare(func.getName()) == 0)
			{
				nFuncIdentifer = func.getIdentifer();
				vol_func = func;
				break;
			}	// if (m_sVolUBTypeOfBindStr.Compare(func.getName()) == 0)
		}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)


		if (m_vecVolUBFuncList.size() > 0)
		{
			for (j = 0;j < m_vecVolUBFuncList.size();j++)
			{
				UCFunctionList flist = m_vecVolUBFuncList[j];
				sInfo.Format(_T("%s, %s, %s, %s"),
											flist.getSpcName(),
											flist.getFuncType(),
											flist.getFuncArea(),
											flist.getFuncDesc());
				if (nFuncIdentifer != ID_OMF_FACTOR_UB)
				{
					if (m_sVolUBBindStr.Compare(sInfo) == 0)
					{
						*m3sk_m3ub = 0.0;
						func_list = flist;
						break;
					}	// if (m_sHgtTypeOfBindStr.Compare(func.getName()) == 0)
				}
				else if (flist.getID() == ID_OMF_FACTOR_UB)
				{
						*m3sk_m3ub = m_fBindToSkUb;
						func_list = flist;
						break;
				}
			}	// for (nIndex = 0;nIndex < m_vecHgtFunc.size();nIndex++)
		}	// if (m_vecVolFuncList.size() > 0)
	}	// if (m_vecVolFunc.size() > 0)

}
/*
// Get data from m_vecTransactionTraktSetSpc; 070504 p�d
BOOL CSpecieDataFormView::getActiveTypeOfFunction(int which,CString &s,UINT* index,int* identifer)
{
	BOOL bReturn = FALSE;
	int nFuncID;
	UINT j;
	CTransaction_trakt_set_spc recSetSpc;

	if (m_vecTransactionTraktSetSpc.size() > 0)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		CTransaction_trakt_data *recTData = getActiveTraktData();
		// First find id's and indexes; 070504 p�d
		for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
		{
			recSetSpc = m_vecTransactionTraktSetSpc[i];
			// Find Settings info for This trakt; 070504 p�d
			if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
				  recSetSpc.getTSetspcTraktID() == recTData->getTDataTraktID(),
					recSetSpc.getTSetspcID() == recTData->getSpecieID())
			{
				// Find information from m_vecHgtFuncList, based on
				// nFuncID,nSpcID and nFuncIndexID; 070504 p�d
				if (which == ID_HGT_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getHgtFuncID();
					for (j = 0;j < m_vecHgtFunc.size();j++)
					{
						UCFunctions funcs = m_vecHgtFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecHgtFunc.size();j++)
				}
				else if (which == ID_VOL_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolFuncID();
					for (j = 0;j < m_vecVolFunc.size();j++)
					{
						UCFunctions funcs = m_vecVolFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
				else if (which == ID_BARK_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getBarkFuncID();
					for (j = 0;j < m_vecBarkFunc.size();j++)
					{
						UCFunctions funcs = m_vecBarkFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecBarkFunc.size();j++)
				}
				else if (which == ID_VOL_UB_TYPEOF_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolUBFuncID();
					for (j = 0;j < m_vecVolUBFunc.size();j++)
					{
						UCFunctions funcs = m_vecVolUBFunc[j];
						// Try to find typeof heightfunction
						if (nFuncID == funcs.getIdentifer())
						{
							s = funcs.getName();
							*index = j;
							*identifer = funcs.getIdentifer();
							bReturn = TRUE;
							break;
						}	// if (nFuncID == funcs.getIdentifer())
					}	// for (j = 0;j < m_vecVolFunc.size();j++)
				}
			}	// if (recSetSpc.getTSetspcDataID() == recTData->getTDataID() &&
		} // for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
	}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)

	return bReturn;

}

CString CSpecieDataFormView::getActiveFunction(int which)
{
	CString sInfo;
	UINT j;
	int nSpcID;
	int nFuncID;
	int nFuncIndex;
	BOOL bFound = FALSE;
	CTransaction_trakt_set_spc recSetSpc;

	if (m_vecTransactionTraktSetSpc.size() > 0)
	{
		// Get active trakt record; holds information on trakt-id; 070430 p�d
		// getActiveTraktDataOnClick() = Get data from record m_recTraktDataOnClick set
		// in CPageTwoFormView on OnReportItemClick.
		// If this fails, we try to get data deom m_recTraktDataActive, set on
		// populateData in CPageTwoFormView; 081128 p�d
		CTransaction_trakt_data *pTData = getActiveTraktDataOnClick();
		if (pTData == NULL)
			pTData = getActiveTraktData();
		// First find id's and indexes; 070504 p�d
		for (UINT i = 0;i < m_vecTransactionTraktSetSpc.size();i++)
		{
			recSetSpc = m_vecTransactionTraktSetSpc[i];
			// Find Settings info for This trakt; 070504 p�d
			if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
				  recSetSpc.getTSetspcTraktID() == pTData->getTDataTraktID(),
					recSetSpc.getTSetspcID() == pTData->getSpecieID())
			{
				// Find information from m_vecHgtFuncList, based on
				// nFuncID,nSpcID and nFuncIndexID; 070504 p�d
				if (which == ID_HGT_FUNC_CB)
				{
					nFuncID	= recSetSpc.getHgtFuncID();
					nSpcID	= recSetSpc.getHgtFuncSpcID();
					nFuncIndex = recSetSpc.getHgtFuncIndex();
					for (j = 0;j < m_vecHgtFuncList.size();j++)
					{
						UCFunctionList flist = m_vecHgtFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s , %s"),flist.getSpcName(),flist.getFuncArea());
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_VOL_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolFuncID();
					nSpcID	= recSetSpc.getVolFuncSpcID();
					nFuncIndex = recSetSpc.getVolFuncIndex();
					for (j = 0;j < m_vecVolFuncList.size();j++)
					{
						UCFunctionList flist = m_vecVolFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s, %s, %s, %s"),
											flist.getSpcName(),
											flist.getFuncType(),
											flist.getFuncArea(),
											flist.getFuncDesc());
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_BARK_FUNC_CB)
				{
					nFuncID	= recSetSpc.getBarkFuncID();
					nSpcID	= recSetSpc.getBarkFuncSpcID();
					nFuncIndex = recSetSpc.getBarkFuncIndex();
					for (j = 0;j < m_vecBarkFuncList.size();j++)
					{
						UCFunctionList flist = m_vecBarkFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s, %s"),
											flist.getSpcName(),
											flist.getFuncArea());
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)
				else if (which == ID_VOL_UB_FUNC_CB)
				{
					nFuncID	= recSetSpc.getVolUBFuncID();
					nSpcID	= recSetSpc.getVolUBFuncSpcID();
					nFuncIndex = recSetSpc.getVolUBFuncIndex();
					for (j = 0;j < m_vecVolUBFuncList.size();j++)
					{
						UCFunctionList flist = m_vecVolUBFuncList[j];
						// Try to find typeof heightfunction
						if (flist.getID() == nFuncID &&
								flist.getSpcID() == nSpcID &&
								flist.getIndex() == nFuncIndex)
						{
							sInfo.Format(_T("%s, %s, %s, %s"),
											flist.getSpcName(),
											flist.getFuncType(),
											flist.getFuncArea(),
											flist.getFuncDesc());
							return sInfo;
						}	// if (flist.getID() == nFuncID &&
					}	// for (UINT j = 0;j < m_vecHgtFuncList.size();j++)
				} // if (which == ID_HGT_TYPEOF_FUNC_CB)



			}	// if (recSetSpc.getTSetspcDataID() == pTData->getTDataID() &&
		}	// for (UINT i = 0;i < if (m_vecTransactionTraktSetSpc.size();i++)
	
	}	// if (m_vecTransactionTraktSetSpc.size() > 0)
	return _T("");
}
*/
// Open dialog td edit transfers for specie; 070831 p�d
void CSpecieDataFormView::handleTransfers(void)
{
	m_bIsDirty = FALSE;

	CTransfersDialog *dlg = new CTransfersDialog(this);
	dlg->setTemplateTransfers(m_vecTransactionTemplateTransfers);
	if (dlg->DoModal() == IDOK)
	{
		setTemplateTransfers(dlg->getTemplateTransfers());
		m_bIsDirty = TRUE;
	}
	delete dlg;
}

void CSpecieDataFormView::setTemplateTransfers(vecTransactionTemplateTransfers &v)
{
	CString S;

	m_vecTransactionTemplateTransfers = v;

	S.Format(_T("%s (%d)"),m_sIsTransfers,m_vecTransactionTemplateTransfers.size());
	CCustomItemButton *pItem = (CCustomItemButton*)m_wndPropertyGrid.FindItem(ID_TRANSFERS);
	if (pItem != NULL)
	{
		pItem->setButtonCaption(S);
	}

	m_wndPropertyGrid.Refresh();

}

// Check ALL items in Propertygrid for changes; 070824 p�d
BOOL CSpecieDataFormView::getIsDirty(void)
{
	if (m_bIsDirty || m_wndPropertyGrid.isDirty())
		return TRUE;
	else
		return FALSE;
}

// Check ALL items in Propertygrid for changes; 070824 p�d
void CSpecieDataFormView::resetIsDirty(void)
{
	m_bIsDirty = FALSE;
	m_wndPropertyGrid.resetIsDirty();
}

BOOL CSpecieDataFormView::isOKToClose(LPCTSTR spc,CStringArray &arr)
{
	CString sErrMsg,S;
	if (m_sHgtTypeOfBindStr.IsEmpty() || m_sHgtBindStr.IsEmpty()) 
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrHgt);
		arr.Add(sErrMsg);
	}

	if (m_sVolTypeOfBindStr.IsEmpty() || m_sVolBindStr.IsEmpty())
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrVolume);
		arr.Add(sErrMsg);
	}

	if (m_sBarkTypeOfBindStr.IsEmpty() || m_sBarkBindStr.IsEmpty()) 
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrBark);
		arr.Add(sErrMsg);
	}

	if (m_sVolUBTypeOfBindStr.IsEmpty() &&
		  m_sVolUBBindStr.IsEmpty() && 
			m_fBindToSkUb == 0.0)
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrVolumeUnderBark);
		arr.Add(sErrMsg);
	}
/*	Not used; 090602 p�d
	if (m_fBindToSkFub == 0.0)
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrM3SkToM3Fub);
		arr.Add(sErrMsg);
	}
*/
	if (m_fBindToFubTo == 0.0) 
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrM3FubToM3To);
		arr.Add(sErrMsg);
	}

	// We only need to check if there's a quality-description
	// set for "Estimateprislista"; 080304 p�d
	if (m_listQDesc.GetCount() > 0)
	{
		if (m_sBindToQualDesc.IsEmpty()) 
		{
			sErrMsg.Format(_T("%s : %s"),spc,m_sErrQualDesc);
			arr.Add(sErrMsg);
		}
	}

	if (m_fBindToH25 == 0.0)
	{
		sErrMsg.Format(_T("%s : %s"),spc,m_sErrH25);
		arr.Add(sErrMsg);
	}

	return (arr.GetCount() == 0);
}
