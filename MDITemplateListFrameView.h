#pragma once


/////////////////////////////////////////////////////////////////////////////
// CTemplateListReportDataRec

class CTemplateListReportDataRec : public CXTPReportRecord
{
	int m_nID;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CTemplateListReportDataRec(void)
	{
		m_nID	= -1;	
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTemplateListReportDataRec(int id,LPCTSTR tmpl_name,LPCTSTR done_by,LPCTSTR date)
	{
		m_nID	= id;
		AddItem(new CTextItem(tmpl_name));
		AddItem(new CTextItem(done_by));
		AddItem(new CTextItem(date));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};



// CTemplateListFrameView form view

class CTemplateListFrameView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTemplateListFrameView)

	//private:
	BOOL m_bInitialized;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	vecTransactionTemplate m_vecTransactionTemplate;
	void getTemplatesFromDB(void);

	int m_nDBIndex;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CMyReportCtrl m_wndTemplates;
//	CXTPReportControl m_wndPricelistsList;

	// Methods
	BOOL setupReport(void);

	BOOL populateData(void);
	
	void setupDoPopulate(void);
public:
	CTemplateListFrameView();           // protected constructor used by dynamic creation
	virtual ~CTemplateListFrameView();

	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnReportClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


