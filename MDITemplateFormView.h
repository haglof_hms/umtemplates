#pragma once

#include "Resource.h"

// CMDITemplateTraktFormView form view

class CMDITemplateTraktFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDITemplateTraktFormView)

	enum enumPRL_OPEN { SAME_AS_BEFORE,NOT_THE_SAME };

	enumPRL_OPEN m_enumPrlOpen;
// private:
	BOOL m_bInitialized;
	BOOL m_bInitialized2;
	BOOL m_bIsDirty;
	BOOL m_bEnableWindows;

	int m_nPriceListTypeOf;		// E.g. 1 = "Prislista", 2 = "Medelprislista"
	int m_nStatus;

	enumTemplateState m_enumTemplateState;

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;

	CComboBox m_wndCBox1;

	CMyPropertyGrid m_wndPropertyGrid;

	CString m_sMsgCap;
	CString m_sMsgDelTemplate;
	CString m_sMsgTmplNameMissing1;
	CString m_sMsgTmplNameMissing2;
	CString m_sMsgDataMissing;
	CString m_sMsgDuplicateTemplName;
	CString m_sMsgDontSaveTemplate;
	CString m_sMsgWrongSIH100Entry;
	CString m_sMsgDiameterclassErr;
	CString m_sMsgNotToBeSaved;
	CString m_sMsgCantFindPricelist;
	
	CString m_sMsgCharError;
	
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sDiamClass;

	CString m_sExchange;
	CString m_sPricelist;
	CString m_sQualdesc;
	CString m_sHgtOverSea;
	CString m_sLatitude;
	CString m_sLongitude;
	CString m_sGrowthArea;
	CString m_sSI_H100;
	CString m_sCostTmpl;

	CString m_sAtCoast;
	CString m_sSouthEast;
	CString m_sRegion5;
	CString m_sPartOfPlot;
	CString m_sSIH100_pine;

	CString m_sYes;
	CString m_sNo;

	CStringArray m_listQDesc;
	CStringArray m_arrValidateMsg;

	void setupPropertyGrid(void);

	void setupMiscDataInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupSoderbergsDataInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctionList m_vecTemplateFuncList;

	vecUCFunctions m_vecExchangeFunc;
	vecTransactionSpecies m_vecTransactionSpecies;
	vecTransactionPricelist m_vecTransactionPricelist;
	vecTransactionAssort m_vecAssortmentsPerSpc;
	void setupTypeOfExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	
	void getTemplatePricelistTypeOf(void);

	void setTabsForSpeciesInSelectedPricelist(void);
	void setFunctionsInTabsForSpeciesForSelectedPricelist(void);

	void getPricelistsInDB(void);

	vecTransaction_costtempl m_vecTransaction_costtempl;
	void getCostTmplInDB(void);
	int getCostTmplID(LPCTSTR cost_tmpl_name);

	CTransaction_template m_recActiveTemplate;
	vecTransactionTemplate m_vecTransactionTemplate;
	CString m_sTemplateXMLFile;
	BOOL createTemplateXMLFile(void);
	
	BOOL addNewTemplate(void);

	void getTemplatesFromDB(void);
	BOOL saveTemplateToDB(short action);
	BOOL removeTemplateFromDB(BOOL show_msg = TRUE);

	// Bind to ...
	CString m_sBindToExchPricelistStr;
	CString m_sBindToPricelistSelected;
	//Lagt till en variabel f�r att �ven h�lla reda p� typen av prislista 20120112 J� Bug #2679	
	//int m_nBindToPricelistSelectedTypeOf;
	//Lagt till en koll om prislistan i best�ndsmallen finns kvar i dbvariabel 20120112 J� Bug #2679	
	BOOL getPricelist(CString csPrlName,int nPrlTypeOf);

	double m_fBindToDiamClass;
	long m_nBindToHgtOverSea;
	long m_nBindToLatitude;
	long m_nBindToLongitude;
	CString m_sBindToGrowthArea;
	CString m_sBindToSIH100;
	CString m_sBindToCostTmpl;

	BOOL m_bBindToIsAtCoast;
	BOOL m_bBindToIsSouthEast;
	BOOL m_bBindToIsRegion5;
	BOOL m_bBindToIsPartOfPlot;
	CString m_sBindToSIH100_pine;	// "Tall S�derbergs funktion"

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	//Lagt till variabel f�r att h�lla reda p� om data skall kollas och sparas eller ej
	//J� 20111017 Bug #2443 
	BOOL m_bDoSave;
	int m_nOldStatus;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL getIsDirty(void);
	void resetIsDirty(void);

	void clearData(void);

	int m_nDBIndex;
	void populateData(UINT idx);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int spc_id, LPCTSTR spc_name, int nIcon);

	// Looks for items not entered in Specie pages; 071121 p�d
	BOOL isSpeciePagesOKToClose(CStringArray &arr);

	BOOL isTemplateNameOK();


	void setLockTemplateFormView(BOOL lock)
	{
		//m_wndPropertyGrid.EnableWindow(!lock); 
		CXTPPropertyGridItem *pItem = NULL;
		if ((pItem = m_wndPropertyGrid.FindItem(ID_EXCH_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_PRL_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);

		if ((pItem = m_wndPropertyGrid.FindItem(ID_MISC_TEMPL_DCLS)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_MISC_TEMPL_HGT_OVER_SEA)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_MISC_TEMPL_LATITUDE)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_MISC_TEMPL_LONGITUDE)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_MISC_TEMPL_SI_H100)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_MISC_TEMPL_COSTS)) != NULL) pItem->SetReadOnly(lock);

		if ((pItem = m_wndPropertyGrid.FindItem(ID_ATCOAST)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_SOUTHEAST)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_REGION5)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_PART_OF_PLOT)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_SIH100_PINE)) != NULL) pItem->SetReadOnly(lock);
	}

protected:
	CMDITemplateTraktFormView();           // protected constructor used by dynamic creation
	virtual ~CMDITemplateTraktFormView();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

public:

	// Need to be PUBLIC
	BOOL isHasDataChanged(short action);

	CString getTemplateName(void)
	{
		return m_wndEdit1.getText();
	}

	BOOL isOKToClose(void)
	{
		if (m_nStatus == ID_TEMPLATE_TRAKT_DEVELOPMENT)
		{
			return TRUE;
		}
		
		if (m_nStatus == ID_TEMPLATE_TRAKT)
		{
			if (m_sBindToExchPricelistStr.IsEmpty()) return FALSE;
			else if (m_sBindToPricelistSelected.IsEmpty()) return FALSE;
			else if (m_fBindToDiamClass == 0.0) return FALSE;
			else if (m_nBindToHgtOverSea == 0) return FALSE;
			else if (m_nBindToLatitude == 0) return FALSE;
			//else if (m_sBindToGrowthArea.IsEmpty()) return FALSE; NOT USED 080508 p�d
			else if (m_sBindToSIH100.IsEmpty()) return FALSE;
			else if (m_sBindToCostTmpl.IsEmpty()) return FALSE;
		}

		return TRUE;
	}

	// Don't do event; OnEnKillfocusEdit1; 071031 p�d
	void resetInitialized2(void)
	{
		m_bInitialized2 = FALSE;
		m_enumTemplateState = TEMPLATE_OPEN;
	}

	int getDBIndex(void)
	{
		return m_nDBIndex;
	}

	void doPouplate(UINT idx);

	void doPouplateFromLastEntry(void);

	void doSaveAndPouplateLastEntry(void);

	void setEnbaleWindows(BOOL enabled)
	{
		m_bEnableWindows = enabled;
		m_wndEdit1.EnableWindow(m_bEnableWindows);
		m_wndEdit1.SetReadOnly(!m_bEnableWindows);
		m_wndEdit2.EnableWindow(m_bEnableWindows);
		m_wndEdit2.SetReadOnly(!m_bEnableWindows);
		m_wndEdit3.EnableWindow(m_bEnableWindows);
		m_wndEdit3.SetReadOnly(!m_bEnableWindows);
		m_wndEdit4.EnableWindow(m_bEnableWindows);
		m_wndEdit4.SetReadOnly(!m_bEnableWindows);
		m_wndCBox1.EnableWindow(m_bEnableWindows);
		m_wndPropertyGrid.EnableWindow(m_bEnableWindows);
	}

	void doOnClose()	{ OnClose(); }

	enum { IDD = IDD_FORMVIEW };
	
	inline BOOL getActiveTemplate(CTransaction_template &rec) 
	{ 
		rec = m_recActiveTemplate;
		return TRUE;
	}

	void doSetNavigationBar();

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_MSG(CMDITemplateTraktFormFrame)
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnEnChangeEdit1();
};