#if !defined(AFX_FORRESTDB_H)
#define AFX_FORRESTDB_H

#include "StdAfx.h"

class CUMTemplateDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
	BOOL templateExist(CTransaction_template &);
	BOOL pricelistExist(CTransaction_pricelist &);
public:
	CUMTemplateDB(void);
	CUMTemplateDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CUMTemplateDB(DB_CONNECTION_DATA &db_connection);

	// Template database items; 070824 p�d
	BOOL getTemplates(vecTransactionTemplate &,int tmpl_type);
	BOOL addTemplate(CTransaction_template &);
	BOOL setTemplate(LPCTSTR name,int type,LPCTSTR xml);
	BOOL updTemplate(CTransaction_template &);
	BOOL delTemplate(CTransaction_template &);


	// External database items.
	// Pricelist is set in UMPricelists; 070824 p�d
	BOOL getPricelists(vecTransactionPricelist &);
	BOOL getPricelist(int,CTransaction_pricelist &);
	BOOL setPricelist(LPCTSTR name,int type,LPCTSTR xml);

	BOOL getCostTmpls(vecTransaction_costtempl &);
	BOOL getCostTmpl(int,CTransaction_costtempl &);
	BOOL setCostTmpl(LPCTSTR name,int type,LPCTSTR xml);

};

#endif