#pragma once

#include "Resource.h"

// CInputDialog dialog

class CInputDialog : public CDialog
{
	DECLARE_DYNAMIC(CInputDialog)

	CString m_sCaption;
	CString m_sPrompt;
	CString m_sInput;
	CString m_sOKBtn;
	CString m_sCancelBtn;

	CEdit m_wndInput;

public:
	CInputDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputDialog();

	virtual INT_PTR DoModal(CString prompt, CString caption,CString ok_btn,CString cancel_btn);
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	inline CString getInput() const { return m_sInput; }

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
