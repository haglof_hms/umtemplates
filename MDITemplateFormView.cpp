// MDITemplateFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TemplateFrame.h"
#include "MDITemplateFormView.h"

#include "SpecieDataFormView.h"

#include "UMTemplatesDB.h"

#include "SelectPricelistDlg.h"

#include "ResLangFileReader.h"

// CMDITemplateTraktFormView

IMPLEMENT_DYNCREATE(CMDITemplateTraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDITemplateTraktFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_WM_SIZE()
	ON_EN_KILLFOCUS(IDC_EDIT1, &CMDITemplateTraktFormView::OnEnKillfocusEdit1)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CMDITemplateTraktFormView::OnCbnSelchangeCombo1)
	ON_EN_CHANGE(IDC_EDIT1, &CMDITemplateTraktFormView::OnEnChangeEdit1)
END_MESSAGE_MAP()

CMDITemplateTraktFormView::CMDITemplateTraktFormView()
	: CXTResizeFormView(CMDITemplateTraktFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bInitialized2 = FALSE;
	m_bIsDirty = FALSE;
	m_nStatus = -1;
	m_bEnableWindows = FALSE;
	m_bDoSave=TRUE;
	m_nOldStatus = -1;
}

CMDITemplateTraktFormView::~CMDITemplateTraktFormView()
{
}

void CMDITemplateTraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP1, m_wndGroup1);
	DDX_Control(pDX, IDC_GROUP2, m_wndGroup2);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL_INFO1, m_wndLbl6);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	//}}AFX_DATA_MAP
}

void CMDITemplateTraktFormView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

void CMDITemplateTraktFormView::OnClose()
{
	// Do a save, no matter what; 080304 p�d	
	if (m_nDBIndex > -1)
	{
		if (isTemplateNameOK())
		{
			saveTemplateToDB(1);
		}
	}
	CXTResizeFormView::OnClose();
}

int CMDITemplateTraktFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (!m_wndPropertyGrid.m_hWnd)
	{
		m_wndPropertyGrid.Create(CRect(0, 0, 0, 0), this, ID_PROPERTY_GRID1);
		m_wndPropertyGrid.SetOwner(this);
		m_wndPropertyGrid.ShowHelp( FALSE );
		m_wndPropertyGrid.SetViewDivider(0.5);
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
	}
	if (! m_bInitialized )
	{
		m_nPriceListTypeOf = -1;

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndLbl6.SetTextColor(RED);

		CXTPPropertyGridItem* pSettings  = NULL;
		if (m_wndPropertyGrid.m_hWnd)
		{
			if (fileExists(m_sLangFN))
			{
				RLFReader *xml = new RLFReader;
				if (xml->Load(m_sLangFN))
				{
					m_sYes = xml->str(IDS_STRING204);
					m_sNo = xml->str(IDS_STRING205);

					m_sDiamClass = (xml->str(IDS_STRING1004));
					m_sHgtOverSea = (xml->str(IDS_STRING1005));
					m_sLatitude = (xml->str(IDS_STRING1006));
					m_sLongitude = (xml->str(IDS_STRING1011));
					m_sGrowthArea = (xml->str(IDS_STRING1007));
					m_sSI_H100 = (xml->str(IDS_STRING1008));
					m_sCostTmpl = (xml->str(IDS_STRING1010));

					m_sExchange = (xml->str(IDS_STRING2001));
					m_sPricelist = (xml->str(IDS_STRING2002));
					m_sQualdesc = (xml->str(IDS_STRING202));

					m_sAtCoast = xml->str(IDS_STRING2200);
					m_sSouthEast = xml->str(IDS_STRING2201);
					m_sRegion5 = xml->str(IDS_STRING2202);
					m_sPartOfPlot = xml->str(IDS_STRING2203);
					m_sSIH100_pine = (xml->str(IDS_STRING2204));

					m_sMsgCap = (xml->str(IDS_STRING214));
					m_sMsgDataMissing.Format(_T("%s\n%s\n\n%s\n%s\n%s\n\n%s\n"),
						(xml->str(IDS_STRING2190)),
						(xml->str(IDS_STRING2191)),
						(xml->str(IDS_STRING2150)),
						(xml->str(IDS_STRING2151)),
						(xml->str(IDS_STRING2152)),
						(xml->str(IDS_STRING110)));
					m_sMsgTmplNameMissing1.Format(_T("%s\n%s\n"),
						(xml->str(IDS_STRING2195)),
						(xml->str(IDS_STRING2196)));
					m_sMsgTmplNameMissing2.Format(_T("%s\n%s\n\n%s\n"),
						(xml->str(IDS_STRING2195)),
						(xml->str(IDS_STRING2196)),
						(xml->str(IDS_STRING2197)));
					m_sMsgDelTemplate = (xml->str(IDS_STRING217));

					m_sMsgDuplicateTemplName.Format(_T("%s\n%s"),
						(xml->str(IDS_STRING2180)),
						(xml->str(IDS_STRING2181)));
					m_sMsgDontSaveTemplate.Format(_T("\n\n%s\n%s\n\n"),
						(xml->str(IDS_STRING2182)),
						(xml->str(IDS_STRING2183)));

					m_sMsgWrongSIH100Entry.Format(_T("%s\n\n%s\n%s\n\n%s\n\n"),
						(xml->str(IDS_STRING2300)),
						(xml->str(IDS_STRING2301)),
						(xml->str(IDS_STRING2302)),
						(xml->str(IDS_STRING2303)));

					m_sMsgCantFindPricelist.Format(_T("%s\n%s\n"),
						(xml->str(IDS_STRING2610)),
						(xml->str(IDS_STRING2611)));

					m_sMsgNotToBeSaved.Format(_T("%s\n%s"),
						(xml->str(IDS_STRING2153)),
						(xml->str(IDS_STRING2154)));
					
					m_sMsgDiameterclassErr = (xml->str(IDS_STRING2184));

					m_sMsgCharError.Format(_T("%s < > /"),xml->str(IDS_STRING2540));

					m_arrValidateMsg.Add(xml->str(IDS_STRING2410));
					m_arrValidateMsg.Add(xml->str(IDS_STRING2411));
					m_arrValidateMsg.Add(xml->str(IDS_STRING2412));
					m_arrValidateMsg.Add(xml->str(IDS_STRING2413));
					m_arrValidateMsg.Add(xml->str(IDS_STRING2414));

					m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING200)));
//					setupTypeOfExchangefunctionsInPropertyGrid(pSettings);

					m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING203)));
//					setupMiscDataInPropertyGrid(pSettings);

					m_wndPropertyGrid.AddCategory((xml->str(IDS_STRING220)));
//					setupSoderbergsDataInPropertyGrid(pSettings);

					// Read Template created in Database; 070827 p�d
					getTemplatesFromDB();

					// Check if there's any templates in database.
					// If not, set mode to TEMPLATE_OPEN; 071029 p�d
					if (m_vecTransactionTemplate.size() > 0)
					{
						m_nDBIndex = m_vecTransactionTemplate.size() - 1;
						m_enumTemplateState = TEMPLATE_NONE;	// Items in database; 071029 p�d
						m_recActiveTemplate = m_vecTransactionTemplate[m_nDBIndex];
					}	
					else
					{
						m_nDBIndex = -1;
						m_enumTemplateState = TEMPLATE_NONE;	// Items in database; 071029 p�d
						m_recActiveTemplate = CTransaction_template();
					}
					// Get TypeOf pricelist in m_nDBIndex for m_vecTransactionTemplate and
					// set in setupExchangefunctionsInPropertyGrid; 070827 p�d
					getTemplatePricelistTypeOf();

					setupPropertyGrid();
				}	// if (xml->Load(sLangFN))
				delete xml;
			}	// if (fileExists(sLangFN))
			_DoCollapseExpand(m_wndPropertyGrid.GetCategories(),&CXTPPropertyGridItem::Expand);
		}

		m_bInitialized = TRUE;
	}

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	// Read Template created in Database; 070827 p�d
	return 0;
}

BOOL CMDITemplateTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDITemplateTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndCBox1.ResetContent();
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndGroup2.SetWindowText((xml->str(IDS_STRING100)));
			m_wndLbl1.SetWindowText((xml->str(IDS_STRING1001) + _T("*")));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING1002)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING1003)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING216)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING1100) + _T("*")));
			m_wndLbl6.SetWindowText((xml->str(IDS_STRING1103)));
			// Add data to Combobox; 080114 p�d
			m_wndCBox1.AddString((xml->str(IDS_STRING1101)));
			m_wndCBox1.AddString((xml->str(IDS_STRING1102)));
		}	// if (xml->Load(sLangFN))
		delete xml;
	}
	m_wndEdit1.SetEnabledColor( BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit1.SetLimitText(50);
	m_wndEdit2.SetEnabledColor( BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit3.SetEnabledColor( BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit4.SetEnabledColor( BLACK, WHITE );
	m_wndEdit4.SetDisabledColor(  BLACK, COL3DFACE );

	if (!m_vecTransactionTemplate.empty())
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1));
	else
		setNavigationButtons(FALSE,FALSE);

	populateData(m_nDBIndex);

	//Lagt till variabel f�r att h�lla reda p� om data skall kollas och sparas eller ej
	//J� 20111017 Bug #2443 
	if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
		m_bDoSave=TRUE;
	else
		m_bDoSave=FALSE;
}

BOOL CMDITemplateTraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDITemplateTraktFormView::doSetNavigationBar()
{
	if (m_vecTransactionTemplate.size() > 0 || m_enumTemplateState == TEMPLATE_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

	setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1));
}

void CMDITemplateTraktFormView::OnSetFocus(CWnd*)
{
	m_wndEdit1.SetFocus();
	m_bInitialized2 = TRUE;
}


LRESULT CMDITemplateTraktFormView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	int nValue = (int)lParam;
	switch (wParam)
	{
	case ID_NEW_ITEM :
		{
			// Do a save, no matter what; 080304 p�d	
			if (m_nDBIndex > -1)
			{
				if (isTemplateNameOK())
					saveTemplateToDB(-1);
			}
			if (!addNewTemplate())
				populateData(m_nDBIndex);
			if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
				m_bDoSave=TRUE;
			else
				m_bDoSave=FALSE;
		}
		break;

	case ID_SAVE_ITEM :
		{
			// Do a save, no matter what; 080304 p�d	
			if (isTemplateNameOK())
				saveTemplateToDB(1);
			m_enumTemplateState = TEMPLATE_OPEN;
		}
		break;

	case ID_DELETE_ITEM :
		{
			if (removeTemplateFromDB())
			{
				populateData(m_nDBIndex);
				if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
					m_bDoSave=TRUE;
				else
					m_bDoSave=FALSE;
			}
		}
		break;

		// Messages from HMSShell; Database navigation toolbar
	case ID_DBNAVIG_START :
		{
			if(m_bDoSave)
			{
				if (isTemplateNameOK())
				{
					// Before movin' on, check if user have changed
					// active data; 061113 p�d
					//				if (nValue == 1) 
					isHasDataChanged(1);
					m_nDBIndex = 0;
					setNavigationButtons(FALSE,TRUE);
					populateData(m_nDBIndex);
					if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
						m_bDoSave=TRUE;
					else
						m_bDoSave=FALSE;
				}
			}
			else
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
					m_bDoSave=TRUE;
				else
					m_bDoSave=FALSE;
			}
			break;
		}
	case ID_DBNAVIG_PREV :
		{
			if(m_bDoSave)
			{
				if (isTemplateNameOK())
				{
					// Before movin' on, check if user have changed
					// active data; 061113 p�d
					//				if (nValue == 1) 
					isHasDataChanged(1);
					m_nDBIndex--;
					if (m_nDBIndex < 0)
						m_nDBIndex = 0;
					if (m_nDBIndex == 0)
						setNavigationButtons(FALSE,TRUE);
					else
						setNavigationButtons(TRUE,TRUE);
					populateData(m_nDBIndex);
					if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
						m_bDoSave=TRUE;
					else
						m_bDoSave=FALSE;

				}
			}
			else
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
					setNavigationButtons(FALSE,TRUE);
				else
					setNavigationButtons(TRUE,TRUE);
				populateData(m_nDBIndex);
				if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
					m_bDoSave=TRUE;
				else
					m_bDoSave=FALSE;

			}
			break;
		}
	case ID_DBNAVIG_NEXT :
		{
			if(m_bDoSave)
			{
				if (isTemplateNameOK())
				{
					// Before movin' on, check if user have changed
					// active data; 061113 p�d
					//				if (nValue == 1) 
					isHasDataChanged(1);
					m_nDBIndex++;
					if (m_nDBIndex > ((UINT)m_vecTransactionTemplate.size() - 1))
						m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;

					if (m_nDBIndex == (UINT)m_vecTransactionTemplate.size() - 1)
						setNavigationButtons(TRUE,FALSE);
					else
						setNavigationButtons(TRUE,TRUE);
					populateData(m_nDBIndex);
					if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
						m_bDoSave=TRUE;
					else
						m_bDoSave=FALSE;

				}

			}
			else
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecTransactionTemplate.size() - 1))
					m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;

				if (m_nDBIndex == (UINT)m_vecTransactionTemplate.size() - 1)
					setNavigationButtons(TRUE,FALSE);
				else
					setNavigationButtons(TRUE,TRUE);
				populateData(m_nDBIndex);
				if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
					m_bDoSave=TRUE;
				else
					m_bDoSave=FALSE;
			}
			break;
		}
	case ID_DBNAVIG_END :
		{
			if(m_bDoSave)
			{
				if (isTemplateNameOK())
				{
					// Before movin' on, check if user have changed
					// active data; 061113 p�d
					//				if (nValue == 1) 
					isHasDataChanged(1);
					m_nDBIndex = (UINT)m_vecTransactionTemplate.size()-1;
					setNavigationButtons(TRUE,FALSE);	
					populateData(m_nDBIndex);
					if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
						m_bDoSave=TRUE;
					else
						m_bDoSave=FALSE;
				}
			}
			else
			{
				m_nDBIndex = (UINT)m_vecTransactionTemplate.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
				if(m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
					m_bDoSave=TRUE;
				else
					m_bDoSave=FALSE;
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

// PUBLIC
BOOL CMDITemplateTraktFormView::isHasDataChanged(short action)
{
	BOOL bReturn = TRUE;
	// Check if we are enabled, if not just quit; 090128 p�d
	if (m_bEnableWindows)
	{
		if (!isTemplateNameOK()) 
			return FALSE;
		else
			bReturn = saveTemplateToDB(action);
		resetIsDirty();
	}
	return bReturn;
}

BOOL CMDITemplateTraktFormView::getIsDirty(void)
{
	CXTPPropertyGridItem *pItem = NULL;

	if (m_wndEdit1.isDirty() ||
		  m_wndEdit2.isDirty() ||
		  m_wndEdit3.isDirty() ||
			m_wndEdit4.isDirty())
		return TRUE;

	if (m_wndPropertyGrid.isDirty())
		return TRUE;

	// get number of pages (i.e. number of species in selected Pricelist); 070809 p�d
	int	nNumOfPages = m_wndTabControl.getNumOfTabPages();
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			m_tabManager = m_wndTabControl.getTabPage(i);
			if (m_tabManager)
			{
				CSpecieDataFormView* pView = DYNAMIC_DOWNCAST(CSpecieDataFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
				if (pView != NULL)
				{
					if (pView->getIsDirty())
						return TRUE;
				}
			}	// if (m_tabManager)
		}	// for (int i = 0;i < nNumOfPages;i++)
	}	// if (nNumOfPages > 0)

	return FALSE;
}

void CMDITemplateTraktFormView::resetIsDirty(void)
{
	m_wndEdit1.resetIsDirty();
  m_wndEdit2.resetIsDirty();
	m_wndEdit3.resetIsDirty();
	m_wndEdit4.resetIsDirty();
	m_wndPropertyGrid.resetIsDirty();
	// get number of pages (i.e. number of species in selected Pricelist); 070809 p�d
	int	nNumOfPages = m_wndTabControl.getNumOfTabPages();
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			m_tabManager = m_wndTabControl.getTabPage(i);
			if (m_tabManager)
			{
				CSpecieDataFormView* pView = DYNAMIC_DOWNCAST(CSpecieDataFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
				if (pView != NULL)
				{
					pView->resetIsDirty();
				}	// if (pView != NULL)
			}	// if (m_tabManager)
		}	// for (int i = 0;i < nNumOfPages;i++)
	}	// if (nNumOfPages > 0)
}

BOOL CMDITemplateTraktFormView::isSpeciePagesOKToClose(CStringArray &arr)
{
	CStringArray sarrPerSpc;
	// If we're in development, there's no need to do all the
	// checkin'; 080304 p�d
	if (m_nStatus == ID_TEMPLATE_TRAKT_DEVELOPMENT)
	{
		return TRUE;
	}

	// get number of pages (i.e. number of species in selected Pricelist); 070809 p�d
	int	nNumOfPages = m_wndTabControl.getNumOfTabPages();
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			m_tabManager = m_wndTabControl.getTabPage(i);
			if (m_tabManager)
			{
				CSpecieDataFormView* pView = DYNAMIC_DOWNCAST(CSpecieDataFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
				if (pView != NULL)
				{
					sarrPerSpc.RemoveAll();
					pView->isOKToClose(m_tabManager->GetCaption(),sarrPerSpc);
					if (sarrPerSpc.GetCount() > 0)
						arr.Append(sarrPerSpc);
				}	// if (pView != NULL)
			}	// if (m_tabManager)
		}	// for (int i = 0;i < nNumOfPages;i++)
	}	// if (nNumOfPages > 0)
	sarrPerSpc.RemoveAll();
	return (arr.GetCount() == 0);
}

void CMDITemplateTraktFormView::clearData(void)
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit1.setItemIntData(-1);
	m_wndEdit2.SetWindowText(getUserName().MakeUpper());
	m_wndEdit3.SetWindowText(getDateEx());
	m_wndEdit4.SetWindowText(_T(""));

	m_sBindToExchPricelistStr = _T("");
	m_sBindToPricelistSelected = _T("");
	m_nPriceListTypeOf=-1;

	m_fBindToDiamClass = DEF_DIAMCLASS;	// Default value for Diameterclass; 080411 p�d
	m_nBindToHgtOverSea = 0;
	m_nBindToLatitude = 0;
	m_nBindToLongitude = 0;
	m_sBindToGrowthArea = _T("");
	m_sBindToSIH100 = _T("");
	m_sBindToCostTmpl = _T("");

	m_wndCBox1.SetCurSel(1);	// "Under utveckling"

	// Update grid; 071029 p�d
	m_wndPropertyGrid.Refresh();

}

// CMDITemplateTraktFormView diagnostics

#ifdef _DEBUG
void CMDITemplateTraktFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMDITemplateTraktFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CMDITemplateTraktFormView message handlers

void CMDITemplateTraktFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType,cx,cy);

	if (m_wndGroup2.GetSafeHwnd())
	{
		setResize(&m_wndGroup2,2,20,cx-4,100);
	}

	if (m_wndGroup1.GetSafeHwnd())
	{
		setResize(&m_wndGroup1,2,120,cx-4,cy-125);
	}

	if (m_wndPropertyGrid.GetSafeHwnd())
	{
		m_wndPropertyGrid.MoveWindow(10, 135, 290, cy-145);
	}

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(320, 135, cx-330, cy-145);
	}
}

BOOL CMDITemplateTraktFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int spc_id, LPCTSTR spc_name, int nIcon)
{
	double fM3FubM3To,fM3SkM3Ub;
	vecUCFunctionList vecFuncList;

	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CSpecieDataFormView* pWnd;
	TRY
	{
		pWnd = (CSpecieDataFormView*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();
	// Get functions per specie and add function identifer
	// to View added; 070828 p�d
	// This is done, so we can list ALL functions for
	// a selected calculation model; 070828 p�d
	getTemplateFunctionsPerSpcecie(spc_id,
																 m_recActiveTemplate.getTemplateFile(),
																 vecFuncList,
																 &fM3FubM3To,
																 &fM3SkM3Ub);

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->CreateWnd(dwStyle,
											 rect, 
											 &m_wndTabControl, 
											 (AFX_IDW_PANE_FIRST + nTab), 
											 &contextT, 
											 m_listQDesc,
											 spc_id,
											 spc_name,
											 vecFuncList,
											 m_vecAssortmentsPerSpc,
											 fM3SkM3Ub))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

// Setup the XML template file; 070808 p�d
BOOL CMDITemplateTraktFormView::createTemplateXMLFile(void)
{
	CXTPPropertyGridItem *pItem = NULL;
	CXTPPropertyGridItem *pChildItem = NULL;
	CXTPPropertyGridItemConstraints *pConstraints = NULL;
	CXTPPropertyGridItemConstraint *pConstraint = NULL;

	UINT nIndex;
	UINT nCostTmplID = -1;
	int nNumOfPages = 0;
	CStringArray arrBuffer;
	CString sBuffer;
	CString S,sTmp, csTmp, csFromAssort, csToAssort;
	double fM3SkM3Ub;
	UCFunctions function;
	UCFunctionList function_list;
	int nTypeOfPricelist = 0;

	//-----------------------------------------------------------------------------
	// Setup sBuffer for XML data; 070809 p�d	
	arrBuffer.RemoveAll();
	arrBuffer.Add(TAG_FIRST_ROW);

	// Start sBuffer XML data; 070809 p�d	
	arrBuffer.Add(TAG_TEMPLATE_START);

	//-----------------------------------------------------------------------------
	// GENERAL DATA
	// Get name of template; 070809 p�d
	csTmp = m_wndEdit1.getText();
	TextToHtml(&csTmp);
	S.Format(TAG_TEMPLATE_NAME, csTmp);
	arrBuffer.Add(S);
	// Get created by; 070809 p�d
	csTmp = m_wndEdit2.getText();
	TextToHtml(&csTmp);
	S.Format(TAG_TEMPLATE_DONE_BY, csTmp);
	arrBuffer.Add(S);
	// Get date; 070809 p�d
	S.Format(TAG_TEMPLATE_DATE,m_wndEdit3.getText());
	arrBuffer.Add(S);
	// Get notes; 120808 p�d
	csTmp = m_wndEdit4.getText();
	TextToHtml(&csTmp);
	S.Format(TAG_TEMPLATE_NOTES, csTmp);
	arrBuffer.Add(S);

	//-----------------------------------------------------------------------------
	// Find selected exchange calculation type; 070809 p�d
	UCFunctions flist;
	if (m_vecExchangeFunc.size() > 0)
	{
		for (nIndex = 0;nIndex < m_vecExchangeFunc.size();nIndex++)
		{
			flist = m_vecExchangeFunc[nIndex];
			// Added for backword compatibility; 090423 p�d
			if (m_sBindToExchPricelistStr.Find(_T("(prislista)")) > 0 || (m_sBindToExchPricelistStr.IsEmpty() && flist.getIdentifer() == 1))
					m_sBindToExchPricelistStr = _T("Matrisprislista");

			if (m_sBindToExchPricelistStr.Find(_T("(medelpriser)")) > 0 || (m_sBindToExchPricelistStr.IsEmpty() && flist.getIdentifer() == 2))
				m_sBindToExchPricelistStr = _T("Medelprislista");

			if (m_sBindToExchPricelistStr.Compare(flist.getName()) == 0)
			{
				S.Format(TAG_TEMPLATE_EXCHANGE,flist.getIdentifer(),flist.getName());
				arrBuffer.Add(S);
				break;
			}	// if (m_sBindToExchPricelistStr.Compare(flist.getName()) == 0)
		}	// if (nIndex < m_vecExchangeFunc.size())

		if (m_vecTransactionPricelist.size() > 0)
		{
			for (nIndex = 0;nIndex < m_vecTransactionPricelist.size();nIndex++)
			{
				CTransaction_pricelist prl = m_vecTransactionPricelist[nIndex];
				nTypeOfPricelist = prl.getTypeOf();
				if (nTypeOfPricelist < 0) nTypeOfPricelist *= -1;
				if (nTypeOfPricelist == flist.getIdentifer() && m_sBindToPricelistSelected.Compare(prl.getName()) == 0)
				{
					CString prlname = prl.getName();
					prlname.Replace(_T("&"), _T("&amp;"));
					S.Format(TAG_TEMPLATE_PRICELIST,prl.getID(),prl.getTypeOf(),prlname);
					arrBuffer.Add(S);
					break;
				}	// if (m_sBindToPricelistSelected.Compare(prl.getName()) == 0)
			}	// for (nIndex = 0;nIndex < m_vecTransactionPricelist.size();nIndex++)
		}	// if (nIndex < m_vecExchangeFunc.size())

	}	// if (m_vecExchangeFunc.size() > 0)
	// MISC. DATA
	// Get diamterclass; 070809 p�d
	S.Format(TAG_TEMPLATE_DCLS,m_fBindToDiamClass);
	arrBuffer.Add(S);

	// Get Height over sea; 070809 p�d
	S.Format(TAG_TEMPLATE_HGT_OVER_SEA,m_nBindToHgtOverSea);
	arrBuffer.Add(S);

	// Get Latitude (breddgrad); 070809 p�d
	S.Format(TAG_TEMPLATE_LATITUDE,m_nBindToLatitude);
	arrBuffer.Add(S);

	// Get Latitude (breddgrad); 070809 p�d
	S.Format(TAG_TEMPLATE_LONGITUDE,m_nBindToLongitude);
	arrBuffer.Add(S);

	// Get Growth area (Tillv�xtomr�de); 070809 p�d
	S.Format(TAG_TEMPLATE_GROWTH_AREA,m_sBindToGrowthArea);
	arrBuffer.Add(S);

	// Get SI H100; 070809 p�d
	S.Format(TAG_TEMPLATE_SI_H100,m_sBindToSIH100);
	arrBuffer.Add(S);

	nCostTmplID = getCostTmplID(m_sBindToCostTmpl);
	CString costname = m_sBindToCostTmpl; costname.Replace(_T("&"), _T("&amp;"));
	S.Format(TAG_TEMPLATE_COSTS_TMPL,nCostTmplID,costname);
	arrBuffer.Add(S);

	// "S�derbergs" specific data; 071206 p�d
	S.Format(TAG_TEMPLATE_ATCOAST,(m_bBindToIsAtCoast ? 1 : 0));
	arrBuffer.Add(S);

	S.Format(TAG_TEMPLATE_SOUTHEAST,(m_bBindToIsSouthEast ? 1 : 0));
	arrBuffer.Add(S);

	S.Format(TAG_TEMPLATE_REGION5,(m_bBindToIsRegion5 ? 1 : 0));
	arrBuffer.Add(S);

	S.Format(TAG_TEMPLATE_PART_OF_PLOT,(m_bBindToIsPartOfPlot ? 1 : 0));
	arrBuffer.Add(S);

	S.Format(TAG_TEMPLATE_SI_H100_PINE,m_sBindToSIH100_pine);
	arrBuffer.Add(S);

	//-----------------------------------------------------------------------------
	// Get information from Species in Tab control per page; 070809 p�d
	
	// get number of pages (i.e. number of species in selected Pricelist); 070809 p�d
	nNumOfPages = m_wndTabControl.getNumOfTabPages();
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			m_tabManager = m_wndTabControl.getTabPage(i);
			if (m_tabManager)
			{
				CSpecieDataFormView* pView = DYNAMIC_DOWNCAST(CSpecieDataFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
				if (pView != NULL)
				{
					S.Format(TAG_TEMPLATE_START_SPECIE,pView->getSpecieID(),pView->getSpecieName());
					arrBuffer.Add(S);
					// Get height information and add to XML template; 070809 p�d
					pView->getHeightFunction(function,function_list);
					S.Format(TAG_TEMPLATE_SPECIE_HGT,
										function_list.getID(),
										function_list.getSpcID(),
										function_list.getIndex(),
										function_list.getSpcName(),
										function_list.getFuncType(),
										function_list.getFuncArea(),
										function_list.getFuncDesc());
					arrBuffer.Add(S);
					// Reset data, so it won't be copied to the next 
					// function type; 070831 p�d
					function = UCFunctions();
					function_list = UCFunctionList();

					// Get volume information and add to XML template; 070809 p�d
					pView->getVolumeFunction(function,function_list);
					S.Format(TAG_TEMPLATE_SPECIE_VOL,
										function_list.getID(),
										function_list.getSpcID(),
										function_list.getIndex(),
										function_list.getSpcName(),
										function_list.getFuncType(),
										function_list.getFuncArea(),
										function_list.getFuncDesc());
					arrBuffer.Add(S);
					// Reset data, so it won't be copied to the next 
					// function type; 070831 p�d
					function = UCFunctions();
					function_list = UCFunctionList();

					// Get bark information and add to XML template; 070809 p�d
					pView->getBarkFunction(function,function_list);
					S.Format(TAG_TEMPLATE_SPECIE_BARK,
										function_list.getID(),
										function_list.getSpcID(),
										function_list.getIndex(),
										function_list.getSpcName(),
										function_list.getFuncType(),
										function_list.getFuncArea(),
										function_list.getFuncDesc());
					arrBuffer.Add(S);
					// Reset data, so it won't be copied to the next 
					// function type; 070831 p�d
					function = UCFunctions();
					function_list = UCFunctionList();

					// Get volume under bark information and add to XML template; 071024 p�d
					pView->getVolumeUBFunction(function,function_list,&fM3SkM3Ub);
					S.Format(TAG_TEMPLATE_SPECIE_VOL_UB,
										function_list.getID(),
										function_list.getSpcID(),
										function_list.getIndex(),
										function_list.getSpcName(),
										function_list.getFuncType(),
										function_list.getFuncArea(),
										function_list.getFuncDesc(),
										fM3SkM3Ub);
					arrBuffer.Add(S);
					// Reset data, so it won't be copied to the next 
					// function type; 070831 p�d
					function = UCFunctions();
					function_list = UCFunctionList();
					// Get rest of specie specific data; 070809 p�d
					S.Format(TAG_TEMPLATE_SPECIE_QDESC,
										pView->getQDescIndex(pView->getQualityDesc()),
										pView->getQualityDesc());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_GROT,pView->getGrotIndex(),pView->getGrotPercent(),pView->getGrotPrice(),pView->getGrotCost());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_TRANSP_DIST1,pView->getTranspDist1());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_TRANSP_DIST2,pView->getTranspDist2());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_SKFUB,pView->getSkToFub());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_FUBTO,pView->getFubToTO());
					arrBuffer.Add(S);
/*
					S.Format(TAG_TEMPLATE_SPECIE_ATCOAST,pView->getIsAtCoast());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_SOUTHEAST,pView->getIsSouthEast());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_REGION5,pView->getIsRegion5());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_PART_OF_PLOT,pView->getIsPartOfPlot());
					arrBuffer.Add(S);
*/
					S.Format(TAG_TEMPLATE_SPECIE_H25,pView->getH25());
					arrBuffer.Add(S);

					S.Format(TAG_TEMPLATE_SPECIE_GCROWN,pView->getGreenCrown());
					arrBuffer.Add(S);

					// Only add transfers if there's any to add; 070831 p�d
					if (pView->getTemplateTransfers().size() > 0)
					{
						S =	TAG_TEMPLATE_TRANSFER_START;
						for (int i = 0;i < pView->getTemplateTransfers().size();i++)
						{
							csFromAssort = pView->getTemplateTransfers()[i].getFromAssort();
							TextToHtml(&csFromAssort);
							csToAssort = pView->getTemplateTransfers()[i].getToAssort();
							TextToHtml(&csToAssort);
							sTmp.Format(TAG_TEMPLATE_TRANSFER_ITEM,
													pView->getTemplateTransfers()[i].getFromAssortID(),
													csFromAssort,
													pView->getTemplateTransfers()[i].getToAssortID(),
													csToAssort,
													pView->getTemplateTransfers()[i].getM3Fub(),
													pView->getTemplateTransfers()[i].getPercent());
							S += sTmp;
						}	// for (int i = 0;i < m_wndReport.GetRecords()->GetCount();i++)
						S +=	TAG_TEMPLATE_TRANSFER_END;
						arrBuffer.Add(S);	
					}	// if (pView->getTemplateTransfers().size() > 0)

					arrBuffer.Add(TAG_TEMPLATE_END_SPECIE);	
					function_list = UCFunctionList();
				}	// if (pView != NULL)
			}	// if (m_tabManager)
		}	// for (int i = 0;i < nNumOfPages;i++)
	}	// if (nNumOfPages > 0)

	//-----------------------------------------------------------------------------
	// End sBuffer XML data; 070809 p�d	
	arrBuffer.Add(TAG_TEMPLATE_END);

	if (arrBuffer.GetCount() > 0)
	{
		for (int i = 0;i < arrBuffer.GetCount();i++)
		{
			sBuffer += arrBuffer.GetAt(i);
		}

		m_sTemplateXMLFile = sBuffer;

		return TRUE;
	}

	return FALSE;
	

/*
	TemplateParser *pParser = new TemplateParser();
	if (pParser != NULL)
	{
		pParser->LoadFromBuffer(sBuffer);

		pParser->SaveToFile("C:\\Template_test.xml");

		delete pParser;
	}
*/
}

BOOL CMDITemplateTraktFormView::addNewTemplate(void)
{
	int nDlgRet = IDOK;
	// Open the CSelectPricelistDlg dialog, to select
	// a new Pricelist; 080707 p�d
	CSelectPricelistDlg *pDlg = new CSelectPricelistDlg();
	if (pDlg == NULL) return FALSE;

	// Get pricelists from DB; 080707 p�d
	getPricelistsInDB();
	pDlg->setPricelists(m_vecTransactionPricelist);
	nDlgRet = pDlg->DoModal();
	if (nDlgRet == IDCANCEL) return FALSE;
	if (nDlgRet == IDOK)
	{
		m_enumTemplateState = TEMPLATE_NEW;	

		//---------------------------------------------------------------------------------
		// Clear property grid; 070828 p�d
		clearData();

		m_recActiveTemplate = CTransaction_template();	// Set an empty
		//---------------------------------------------------------------------------------
		// Clear/Remove tabs in TabControl; 070808 p�d
		if (m_wndTabControl.getNumOfTabPages() > 0)
		{
			for (int ii = m_wndTabControl.getNumOfTabPages()-1;ii >= 0;ii--)
			{
				m_wndTabControl.getTabPage(ii)->Remove();
			}
			m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
		}

		setEnbaleWindows(TRUE);

		m_sBindToExchPricelistStr = pDlg->getSelectedExch();
		m_sBindToPricelistSelected = pDlg->getSelectedPrl();
		m_nPriceListTypeOf = pDlg->getSelectedPrlTypeOf();

		// Added for backword compatibility; 090423 p�d
		if (m_sBindToExchPricelistStr.Find(_T("(medelpriser)")) > 0)
				m_sBindToExchPricelistStr = _T("Medelprislista");
		if (m_sBindToExchPricelistStr.Find(_T("(prislista)")) > 0)
				m_sBindToExchPricelistStr = _T("Matrisprislista");

		m_wndPropertyGrid.Refresh();

		setTabsForSpeciesInSelectedPricelist();

		// Set status to UNDER DEVLOPMENT for a NEW Template; 081027 p�d
		m_nStatus = ID_TEMPLATE_TRAKT_DEVELOPMENT;

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		setLockTemplateFormView(FALSE);

	}	// if (pDlg == IDOK)

	return TRUE;

}

void CMDITemplateTraktFormView::getTemplatePricelistTypeOf(void)
{
	TCHAR szTemplExchange[127];
	if (m_vecTransactionTemplate.size() > 0 && 
		  m_nDBIndex >= 0 &&
			m_nDBIndex < m_vecTransactionTemplate.size())
	{
		m_recActiveTemplate = m_vecTransactionTemplate[m_nDBIndex];

		TemplateParser *pars = new TemplateParser();
		if (pars != NULL)
		{
			if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
			{
				//H�mta id med exchange funktionen ist�llet eftersom om man itne valt n�gon 
				//prislista s� finns ingen prislisteinformation, dvs type_of tagen finns inte i mallens xml 20120117 J� Bug 2679
				pars->getTemplateExchange(&m_nPriceListTypeOf,szTemplExchange);
				//pars->getTemplatePricelistTypeOf(&m_nPriceListTypeOf);
				

			}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))

			delete pars;
		}	// if (pars != NULL)
	}
}

void CMDITemplateTraktFormView::setTabsForSpeciesInSelectedPricelist(void)
{
	CString S;
	CTransaction_species recSelSpecie;
	CSpecieDataFormView* pView = NULL;
	BOOL bFound=FALSE;
	TCHAR szActivePrlName[127];
	int nID,nActivePrlTypeOf=-1;

	/*
	//Ta r�tt p� vilken typ av prislista som �r vald, matris eller medel
	if (m_wndPropertyGrid.GetSafeHwnd())
	{
		CXTPPropertyGridItem* pItem = NULL;
		CXTPPropertyGridItems *pCategories = NULL; 
		pCategories =	m_wndPropertyGrid.GetCategories();
		if (pCategories != NULL)
		{
			pItem  = pCategories->GetAt(0);
			if(pItem!=NULL)
			{
				CXTPPropertyGridItems *pChilds;
				pChilds=pItem->GetChilds();
				if(pChilds!=NULL)
				{
					CXTPPropertyGridItem* pChild;
					pChild=pChilds->GetAt(0);
					if(pChild!=NULL)
					{
						CString csTest=_T("");
						csTest=pChild->GetCaption();
						CXTPPropertyGridItemConstraints *pItemConstraints = pChild->GetConstraints();
						if (pItemConstraints != NULL)
						{
							m_nBindToPricelistSelectedTypeOf = pItemConstraints->GetCurrent();
						}
					}
				}
			}
		}
	}
	if(m_nBindToPricelistSelectedTypeOf<0)
		m_nBindToPricelistSelectedTypeOf=m_nPriceListTypeOf;*/

	//m_nBindToPricelistSelectedTypeOf=m_nPriceListTypeOf;
	
	CString sActiveTemplateXML(m_recActiveTemplate.getTemplateFile());
	// Get info. on active template; 090624 p�d
	TemplateParser tmplParser_active;
	if (tmplParser_active.LoadFromBuffer(sActiveTemplateXML))
	{
		tmplParser_active.getTemplatePricelist(&nID,szActivePrlName);
		tmplParser_active.getTemplatePricelistTypeOf(&nActivePrlTypeOf);
	}


	// Get pricelists from DB; 070808 p�d
	getPricelistsInDB();
	// Try to match the Selected Exchange type and Pricelist; 070608 p�d
	for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
	{
		CTransaction_pricelist prl = m_vecTransactionPricelist[i];
		//Kontrollera �ven typen av prislista h�r och inte bara namnet 
		//J� 20120112 bug #2679
		if ((m_sBindToPricelistSelected.Compare(prl.getName()) == 0)  && (m_nPriceListTypeOf==prl.getTypeOf()))
		{
			PricelistParser prlParser;
			if (prlParser.LoadFromBuffer(prl.getPricelistFile()))
			{
				bFound=TRUE;
				m_wndTabControl.SetRedraw( FALSE );

				//---------------------------------------------------------------------------------
				// Get species in pricelist; 070808 p�d
				prlParser.getSpeciesInPricelistFile(m_vecTransactionSpecies);

				// Try to find out if the pricelist selected is the same as the
				// pricelist active and if the pricelist selected have changed
				// the number of species; 090625 p�d
				// Pricelist not changed; 090625 p�d
				if ( (m_sBindToPricelistSelected.Compare(szActivePrlName) == 0) && (m_nPriceListTypeOf==nActivePrlTypeOf))
				{
					m_enumPrlOpen = SAME_AS_BEFORE;
		
				}
				// Pricelist changed; 090625 p�d
				else
				{
					m_enumPrlOpen = NOT_THE_SAME;
				}
				
				//---------------------------------------------------------------------------------
				// Clear TabControl; 070808 p�d
				m_wndTabControl.DeleteAllItems();

				//---------------------------------------------------------------------------------
				// Get assortments species in pricelist; 070810 p�d
				prlParser.getAssortmentPerSpecie(m_vecAssortmentsPerSpc);

				for (UINT j = 0;j < m_vecTransactionSpecies.size();j++)
				{
					CTransaction_species spc = m_vecTransactionSpecies[j];

					//---------------------------------------------------------------------------------
					// Get qualitydescriptions for pricelist; 070808 p�d
					prlParser.getQualDescNameForSpecie(spc.getSpcID(),m_listQDesc);
		
					AddView(RUNTIME_CLASS(CSpecieDataFormView), spc.getSpcName(),spc.getSpcID(),spc.getSpcName() ,3);	
				}
				m_wndTabControl.SetRedraw( TRUE );
				m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

			}	// if (pPrlParser->LoadFromBuffer(prl.getPricelistFile()))
			break;
		}	// if (m_sBindToPricelistSelected.Compare(prl.getName()) == 0)
	}	// for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
	if(!bFound)
	{
		// Clear/Remove tabs in TabControl; 080707 p�d
		if (m_wndTabControl.getNumOfTabPages() > 0)
		{
			for (int ii = m_wndTabControl.getNumOfTabPages()-1;ii >= 0;ii--)
			{
				m_wndTabControl.getTabPage(ii)->Remove();
			}
			m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
		}
		//Om den inte hittar prislista s� populera om prislistevalen
	}
}

void CMDITemplateTraktFormView::setFunctionsInTabsForSpeciesForSelectedPricelist(void)
{
	CString sInfo,S;
	double fM3SkToM3Fub = 0.0;
	double fM3FubToM3To = 0.0;
	double fM3SkToM3Ub = 0.0;
	double fH25 = 0.0;
	double fGCrown = 0.0;
	int nQDescIndex;
	int nTranspDist1 = 0;
	int nTranspDist2 = 0;
	TCHAR szQDesc[127];
	int nGrotIndex = 0;
	double fGrotPercent = 0.0;
	double fGrotPrice = 0.0;
	double fGrotCost = 0.0;
	vecTransactionTemplateTransfers vecTransfers;
	vecUCFunctionList vecFuncList;

	// get number of pages (i.e. number of species in selected Pricelist); 070809 p�d
	int	nNumOfPages = m_wndTabControl.getNumOfTabPages();
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			m_tabManager = m_wndTabControl.getTabPage(i);
			if (m_tabManager)
			{
				CSpecieDataFormView* pView = DYNAMIC_DOWNCAST(CSpecieDataFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
				if (pView != NULL)
				{
					//-------------------------------------------------------------------------				
					fM3SkToM3Fub = 0.0;
					fM3FubToM3To = 0.0;
					fM3SkToM3Ub = 0.0;
					fH25 = 0.0;
					fGCrown = 0.0;
					nQDescIndex = -1;
					nTranspDist1 = 0;
					nTranspDist2 = 0;
					szQDesc[0] = '\0';

					getTemplateMiscFunctionsPerSpcecie(pView->getSpecieID(),
																						 m_recActiveTemplate.getTemplateFile(),
																						 &nQDescIndex,
																						 szQDesc,
																						 &fM3SkToM3Fub,
																						 &fM3FubToM3To,
																						 &fH25,
																						 &nTranspDist1,
																						 &nTranspDist2,
																						 &fGCrown);

					getTemplateGrotFunctionsPerSpcecie(pView->getSpecieID(),
																						 m_recActiveTemplate.getTemplateFile(),
																						 &nGrotIndex,
																						 &fGrotPercent,
																						 &fGrotPrice,
																						 &fGrotCost);

					if (m_enumPrlOpen == SAME_AS_BEFORE)
					{
						pView->setQualityDesc(szQDesc);

						pView->setTranspDist1(nTranspDist1);
						pView->setTranspDist2(nTranspDist2);

						pView->setSkToFub(0.0);
						pView->setFubToTo(fM3FubToM3To);
						pView->setH25(fH25);
						pView->setGreenCrown(fGCrown);
						
						pView->setGrotIndex(nGrotIndex);
						pView->setGrotPercent(fGrotPercent);
						pView->setGrotPrice(fGrotPrice);
						pView->setGrotCost(fGrotCost);
						//-------------------------------------------------------------------------				
						getTemplateTransfersPerSpcecie(pView->getSpecieID(),
																					 m_recActiveTemplate.getTemplateFile(),
																					 vecTransfers);
						pView->setTemplateTransfers(vecTransfers);
					}
					else
					{
						pView->setQualityDesc(_T(""));

						pView->setTranspDist1(0);
						pView->setTranspDist2(0);

						pView->setSkToFub(0.0);
						pView->setFubToTo(fM3FubToM3To);
						pView->setH25(fH25);
						pView->setGreenCrown(fGCrown);

						pView->setGrotIndex(nGrotIndex);
						pView->setGrotPercent(fGrotPercent);
						pView->setGrotPrice(fGrotPrice);
						pView->setGrotCost(fGrotCost);
					}
					//-------------------------------------------------------------------------				
					getTemplateFunctionsPerSpcecie(pView->getSpecieID(),
																				 m_recActiveTemplate.getTemplateFile(),
																				 vecFuncList,
																				 &fM3FubToM3To,
																				 &fM3SkToM3Ub);
					if (vecFuncList.size() == 4)	// 4 => Height,Volume,Bark and Volume under bark; 071024 p�d
					{
						// Only add data if there's data to add; 070828 p�d
						if (vecFuncList[0].getSpcName() != _T(""))
						{
							sInfo.Format(_T("%s , %s"),
									vecFuncList[0].getSpcName(),
									vecFuncList[0].getFuncArea());
							pView->setBindToHgt(vecFuncList[0].getFuncType(),sInfo);
						}
					
						// Only add data if there's data to add; 070828 p�d
						if (vecFuncList[1].getSpcName() != _T(""))
						{
							sInfo.Format(_T("%s, %s, %s, %s"),
									vecFuncList[1].getSpcName(),
									vecFuncList[1].getFuncType(),
									vecFuncList[1].getFuncArea(),
									vecFuncList[1].getFuncDesc());
							pView->setBindToVol(vecFuncList[1].getFuncType(),sInfo);
						}

						// Only add data if there's data to add; 070828 p�d
						if (vecFuncList[2].getSpcName() != _T(""))
						{
							sInfo.Format(_T("%s, %s"),
									vecFuncList[2].getSpcName(),
									vecFuncList[2].getFuncArea());
							pView->setBindToBark(vecFuncList[2].getFuncType(),sInfo);
						}
					
						// Only add data if there's data to add; 070828 p�d
						if (vecFuncList[3].getSpcName() != _T(""))
						{
							sInfo.Format(_T("%s, %s, %s, %s"),
									vecFuncList[3].getSpcName(),
									vecFuncList[3].getFuncType(),
									vecFuncList[3].getFuncArea(),
									vecFuncList[3].getFuncDesc());
							pView->setBindToVolUB(vecFuncList[3].getFuncType(),sInfo,fM3SkToM3Ub);
						}

					}
				}	// if (pView != NULL)
			}	// if (m_tabManager)
		}
	}
}


// Pricelist set in prn_pricelist_table
void CMDITemplateTraktFormView::getPricelistsInDB(void)
{
	if (m_bConnected)
	{
		CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_vecTransactionPricelist.clear();
			m_bConnected = pDB->getPricelists(m_vecTransactionPricelist);
			delete pDB;
		}
	}
}

void CMDITemplateTraktFormView::getCostTmplInDB(void)
{
	if (m_bConnected)
	{
		CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_vecTransaction_costtempl.clear();
			m_bConnected = pDB->getCostTmpls(m_vecTransaction_costtempl);
			delete pDB;
		}
	}
}

int CMDITemplateTraktFormView::getCostTmplID(LPCTSTR cost_tmpl_name)
{
	// To be sure, we load costs templates; 071010 p�d
	getCostTmplInDB();
	if (m_vecTransaction_costtempl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			CTransaction_costtempl rec = m_vecTransaction_costtempl[i];
			if (rec.getTemplateName().Compare(cost_tmpl_name) == 0)
				return rec.getID();
		}	// for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
	}	// if (m_vecTransaction_costtempl.size() > 0)
	return -1;
}

void CMDITemplateTraktFormView::getTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_bConnected = pDB->getTemplates(m_vecTransactionTemplate,ID_TEMPLATE_TRAKT);	// Get trakt templates; 070824 p�d
			delete pDB;
		}
	}
}

// Save template to database; 070824 p�d
BOOL CMDITemplateTraktFormView::saveTemplateToDB(short action)
{
	CString S;
	BOOL bIsXMLFile = FALSE;
	CStringArray arr;

	if (action > -1) 
		m_wndPropertyGrid.Refresh();

	BOOL bCanTraktFormViewClose = TRUE;
	BOOL bCanSpecieDataFormViewClose = TRUE;

	bCanTraktFormViewClose = isOKToClose();
	bCanSpecieDataFormViewClose = isSpeciePagesOKToClose(arr);

	CString sTemplName = m_wndEdit1.getText();

	// Status can be "Klar att anv�nda" or "Under utveckling"; 080304 p�d
	// If "Under utveckling", only check for a Name; 080304 p�d
	if (m_nStatus == ID_TEMPLATE_TRAKT_DEVELOPMENT)
	{
		// Check if user have entered a name for the Template.
		// If not, tell'em and get the hell out of this method; 070829 p�d
		if (m_wndEdit1.getText().IsEmpty())		// Name of Template
		{
			if (action == 1)	// From within this; 081027 p�d
			{
				::MessageBox(this->GetSafeHwnd(),(m_sMsgTmplNameMissing1),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
				return FALSE;
			}
			else if (action == 2) // From Close/SysCommand; 081027 p�d
				if (::MessageBox(this->GetSafeHwnd(),(m_sMsgTmplNameMissing2),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				{
					removeTemplateFromDB(FALSE);						
					return TRUE;
				}
				else
					return FALSE;
		}
		// Check that user haven't changed the name of the Template to a name
		// that already been used; 080411 p�d
		int nID = m_wndEdit1.getItemIntData();
		if (m_bInitialized2)
		{
			if (m_vecTransactionTemplate.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
				{
					if (sTemplName.CompareNoCase(m_vecTransactionTemplate[i].getTemplateName()) == 0 &&
							nID != m_vecTransactionTemplate[i].getID())	// Don't check the active item; 080407 p�d
					{
						::MessageBox(this->GetSafeHwnd(),(m_sMsgDuplicateTemplName+m_sMsgDontSaveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
						return FALSE;
					}
				}
			}
		}
	}
	else if (m_nStatus == ID_TEMPLATE_TRAKT)
	{
		CString sErrMsgCap;
		CString sErrMsg;
		CString sSpcErrMsg;

		// Check if user have entered a name for the Template.
		// If not, tell'em and get the hell out of this method; 070829 p�d
		if (m_wndEdit1.getText().IsEmpty() ||		// Name of Template
				m_sBindToExchPricelistStr.IsEmpty() ||	// Exchange calculation
				m_sBindToPricelistSelected.IsEmpty() ||	// Pricelist
				m_fBindToDiamClass == 0.0 ||
				!bCanTraktFormViewClose ||
				!bCanSpecieDataFormViewClose)
		{
			if (arr.GetCount() > 0)
			{
				for (int i = 0;i < arr.GetCount();i++)
				{
					sSpcErrMsg += arr.GetAt(i) + _T("\n");
				}
				sErrMsg.Format(_T("%s\n%s\n\n%s"),m_sMsgDataMissing,sSpcErrMsg,m_sMsgNotToBeSaved);
			}
			else
				sErrMsg.Format(_T("%s\n"),m_sMsgDataMissing,m_sMsgNotToBeSaved);

			sErrMsgCap.Format(_T("%s (%s)"),m_sMsgCap, m_wndEdit1.getText());
			::MessageBox(this->GetSafeHwnd(),(sErrMsg),(sErrMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}

		// Check that user haven't changed the name of the Template to a name
		// that already been used; 080411 p�d
		int nID = m_wndEdit1.getItemIntData();
		if (m_bInitialized2)
		{
			if (m_vecTransactionTemplate.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
				{
					if (sTemplName.CompareNoCase(m_vecTransactionTemplate[i].getTemplateName()) == 0 &&
							nID != m_vecTransactionTemplate[i].getID())	// Don't check the active item; 080407 p�d
					{
						::MessageBox(this->GetSafeHwnd(),(m_sMsgDuplicateTemplName+m_sMsgDontSaveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
						return FALSE;
					}	// if (sTemplName.CompareNoCase(m_vecTransactionTemplate[i].getTemplateName()) == 0 &&
				}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			}	// if (m_vecTransactionTemplate.size() > 0)
		}	// if (m_bInitialized2)

	}

	CTransaction_template tmpl;
	if (m_bConnected)
	{
		CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			// We need to create the XML-file; 070829 p�d
			bIsXMLFile = createTemplateXMLFile();
			if (m_nStatus == ID_TEMPLATE_TRAKT)
			{
				// If it's a "Klar att anv�nda", we need to check
				// if the xml-file realy have been created; 080304 p�d
				if (!bIsXMLFile) return FALSE;
			}

			tmpl = CTransaction_template(m_recActiveTemplate.getID(),
																	 sTemplName,
																	 m_nStatus,
																	 m_sTemplateXMLFile,
																	 m_wndEdit4.getText(), // note
																	 m_wndEdit2.getText());
			if (!pDB->addTemplate(tmpl))
			{
				pDB->updTemplate(tmpl);
				// Read Template created in Database; 070827 p�d
				getTemplatesFromDB();
			}
			else
			{

				// Read Template created in Database; 070827 p�d
				getTemplatesFromDB();
				// Get TypeOf pricelist in m_nDBIndex for m_vecTransactionTemplate and
				// set in setupExchangefunctionsInPropertyGrid; 070827 p�d
				getTemplatePricelistTypeOf();
					
				m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;				
				setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < m_vecTransactionTemplate.size());
				populateData(m_nDBIndex);
			}
			delete pDB;
		}
	}

	return TRUE;
}


BOOL CMDITemplateTraktFormView::removeTemplateFromDB(BOOL show_msg)
{
	BOOL bRemove = FALSE;
	// Check that we are in TEMPLATE_OPEN mode; 071029 p�d
	if (show_msg)
		if (::MessageBox(this->GetSafeHwnd(),(m_sMsgDelTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
			bRemove = TRUE;
		else return FALSE;
	else
		bRemove = TRUE;

	//if (m_enumTemplateState == TEMPLATE_OPEN)
	//{
		CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			if (bRemove)
			{
				if (pDB->delTemplate(m_recActiveTemplate))
				{
					getTemplatesFromDB();
					if (m_vecTransactionTemplate.size() > 0)
					{
						m_nDBIndex = m_vecTransactionTemplate.size() - 1;
						setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1));
					}
					else
					{
						m_nDBIndex = -1;
						setNavigationButtons(FALSE,FALSE);
					}
				}
			}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sMsgDelTemplate),_T(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
			delete pDB;
		}	// if (pDB != NULL)
	//}	// if (m_enumTemplateState == TEMPLATE_OPEN)

	return bRemove;
}

void CMDITemplateTraktFormView::populateData(UINT idx)
{
	TCHAR szTemplName[127];
	TCHAR szTemplDoneBy[127];
	TCHAR szTemplDate[127];
	TCHAR szTemplNotes[1024];
	TCHAR szTemplExchange[127];
	TCHAR szTemplPricelist[127];
	TCHAR szTemplateGrowthArea[127];
	TCHAR szTemplateSI_H100[127];
	TCHAR szTemplateSI_H100_Pine[127];
	int nCostsTmplID,nTemplPricelistTypeOf;
	TCHAR szTemplateCostsTmpl[127];
	BOOL bPriceListExists=FALSE;
	CString sNotes;
	int nID;

	if (m_vecTransactionTemplate.size() > 0&& 
		  idx >= 0 && 
			idx < m_vecTransactionTemplate.size())
	{

		
		setEnbaleWindows(TRUE);
		m_enumTemplateState = TEMPLATE_OPEN;	
	
		m_recActiveTemplate = m_vecTransactionTemplate[idx];

		TemplateParser *pars = new TemplateParser();
		if (pars != NULL)
		{
			if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
			{	
				szTemplName[0] = '\0';
				szTemplDoneBy[0] = '\0';
				szTemplDate[0] = '\0';
				szTemplNotes[0] = '\0';
				szTemplExchange[0] = '\0';
				szTemplPricelist[0] = '\0';
				nTemplPricelistTypeOf = -1;
				m_fBindToDiamClass = 0.0;
				m_nBindToHgtOverSea = 0.0;
				m_nBindToLatitude = 0.0;
				m_nBindToLongitude = 0.0;

				pars->getTemplateName(szTemplName);
				pars->getTemplateDoneBy(szTemplDoneBy);
				// Added 100114 p�d
				if (szTemplDoneBy == L"")
					_tcscpy(szTemplDoneBy,getUserName().MakeUpper());
				pars->getTemplateDate(szTemplDate);
				//----------------------------------------------------
				// pars->getTemplateNotes(szTemplNotes) added for
				// redmine #2659 20120808 P�D
				pars->getTemplateNotes(szTemplNotes);
				sNotes = szTemplNotes;
				sNotes.Replace(L"\n",L"\r\n");
				//�ndrat h�mtar prislistetyp fr�n echangetypen ist�llet f�r fr�n prislistexml-delen i best�ndsmallen d� den kanske inte �r vald �nd�
				//20120118 J� Bug #2679
				//pars->getTemplateExchange(&nID,szTemplExchange);
				//pars->getTemplatePricelistTypeOf(&nTemplPricelistTypeOf);
				pars->getTemplateExchange(&nTemplPricelistTypeOf,szTemplExchange);
				pars->getTemplatePricelist(&nID,szTemplPricelist);				
				pars->getTemplateDCLS(&m_fBindToDiamClass);
				pars->getTemplateHgtOverSea((int*)&m_nBindToHgtOverSea);
				pars->getTemplateLatitude((int*)&m_nBindToLatitude);
				pars->getTemplateLongitude((int*)&m_nBindToLongitude);
				pars->getTemplateGrowthArea(szTemplateGrowthArea);
				pars->getTemplateSI_H100(szTemplateSI_H100);
				pars->getTemplateCostsTmpl(&nCostsTmplID,szTemplateCostsTmpl);
				nCostsTmplID = getCostTmplID(szTemplateCostsTmpl); // Make sure cost template is not 'under development'
				if( nCostsTmplID < 0 ) szTemplateCostsTmpl[0] = '\0';

				// "S�derbergs" specific data; 071206 p�d
				int nValue;
				pars->getIsAtCoast(&nValue);
				m_bBindToIsAtCoast = (nValue == 1 ? TRUE : FALSE);
				
				pars->getIsSouthEast(&nValue);
				m_bBindToIsSouthEast = (nValue == 1 ? TRUE : FALSE);
				
				pars->getIsRegion5(&nValue);
				m_bBindToIsRegion5 = (nValue == 1 ? TRUE : FALSE);
				
				pars->getPartOfPlot(&nValue);
				m_bBindToIsPartOfPlot = (nValue == 1 ? TRUE : FALSE);

				pars->getSIH100_Pine(szTemplateSI_H100_Pine);
			}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))

			delete pars;

			m_wndEdit1.SetWindowText(szTemplName);
			m_wndEdit1.setItemIntData(m_recActiveTemplate.getID());
			m_wndEdit2.SetWindowText(szTemplDoneBy);
			m_wndEdit3.SetWindowText(szTemplDate);
//			m_wndEdit4.SetWindowText(m_recActiveTemplate.getTemplateNotes());
//			m_wndEdit4.SetWindowText(szTemplNotes);
			m_wndEdit4.SetWindowText(sNotes);
			m_sBindToExchPricelistStr = szTemplExchange;
			// Added for backward compatibility; 090423 p�d
			if (m_sBindToExchPricelistStr.Find(_T("(medelpriser)")) > 0)
					m_sBindToExchPricelistStr = _T("Medelprislista");
			if (m_sBindToExchPricelistStr.Find(_T("(prislista)")) > 0)
					m_sBindToExchPricelistStr = _T("Matrisprislista");
			
			m_sBindToPricelistSelected = szTemplPricelist;
			m_nPriceListTypeOf= nTemplPricelistTypeOf;

			bPriceListExists=getPricelist(m_sBindToPricelistSelected,m_nPriceListTypeOf);
			//S�tt upp prislister att v�lja mellan h�r annars populeras inte den om om tex typen �r skiljd fr�n f�reg�ende mall ingen prislista �r vald sedan f�rut.
			//20120118 J� Bug #2679
			if (m_wndPropertyGrid.GetSafeHwnd() && m_sBindToPricelistSelected.GetLength()<1)
			{
				CXTPPropertyGridItem* pItem = NULL;
				CXTPPropertyGridItems *pCategories = NULL; 
				pCategories =	m_wndPropertyGrid.GetCategories();
				if (pCategories != NULL)
				{
					pItem  = pCategories->GetAt(0);
					if(pItem!=NULL)
					{
						CXTPPropertyGridItems *pChilds;
						pChilds=pItem->GetChilds();
						if(pChilds!=NULL)
						{
							CXTPPropertyGridItem* pChild;
							pChild=pChilds->GetAt(0);
							if(pChild!=NULL)
							{
								setupExchangefunctionsInPropertyGrid(pChild);
							}
						}
					}
				}
			}

			m_sBindToGrowthArea = szTemplateGrowthArea;
			m_sBindToSIH100 = szTemplateSI_H100;
			m_sBindToCostTmpl = szTemplateCostsTmpl;

			m_sBindToSIH100_pine = szTemplateSI_H100_Pine;

			// Need to do this to realize data in ProertyGrid; 070827 p�d
			m_wndPropertyGrid.Refresh();
     		m_nStatus = m_recActiveTemplate.getTypeOf();
			
			if( nCostsTmplID < 0 ) m_nStatus = ID_TEMPLATE_TRAKT_DEVELOPMENT; // Set status to 'under development' if cost template is missing;
	
			// Set status to 'under development' if pricelist is missing 20120112 J� Bug#2679
			if( bPriceListExists ==FALSE )
			{
				m_nStatus = ID_TEMPLATE_TRAKT_DEVELOPMENT; 
				// Clear/Remove tabs in TabControl; 080707 p�d
				if (m_wndTabControl.getNumOfTabPages() > 0)
				{
					for (int ii = m_wndTabControl.getNumOfTabPages()-1;ii >= 0;ii--)
					{
						m_wndTabControl.getTabPage(ii)->Remove();
					}
					m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
				}
				m_sMsgCantFindPricelist=m_sMsgCantFindPricelist+_T("\n")+m_sBindToPricelistSelected;
				m_sBindToPricelistSelected = _T("");
				::MessageBox(this->GetSafeHwnd(),(m_sMsgCantFindPricelist),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			}
			else
			{
			setTabsForSpeciesInSelectedPricelist();
			setFunctionsInTabsForSpeciesForSelectedPricelist();
			}			
			
			// Setup status; 080304 p�d
			if (m_nStatus == ID_TEMPLATE_TRAKT)
				m_wndCBox1.SetCurSel(0);	// "Klar att anv�nda"
			else if (m_nStatus == ID_TEMPLATE_TRAKT_DEVELOPMENT)
				m_wndCBox1.SetCurSel(1);	// "Under utveckling"

			// Lock/UnLock SpecieDataFormView(s); 100602 p�d
			m_wndEdit1.EnableWindow(!(m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit1.SetReadOnly((m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit2.EnableWindow(!(m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit2.SetReadOnly((m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit3.EnableWindow(!(m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit3.SetReadOnly((m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit4.EnableWindow(!(m_nStatus == ID_TEMPLATE_TRAKT));
			m_wndEdit4.SetReadOnly((m_nStatus == ID_TEMPLATE_TRAKT));
			if (m_wndTabControl.getNumOfTabPages() > 0)
			{
				for (int ii = 0;ii < m_wndTabControl.getNumOfTabPages();ii++)
				{
					m_tabManager = m_wndTabControl.getTabPage(ii);
					if (m_tabManager)
					{
						CSpecieDataFormView* pView = DYNAMIC_DOWNCAST(CSpecieDataFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
						if (pView)	
							pView->setLockSpecieDataView(m_nStatus == ID_TEMPLATE_TRAKT);
					}	// if (m_tabManager)
				}	// for (int ii = 0;ii < m_wndTabControl.getNumOfTabPages()-1;ii++)
			} // if (m_wndTabControl.getNumOfTabPages() > 0)

			setLockTemplateFormView(m_nStatus == ID_TEMPLATE_TRAKT);
		}
	}
	else
	{
		clearData();
		setEnbaleWindows(FALSE);
		//---------------------------------------------------------------------------------
		// Clear/Remove tabs in TabControl; 080707 p�d
		if (m_wndTabControl.getNumOfTabPages() > 0)
		{
			for (int ii = m_wndTabControl.getNumOfTabPages()-1;ii >= 0;ii--)
			{
				m_wndTabControl.getTabPage(ii)->Remove();
			}
			m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
		}
		setLockTemplateFormView(TRUE);

		m_enumTemplateState = TEMPLATE_NONE;	
	}

	CMDITemplateTraktFormFrame *pFrame = (CMDITemplateTraktFormFrame*)getFormViewByID(IDD_FORMVIEW)->GetParent();
	if (pFrame != NULL)
		pFrame->setEnableTBtns(m_vecTransactionTemplate.size() > 0);

	doSetNavigationBar();
}

BOOL CMDITemplateTraktFormView::getPricelist(CString csPrlName,int nPrlTypeOf)
{
	//Kolal om det �r n�gon prislista vald
	if(csPrlName.GetLength()<=0)
		return TRUE;
	getPricelistsInDB();
	// Add pricelists for Exchange calculation model selected; 070430 p�d
	if (m_vecTransactionPricelist.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
		{
			CTransaction_pricelist prl = m_vecTransactionPricelist[i];

			if ((nPrlTypeOf == prl.getTypeOf()) && (csPrlName.Compare(prl.getName()) == 0))
			{
				return TRUE;
			}
		}	
	}
	return FALSE;
}


void CMDITemplateTraktFormView::doPouplate(UINT idx)
{
	m_nDBIndex = idx;
	populateData(idx);
}

// Added 2008-11-28 p�d
// Reload templates and set to last entry.
// Used e.g. in CMDITemplateTraktFormFrame::OnImportStandTemplate(void)
void CMDITemplateTraktFormView::doPouplateFromLastEntry(void)
{
	// Read Template created in Database; 081128 p�d
	getTemplatesFromDB();

	// Check if there's any templates in database.
	// If not, set mode to TEMPLATE_OPEN; 081128 p�d
	if (m_vecTransactionTemplate.size() > 0)
	{
		m_nDBIndex = m_vecTransactionTemplate.size() - 1;
		m_enumTemplateState = TEMPLATE_OPEN;	// Items in database; 081127 p�d
		populateData(m_nDBIndex);
		setNavigationButtons(m_nDBIndex > 0,FALSE);
	}
}

void CMDITemplateTraktFormView::doSaveAndPouplateLastEntry(void)
{
	if (m_nDBIndex >= 0 && m_nDBIndex < m_vecTransactionTemplate.size())
	{
		saveTemplateToDB(-2);
		m_recActiveTemplate = m_vecTransactionTemplate[m_nDBIndex];
	}
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDITemplateTraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

// Events controld by Database navigation bar in HMSShell; 060112 p�d

LRESULT CMDITemplateTraktFormView::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	int nIndex=-1;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;

	if (wParam == XTP_PGN_ITEMVALUE_CHANGED)
	{
		if (pItem->GetID() == ID_EXCH_FUNC_CB)
		{
			//matris=index=0 eller medel=intdex =1 som �r vald s�tt om typ av prislista 20120119 J� Bug #2679
			CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
			if (pItemConstraints != NULL)
			{
				nIndex = pItemConstraints->GetCurrent();
			} //	if (pItemConstraints != NULL)

			m_nPriceListTypeOf=nIndex+1;

			//---------------------------------------------------------------------------------
			// Clear/Remove tabs in TabControl; 070808 p�d
			if (m_wndTabControl.getNumOfTabPages() > 0)
			{
				for (int ii = m_wndTabControl.getNumOfTabPages()-1;ii >= 0;ii--)
				{
					m_wndTabControl.getTabPage(ii)->Remove();
				}
				m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
			}
			setupExchangefunctionsInPropertyGrid(pItem);
		}

		if (pItem->GetID() == ID_PRL_FUNC_CB)
		{			

			setTabsForSpeciesInSelectedPricelist();
			setFunctionsInTabsForSpeciesForSelectedPricelist();
		}

		if (pItem->GetID() == ID_MISC_TEMPL_SI_H100)
		{
			if (m_sBindToSIH100.FindOneOf(_T("TGB")) == -1)
			{
				m_sBindToSIH100.Empty();
				m_wndPropertyGrid.Refresh();
				::MessageBox(this->GetSafeHwnd(),(m_sMsgWrongSIH100Entry),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
				pItem->SetFocusToInplaceControl();
			}
		}
	}


	return FALSE;
}

void CMDITemplateTraktFormView::setupMiscDataInPropertyGrid(CXTPPropertyGridItem *pItem)
{
//	CXTPPropertyGridItemDouble *pDiamClass = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sDiamClass + _T("*"),0.0,_T("%.1f")));
	// Added 081126 p�d
	
	//CXTPPropertyGridItemDouble *pDiamClass = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CMyPropGridItemDouble_validate(m_sDiamClass + _T("*"),_T("%.2f"),
	//m_sMsgCap,m_arrValidateMsg,0.1,VALUE_GT));
	//#4663 20151105 J�
	CXTPPropertyGridItemDouble *pDiamClass = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CMyPropGridItemDouble_validate(m_sDiamClass + _T("*"),_T("%.0f"), 
	m_sMsgCap,m_arrValidateMsg,DEF_MIN_DIAMCLASS,VALUE_GE,TRUE,TRUE));

	pDiamClass->SetID(ID_MISC_TEMPL_DCLS);
	pDiamClass->BindToDouble(&m_fBindToDiamClass);

	CXTPPropertyGridItemNumber *pHgtOverSea = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber(m_sHgtOverSea + _T("*")));
	pHgtOverSea->SetID(ID_MISC_TEMPL_HGT_OVER_SEA);
	pHgtOverSea->BindToNumber(&m_nBindToHgtOverSea);

	CXTPPropertyGridItemNumber *pLatitude = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber(m_sLatitude + _T("*")));
	pLatitude->SetID(ID_MISC_TEMPL_LATITUDE);
	pLatitude->BindToNumber(&m_nBindToLatitude);

	CXTPPropertyGridItemNumber *pLongitude = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber(m_sLongitude));
	pLongitude->SetID(ID_MISC_TEMPL_LONGITUDE);
	pLongitude->BindToNumber(&m_nBindToLongitude);

/*	COMMENTED OUT 2008-04-08 P�D
		Don't use GrowthArea "Tillv�xtomr�de" in "TraktMall"; 080408 p�d
	CXTPPropertyGridItem *pGrowthArea = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem(_T(m_sGrowthArea)));
	pGrowthArea->SetFlags(xtpGridItemHasComboButton);
	pGrowthArea->SetConstraintEdit( FALSE );
	pGrowthArea->SetID(ID_MISC_TEMPL_GROWTH_AREA);
	pGrowthArea->BindToString(&m_sBindToGrowthArea);
	pGrowthArea->GetConstraints()->AddConstraint(_T(GROWTH_AREA_1),0);
	pGrowthArea->GetConstraints()->AddConstraint(_T(GROWTH_AREA_2),1);
	pGrowthArea->GetConstraints()->AddConstraint(_T(GROWTH_AREA_3),2);
	pGrowthArea->GetConstraints()->AddConstraint(_T(GROWTH_AREA_4),3);
	pGrowthArea->GetConstraints()->AddConstraint(_T(GROWTH_AREA_5),4);
	pGrowthArea->GetConstraints()->AddConstraint(_T(GROWTH_AREA_6),5);
*/
	// Just a default value for GrowthArea, NOT USED; 080408 p�d
	m_sBindToGrowthArea = (GROWTH_AREA_1);

	CXTPPropertyGridItem *pSIH100 = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem(m_sSI_H100 + _T("*")));
	pSIH100->SetMask(_T(">99"),_T("___"));
	pSIH100->SetID(ID_MISC_TEMPL_SI_H100);
	pSIH100->BindToString(&m_sBindToSIH100);

	
	CXTPPropertyGridItem *pCostsTmpl = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem(m_sCostTmpl + _T("*")));
	pCostsTmpl->SetFlags(xtpGridItemHasComboButton);
	pCostsTmpl->SetConstraintEdit( FALSE );
	pCostsTmpl->SetID(ID_MISC_TEMPL_COSTS);
	pCostsTmpl->BindToString(&m_sBindToCostTmpl);
	// Get cost templates from table and
	// add as constraints; 071010 p�d
	getCostTmplInDB();
	if (m_vecTransaction_costtempl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			CTransaction_costtempl rec = m_vecTransaction_costtempl[i];
			pCostsTmpl->GetConstraints()->AddConstraint((rec.getTemplateName()),rec.getID());
		}
	}
}

void CMDITemplateTraktFormView::setupSoderbergsDataInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItemBool *pExtraInfo1 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sAtCoast + _T("*"),FALSE));
	pExtraInfo1->SetID(ID_ATCOAST);
	pExtraInfo1->BindToBool(&m_bBindToIsAtCoast);

	CXTPPropertyGridItemBool *pExtraInfo2 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sSouthEast + _T("*"),FALSE));
	pExtraInfo2->SetID(ID_SOUTHEAST);
	pExtraInfo2->BindToBool(&m_bBindToIsSouthEast);

	CXTPPropertyGridItemBool *pExtraInfo3 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sRegion5 + _T("*"),FALSE));
	pExtraInfo3->SetID(ID_REGION5);
	pExtraInfo3->BindToBool(&m_bBindToIsRegion5);

	CXTPPropertyGridItemBool *pExtraInfo4 = (CXTPPropertyGridItemBool *)pItem->AddChildItem(new CMyPropGridItemBool(m_sYes,m_sNo,m_sPartOfPlot + _T("*"),FALSE));
	pExtraInfo4->SetID(ID_PART_OF_PLOT);
	pExtraInfo4->BindToBool(&m_bBindToIsPartOfPlot);

	CXTPPropertyGridItem *pSIH100_pine = (CXTPPropertyGridItem *)pItem->AddChildItem(new CXTPPropertyGridItem(m_sSIH100_pine + _T("*")));
	pSIH100_pine->SetID(ID_SIH100_PINE);
	pSIH100_pine->BindToString(&m_sBindToSIH100_pine);
	pSIH100_pine->SetMask(_T("T00"),_T("T__"));

}

void CMDITemplateTraktFormView::setupPropertyGrid(void)
{
	CXTPPropertyGridItem* pItem = NULL;
	CXTPPropertyGridItems *pCategories = NULL; 
	if (m_wndPropertyGrid.GetSafeHwnd())
	{
		pCategories =	m_wndPropertyGrid.GetCategories();
		if (pCategories != NULL)
		{
			pItem  = pCategories->GetAt(0);
			if (pItem != NULL) setupTypeOfExchangefunctionsInPropertyGrid(pItem);
			pItem  = pCategories->GetAt(1);
			if (pItem != NULL) setupMiscDataInPropertyGrid(pItem);
			pItem  = pCategories->GetAt(2);
			if (pItem != NULL) setupSoderbergsDataInPropertyGrid(pItem);
		}
	}
}

void CMDITemplateTraktFormView::setupTypeOfExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CString sInfo,S;
	UCFunctions flist;
	if (pItem->HasChilds())
	{
		pItem->GetChilds()->Clear();
	}
	getTemplatePricelistTypeOf();
	// get exchange functions from UCCalculate.dll modules in
	// ..\Module directory; 070413 p�d
	getExchangeFunctions(m_vecExchangeFunc);
	if (m_vecExchangeFunc.size() > 0)
	{
		CXTPPropertyGridItem *pExchangeItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sExchange + _T("*")));
		if (pExchangeItem != NULL)
		{
			pExchangeItem->BindToString(&m_sBindToExchPricelistStr);

			for (UINT j = 0;j < m_vecExchangeFunc.size();j++)
			{
				flist = m_vecExchangeFunc[j];
				sInfo.Format(_T("%s"),flist.getName());
				pExchangeItem->GetConstraints()->AddConstraint((sInfo),j);
			}	// for (UINT j = 0;j < func_list.size();j++)

			pExchangeItem->SetFlags(xtpGridItemHasComboButton);
			pExchangeItem->SetConstraintEdit( FALSE );
			pExchangeItem->SetID(ID_EXCH_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
//			pExchangeItem->GetConstraints()->SetCurrent(m_nPriceListTypeOf);
			setupExchangefunctionsInPropertyGrid(pExchangeItem);
		}	// for (UINT j = 0;j < func_list.size();j++)
	}	// if (func_list.size() > 0)
}

// This function is called from OnGridNotify
// Actaully adds pricelists for selected exchange functions; 070507 p�d
void CMDITemplateTraktFormView::setupExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	UCFunctions list;
	CString sInfo;
	int nIndex;
	CXTPPropertyGridItem *pExchangeItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;

	//nIndex = m_nPriceListTypeOf;
	CString csTest=_T("");
	csTest=pItem->GetCaption();
	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)

	if (nIndex < 0 || m_sBindToPricelistSelected.GetLength()<1)
		nIndex = m_nPriceListTypeOf-1;
	else
		m_nPriceListTypeOf=nIndex+1;

	if (!pItem->HasChilds())
	{
		pExchangeItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sPricelist + _T("*")));
		pExchangeItem->BindToString(&m_sBindToPricelistSelected);



	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pExchangeItem = pGridChilds->GetAt(0);
			m_sBindToPricelistSelected = "";

			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pExchangeItem != NULL)
	{
		pItem->Expand();
		pExchangeItem->GetConstraints()->RemoveAll();
		if (nIndex >= 0  && nIndex < m_vecExchangeFunc.size())
		{
			list = m_vecExchangeFunc[nIndex];
		}
		getPricelistsInDB();
		// Add pricelists for Exchange calculation model selected; 070430 p�d
		if (m_vecTransactionPricelist.size() > 0)
		{
			for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
			{
				CTransaction_pricelist prl = m_vecTransactionPricelist[i];
		
				// Check the Function identifer; 070416 p�d
				if (list.getIdentifer() == prl.getTypeOf())
				{
					sInfo.Format(_T("%s"),prl.getName());
					pExchangeItem->GetConstraints()->AddConstraint((sInfo),i);
				}	// if (list.getID() == nFunctionIdentifer)
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
		}	// if (m_vecVolFuncList.size() > 0)

		pExchangeItem->SetFlags(xtpGridItemHasComboButton);
		pExchangeItem->SetConstraintEdit( FALSE );
		pExchangeItem->SetID(ID_PRL_FUNC_CB);
	} // 	if (pBarkItem != NULL)

}

BOOL CMDITemplateTraktFormView::isTemplateNameOK()
{
	CString sTemplName = m_wndEdit1.getText();

	int nID = m_recActiveTemplate.getID();
	if (m_vecTransactionTemplate.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		{
			if (sTemplName.CompareNoCase(m_vecTransactionTemplate[i].getTemplateName()) == 0 &&
					nID != m_vecTransactionTemplate[i].getID())	// Don't check the active item; 080407 p�d
			{
				::MessageBox(this->GetSafeHwnd(),(m_sMsgDuplicateTemplName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
				m_wndEdit1.SetWindowText((m_recActiveTemplate.getTemplateName()));
				m_wndEdit1.SetFocus();
				return FALSE;
			}
		}
	}
	return TRUE;
}

// Added 071031 p�d
// When m_wndEdit loses focus, check that the
// name of the Template doesn't already exist; 071031 p�d
void CMDITemplateTraktFormView::OnEnKillfocusEdit1()
{
}
// Setup m_nStatus on change selection; 080304 p�d
void CMDITemplateTraktFormView::OnCbnSelchangeCombo1()
{
	int nSelSpecies = m_wndTabControl.GetCurSel();	//#4519 sparar undan vilket tr�dslag som �r aktivt
	int nStatus = m_wndCBox1.GetCurSel();
	m_nOldStatus=m_nStatus;

	if (nStatus != CB_ERR)
	{
		if (nStatus == 0)	// "Klar att anv�nda"
			m_nStatus = ID_TEMPLATE_TRAKT;
		else if (nStatus == 1)	// "Under utveckling"
			m_nStatus = ID_TEMPLATE_TRAKT_DEVELOPMENT;
	}
	else
		m_nStatus = ID_TEMPLATE_TRAKT_DEVELOPMENT;

	//Lagt till variabel f�r att h�lla reda p� om data skall kollas och sparas eller ej
	//J� 20111017 Bug #2443, s�ttar att den skall sparas oms tatus g�r fr�n klar till under utv. 
	if(!m_bDoSave)
		if(m_nOldStatus==ID_TEMPLATE_TRAKT && m_nStatus==ID_TEMPLATE_TRAKT_DEVELOPMENT)
			m_bDoSave=TRUE;

	// If user selects "Klar att anv�nda" and there's
	// data missing, tell user and set back to "Under utveckling"; 080418 p�d
	if (m_nStatus == ID_TEMPLATE_TRAKT)
	{
		CString	sSpcErrMsg;
		CString sErrMsg;
		CString sErrMsgCap;
		CStringArray arr;
		BOOL bCanTraktFormViewClose = isOKToClose();
		BOOL bCanSpecieDataFormViewClose = isSpeciePagesOKToClose(arr);

		// Check if user have entered a name for the Template.
		// If not, tell'em and get the hell out of this method; 070829 p�d
		if (m_wndEdit1.getText().IsEmpty() ||		// Name of Template
				m_sBindToExchPricelistStr.IsEmpty() ||	// Exchange calculation
				m_sBindToPricelistSelected.IsEmpty() ||	// Pricelist
				m_fBindToDiamClass == 0.0 ||
				!bCanTraktFormViewClose ||
				!bCanSpecieDataFormViewClose)
		{
			if (arr.GetCount() > 0)
			{
				for (int i = 0;i < arr.GetCount();i++)
				{
					sSpcErrMsg += arr.GetAt(i) + _T("\n");
				}
				sErrMsg.Format(_T("%s\n%s\n\n%s"),m_sMsgDataMissing,sSpcErrMsg,m_sMsgNotToBeSaved);
			}
			else
				sErrMsg.Format(_T("%s\n"),m_sMsgDataMissing,m_sMsgNotToBeSaved);

			
			sErrMsgCap.Format(_T("%s (%s)"),m_sMsgCap,m_wndEdit1.getText());
			::MessageBox(this->GetSafeHwnd(),(sErrMsg),(sErrMsgCap),MB_ICONEXCLAMATION | MB_OK);
			m_wndCBox1.SetCurSel(1);	// "Under utveckling"; 080418 p�d
			// Set sataus back to "Under utveckling";	080418 p�d
			m_nStatus = ID_TEMPLATE_TRAKT_DEVELOPMENT;
		}

	}

	saveTemplateToDB(1);
	getTemplatesFromDB();
	populateData(m_nDBIndex);
	m_wndTabControl.SetCurSel(nSelSpecies);	//#4519 s�tt aktivt tr�dslag
}

void CMDITemplateTraktFormView::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());

	}
}
