// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#define ZIP_ARCHIVE_MFC

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <SQLAPI.h> // main SQLAPI++ header

#include <vector>
#include <map>

#define _USE_MATH_DEFINES
#include <math.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

// ... in PAD_HMSFuncLib
#include "pad_hms_miscfunc.h"
#include "pricelistparser.h"	
#include "templateparser.h"	
#include "CostsTmplParser.h"
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"

#define MSG_IN_SUITE				 					(WM_USER + 10)		// This identifer's used to send messages internally

#define IDR_GENERAL_INFO_PANE					0x9001

#define IDC_TABCONTROL								0x9100
#define IDC_REPORT										0x9101
#define IDC_TEMPLATE_LIST							0x9102

#define ID_MISC_TEMPL_NAME						0x9003
#define ID_MISC_TEMPL_DONE_BY					0x9004
#define ID_MISC_TEMPL_DATE						0x9005
#define ID_MISC_TEMPL_DCLS						0x9006
#define ID_MISC_TEMPL_HGT_OVER_SEA		0x9007
#define ID_MISC_TEMPL_LATITUDE				0x9008
#define ID_MISC_TEMPL_GROWTH_AREA			0x9009
#define ID_MISC_TEMPL_SI_H100					0x9010
#define ID_MISC_TEMPL_CORR_FACTOR			0x9011
#define ID_MISC_TEMPL_COSTS						0x9012
#define ID_MISC_TEMPL_LONGITUDE				0x9013

#define ID_EXCH_FUNC_CB								0x9107
#define ID_PRL_FUNC_CB								0x9108
#define ID_QDESC_FUNC_CB							0x9109

#define ID_HGT_TYPEOF_FUNC_CB					0x9110		// Type of height functions (e.g. H25,Regression,S�derbergs)
#define ID_HGT_FUNC_CB								0x9111		// Which functions to use witin e.g. H25, Regression, S�derbergs etc

#define ID_VOL_TYPEOF_FUNC_CB					0x9112		// Type of volume functions (e.g. Brandels,N�slunds)
#define ID_VOL_FUNC_CB								0x9113		// Which functions to use witin e.g. Brandels, N�slunds etc

#define ID_BARK_TYPEOF_FUNC_CB				0x9114		// Type of bark functions (e.g. Jonsson/Ostlin, Skogforsk, S�derbergs)
#define ID_BARK_FUNC_CB								0x9115		// Which functions to use witin e.g. Jonsson/Ostlin, Skogforsk, S�derbergs etc

#define ID_VOL_UB_TYPEOF_FUNC_CB			0x9116		// Type of volume functions (e.g. Brandels,N�slunds)
#define ID_VOL_UB_FUNC_CB							0x9117		// Which functions to use witin e.g. Brandels, N�slunds etc

#define ID_M3SK_TO_M3FUB							0x9118
#define ID_M3FUB_TO_M3TO							0x9119
#define ID_M3SK_TO_M3UB								0x9120
#define ID_ATCOAST										0x9121
#define ID_SOUTHEAST									0x9122
#define ID_REGION5										0x9123
#define ID_PART_OF_PLOT								0x9124
#define ID_H25												0x9125
#define ID_TRANSFERS									0x9126
#define ID_SIH100_PINE								0x9127
#define ID_TRANSP_DIST1								0x9128
#define ID_TRANSP_DIST2								0x9129
#define ID_GREENCROWN									0x9130

#define ID_PROPERTY_GRID1							0x9140						// ID for PropertyGrid holding general data; 081126 p�d
#define ID_PROPERTY_GRID2							0x9141						// ID for PropertyGrid holding specie-specific data; 081126 p�d

#define ID_GROT_TYPEOF_FUNC_CB				0x9142		
#define ID_GROT_PERCENT								0x9143		
#define ID_GROT_PRICE									0x9144		
#define ID_GROT_COST									0x9145		

/////////////////////////////////////////////////////////////////////
// String id
#define IDS_STRING100								100
#define IDS_STRING110								110
#define IDS_STRING111								111
#define IDS_STRING112								112
#define IDS_STRING113								113
#define IDS_STRING114								114
#define IDS_STRING115								115
#define IDS_STRING116								116
#define IDS_STRING117								117
#define IDS_STRING118								118

#define IDS_STRING1001							1001
#define IDS_STRING1002							1002
#define IDS_STRING1003							1003
#define IDS_STRING1004							1004
#define IDS_STRING1005							1005
#define IDS_STRING1006							1006
#define IDS_STRING1007							1007
#define IDS_STRING1008							1008
#define IDS_STRING1010							1010
#define IDS_STRING1011							1011

#define IDS_STRING1100							1100
#define IDS_STRING1101							1101
#define IDS_STRING1102							1102
#define IDS_STRING1103							1103

#define IDS_STRING200								200
#define IDS_STRING2001							2001
#define IDS_STRING2002							2002
#define IDS_STRING2003							2003

#define IDS_STRING201								201
#define IDS_STRING2010							2010
#define IDS_STRING2011							2011
#define IDS_STRING2012							2012
#define IDS_STRING2013							2013
#define IDS_STRING2014							2014
#define IDS_STRING2015							2015
#define IDS_STRING2016							2016
#define IDS_STRING2017							2017

#define IDS_STRING202								202
#define IDS_STRING2020							2020

#define IDS_STRING203								203
#define IDS_STRING2030							2030
#define IDS_STRING2031							2031
#define IDS_STRING2032							2032
#define IDS_STRING2033							2033
#define IDS_STRING2034							2034
#define IDS_STRING2035							2035
#define IDS_STRING2036							2036
#define IDS_STRING2037							2037

#define IDS_STRING204								204
#define IDS_STRING205								205

#define IDS_STRING206								206
#define IDS_STRING2060							2060

#define IDS_STRING207								207
#define IDS_STRING2070							2070
#define IDS_STRING2071							2071
#define IDS_STRING2072							2072
#define IDS_STRING2073							2073

#define IDS_STRING208								208
#define IDS_STRING209								209
#define IDS_STRING210								210
#define IDS_STRING211								211

#define IDS_STRING212								212
#define IDS_STRING213								213

#define IDS_STRING214								214
#define IDS_STRING2150							2150
#define IDS_STRING2151							2151
#define IDS_STRING2152							2152
#define IDS_STRING2153							2153
#define IDS_STRING2154							2154

#define IDS_STRING216								216

#define IDS_STRING217								217

#define IDS_STRING2180							2180
#define IDS_STRING2181							2181
#define IDS_STRING2182							2182
#define IDS_STRING2183							2183

#define IDS_STRING2184							2184

#define IDS_STRING2190							2190
#define IDS_STRING2191							2191
#define IDS_STRING2192							2192
#define IDS_STRING2193							2193
#define IDS_STRING2194							2194
#define IDS_STRING2195							2195
#define IDS_STRING2196							2196
#define IDS_STRING2197							2197

#define IDS_STRING220								220
#define IDS_STRING2200							2200
#define IDS_STRING2201							2201
#define IDS_STRING2202							2202
#define IDS_STRING2203							2203
#define IDS_STRING2204							2204

#define IDS_STRING221								221
#define IDS_STRING2210							2210
#define IDS_STRING2211							2211

#define IDS_STRING2300							2300
#define IDS_STRING2301							2301
#define IDS_STRING2302							2302
#define IDS_STRING2303							2303

#define IDS_STRING240								240
#define IDS_STRING2400							2400
#define IDS_STRING2401							2401
#define IDS_STRING2402							2402
#define IDS_STRING2403							2403
#define IDS_STRING2404							2404

#define IDS_STRING2410							2410
#define IDS_STRING2411							2411
#define IDS_STRING2412							2412
#define IDS_STRING2413							2413
#define IDS_STRING2414							2414

#define IDS_STRING250								250
#define IDS_STRING2500							2500
#define IDS_STRING2501							2501

#define IDS_STRING251								251
#define IDS_STRING2510							2510
#define IDS_STRING2511							2511
#define IDS_STRING2512							2512
#define IDS_STRING2513							2513

#define IDS_STRING2520							2520
#define IDS_STRING2521							2521
#define IDS_STRING2522							2522
#define IDS_STRING2523							2523

#define IDS_STRING2530							2530
#define IDS_STRING2531							2531
#define IDS_STRING2532							2532

#define IDS_STRING2540							2540

#define IDS_STRING260								260
#define IDS_STRING2600							2600
#define IDS_STRING2601							2601
#define IDS_STRING2602							2602
#define IDS_STRING2603							2603

#define IDS_STRING2610							2610
#define IDS_STRING2611							2611

// #4582
#define IDS_STRING262							262
#define IDS_STRING263							263
#define IDS_STRING264							264

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3
};

//////////////////////////////////////////////////////////////////////////////////////////
// Type of cost template; 071008 p�d
#define COST_TYPE_1									1	// "Kostnader f�r Rotpost"
#define COST_TYPE_2									2	// "Kostnader f�r Rotpost i LandValue"

//////////////////////////////////////////////////////////////////////////////////////////
// Register keys

const LPCTSTR REG_TEMPLATE_TRAKT_ENTRY_KEY						= _T("UMTemplate\\TemplateTraktEntry\\Placement");

const LPCTSTR REG_TEMPLATE_TRAKT_LIST_ENTRY_KEY				= _T("UMTemplate\\TemplateTraktListEntry\\Placement");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of the Window
const int MIN_X_SIZE										= 670;
const int MIN_Y_SIZE										= 370;

const int MIN_X_SIZE_LIST								= 350;
const int MIN_Y_SIZE_LIST								= 200;

// Use this root key in my own registry settings; 070628 p�d
const LPCTSTR PROGRAM_NAME						= _T("UMTemplates");				// Used for Languagefile, registry entries etc.; 070628 p�d


const LPCTSTR SQL_SERVER_SQRIPT_DIRECTORY			= _T("Microsoft SQL Server");

const LPCTSTR TEMPLATE_TABLES									= _T("Create_templ_template_table.sql");
//////////////////////////////////////////////////////////////////////////////////////////
// Name of tables in database; 070808 p�d
const LPCTSTR TBL_PRICELISTS								= _T("prn_pricelist_table");
const LPCTSTR TBL_TEMPLATE									= _T("tmpl_template_table");
const LPCTSTR TBL_COSTS_TEMPLATE						= _T("cost_template_table");


//////////////////////////////////////////////////////////////////////////////////////////
//	Growth areas (Tillv�xtomr�den); 070809 p�d
const LPCTSTR GROWTH_AREA_1					= _T("I");
const LPCTSTR GROWTH_AREA_2					= _T("II");
const LPCTSTR GROWTH_AREA_3					= _T("III");
const LPCTSTR GROWTH_AREA_4					= _T("IV");
const LPCTSTR GROWTH_AREA_5					= _T("V");
const LPCTSTR GROWTH_AREA_6					= _T("VI");

//////////////////////////////////////////////////////////////////////////////////////////
//
const TCHAR sz0dec[] = _T("%.0f");
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");
const TCHAR sz3dec[] = _T("%.3f");

//////////////////////////////////////////////////////////////////////////////////////////
// Identifies type of template; 070824 p�d
// 1 = Trakt	2 = Objekt etc.
#define ID_TEMPLATE_TRAKT								 1
#define ID_TEMPLATE_TRAKT_DEVELOPMENT		-1
#define ID_TEMPLATE_OBJECT							 2
#define ID_TEMPLATE_OBJECT_DEVELOPMENT	-2

//////////////////////////////////////////////////////////////////////////////////////////
// Deafult diameterclass; 080411 p�d
#define DEF_DIAMCLASS					2.0
#define DEF_MIN_DIAMCLASS				1.0

//////////////////////////////////////////////////////////////////////////////////////////
// Identifer for constant for "omf�ringstal" m3sk to m3ub; 071024 p�d
#define ID_BRANDELS_UB				1100
#define ID_NASLUNDS_UB				1101
#define ID_HAGBERG_MATERN_UB	1102
#define	ID_OMF_FACTOR_UB			3000
//////////////////////////////////////////////////////////////////////////////////////////
// this enuerated type is used to set Action: New or Update; 060329 p�d
typedef enum {	
								TEMPLATE_NONE,
								TEMPLATE_NEW,
								TEMPLATE_OPEN,
						 } enumTemplateState;

//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d
const LPCTSTR TOOLBAR_RES_DLL			= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091014 p�d

const int RES_TB_IMPORT						= 19;
const int RES_TB_EXPORT						= 9;

//////////////////////////////////////////////////////////////////////////////////////////
// Tags for XML template file; 070808 p�d
const LPCTSTR TAG_FIRST_ROW										= _T("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

const LPCTSTR TAG_TEMPLATE_START								= _T("<template>");
const LPCTSTR TAG_TEMPLATE_END									= _T("</template>");

const LPCTSTR TAG_TEMPLATE_NAME								= _T("<template_name>%s</template_name>");
const LPCTSTR TAG_TEMPLATE_DONE_BY							= _T("<template_done_by>%s</template_done_by>");
const LPCTSTR TAG_TEMPLATE_DATE								= _T("<template_date>%s</template_date>");
const LPCTSTR TAG_TEMPLATE_NOTES							= _T("<template_notes>%s</template_notes>");

const LPCTSTR TAG_TEMPLATE_EXCHANGE						= _T("<template_exchange id=\"%d\" name=\"%s\"/>");
const LPCTSTR TAG_TEMPLATE_PRICELIST						= _T("<template_pricelist id=\"%d\" type_of=\"%d\" name=\"%s\"/>");

const LPCTSTR TAG_TEMPLATE_DCLS								= _T("<template_dcls>%.2f</template_dcls>");
const LPCTSTR TAG_TEMPLATE_HGT_OVER_SEA				= _T("<template_hgt_over_sea>%d</template_hgt_over_sea>");
const LPCTSTR TAG_TEMPLATE_LATITUDE						= _T("<template_latitude>%d</template_latitude>");
const LPCTSTR TAG_TEMPLATE_LONGITUDE					= _T("<template_longitude>%d</template_longitude>");
const LPCTSTR TAG_TEMPLATE_GROWTH_AREA					= _T("<template_growth_area>%s</template_growth_area>");
const LPCTSTR TAG_TEMPLATE_SI_H100							= _T("<template_si_h100>%s</template_si_h100>");
const LPCTSTR TAG_TEMPLATE_COSTS_TMPL					= _T("<template_costs_tmpl id=\"%d\">%s</template_costs_tmpl>");
const LPCTSTR TAG_TEMPLATE_ATCOAST							= _T("<template_at_coast>%d</template_at_coast>");
const LPCTSTR TAG_TEMPLATE_SOUTHEAST						= _T("<template_south_east>%d</template_south_east>");
const LPCTSTR TAG_TEMPLATE_REGION5							= _T("<template_region_5>%d</template_region_5>");
const LPCTSTR TAG_TEMPLATE_PART_OF_PLOT				= _T("<template_part_of_plot>%d</template_part_of_plot>");
const LPCTSTR TAG_TEMPLATE_SI_H100_PINE				= _T("<template_si_h100_pine>%s</template_si_h100_pine>");

const LPCTSTR TAG_TEMPLATE_START_SPECIE				= _T("<template_specie id=\"%d\" name=\"%s\">");
const LPCTSTR TAG_TEMPLATE_END_SPECIE					= _T("</template_specie>");
const LPCTSTR TAG_TEMPLATE_SPECIE_HGT					= _T("<template_specie_hgt id=\"%d\" spc_id=\"%d\" index=\"%d\" spc_name=\"%s\" func_type=\"%s\" func_area=\"%s\" func_desc=\"%s\"/>");
const LPCTSTR TAG_TEMPLATE_SPECIE_VOL					= _T("<template_specie_vol id=\"%d\" spc_id=\"%d\" index=\"%d\" spc_name=\"%s\" func_type=\"%s\" func_area=\"%s\" func_desc=\"%s\"/>");
const LPCTSTR TAG_TEMPLATE_SPECIE_BARK					= _T("<template_specie_bark id=\"%d\" spc_id=\"%d\" index=\"%d\" spc_name=\"%s\" func_type=\"%s\" func_area=\"%s\" func_desc=\"%s\"/>");
const LPCTSTR TAG_TEMPLATE_SPECIE_VOL_UB				= _T("<template_specie_vol_ub id=\"%d\" spc_id=\"%d\" index=\"%d\" spc_name=\"%s\" func_type=\"%s\" func_area=\"%s\" func_desc=\"%s\" m3sk_m3ub=\"%f\"/>");
const LPCTSTR TAG_TEMPLATE_SPECIE_QDESC				= _T("<template_specie_qdesc id=\"%d\" qdesc_name=\"%s\"/>");

const LPCTSTR TAG_TEMPLATE_SPECIE_GROT				= _T("<template_specie_grot id=\"%d\" percent=\"%f\" price=\"%f\" cost=\"%f\"/>");	// Added 100317 p�d

// "Transport till v�g. ex. Skotare"; 080317 p�d
// "Transport till Indistri"; 080317 p�d
const LPCTSTR TAG_TEMPLATE_TRANSP_DIST1				= _T("<template_transp_dist1>%d</template_transp_dist1>");	
const LPCTSTR TAG_TEMPLATE_TRANSP_DIST2				= _T("<template_transp_dist2>%d</template_transp_dist2>");

const LPCTSTR TAG_TEMPLATE_SPECIE_SKFUB				= _T("<template_specie_sk_to_fub>%.2f</template_specie_sk_to_fub>");
const LPCTSTR TAG_TEMPLATE_SPECIE_FUBTO				= _T("<template_specie_fub_to_to>%.2f</template_specie_fub_to_to>");
/*
const LPCTSTR TAG_TEMPLATE_SPECIE_ATCOAST			= _T("<template_specie_at_coast>%d</template_specie_at_coast>");
const LPCTSTR TAG_TEMPLATE_SPECIE_SOUTHEAST		= _T("<template_specie_south_east>%d</template_specie_south_east>");
const LPCTSTR TAG_TEMPLATE_SPECIE_REGION5			= _T("<template_specie_region_5>%d</template_specie_region_5>");
const LPCTSTR TAG_TEMPLATE_SPECIE_PART_OF_PLOT	= _T("<template_specie_part_of_plot>%d</template_specie_part_of_plot>");
*/
const LPCTSTR TAG_TEMPLATE_SPECIE_H25					= _T("<template_specie_h25>%.1f</template_specie_h25>");
const LPCTSTR TAG_TEMPLATE_SPECIE_GCROWN				= _T("<template_specie_gcrown>%.0f</template_specie_gcrown>");

const LPCTSTR TAG_TEMPLATE_TRANSFER_START			= _T("<template_transfers>");
const LPCTSTR TAG_TEMPLATE_TRANSFER_END				= _T("</template_transfers>");
const LPCTSTR TAG_TEMPLATE_TRANSFER_ITEM				= _T("<template_transfer_item from_id=\"%d\" from_name=\"%s\" to_id=\"%d\" to_name=\"%s\" m3fub=\"%.2f\" percent=\"%.2f\"/>");

//////////////////////////////////////////////////////////////////////////////////////////
//	Create database table; 080701 p�d
const LPCTSTR table_Template = _T("CREATE TABLE dbo.%s (")
														 _T("tmpl_id INT NOT NULL IDENTITY,")
														 _T("tmpl_name NVARCHAR(50),")
														 _T("tmpl_type_of int,")				/* Idetifies type of template; ex. 1 = Trakt template */
														 _T("tmpl_template NTEXT,")				/* Template in xml format */
														 _T("tmpl_notes ntext,")				/* Noteringar f�r Mall */
														 _T("created_by NVARCHAR(20),")
														 _T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
														 _T("PRIMARY KEY (tmpl_id));");
//////////////////////////////////////////////////////////////////////////////////////////
// Buffer
#define BUFFER_SIZE		1024*100		// 100 kb buffer


const LPCTSTR TEMPLATE_FN_EXT				= _T(".xml");							// Uses ordinary xml-file
const LPCTSTR TEMPLATE_ARCIVE_FN_EXT		= _T(".hzp");							// Extension for Template archive; 081201 p�d

const LPCTSTR TEMPORARY_EXTRACT_DIR			= _T("C:\\temp");					// Extracting zip-files to this directory; 081127 p�d

const LPCTSTR TEMPORARY_FN_STAND			= _T("stand.xml");				// Name of Stand-template in zip-file; 081127 p�d
const LPCTSTR TEMPORARY_FN_PRICELIST		= _T("pricelist.xml");		// Name of Pricelist-template in zip-file; 081127 p�d
const LPCTSTR TEMPORARY_FN_COST				= _T("cost.xml");					// Name of Cost-template in zip-file; 081127 p�d

//////////////////////////////////////////////////////////////////////////
// CMyPropGridItem_validate derived from CXTPPropertyGridItemDouble; 081126 p�d
typedef	enum { VALUE_LT,	// Value less than
VALUE_LE,	// Value less than or equal
VALUE_GT,	// Value greater than
VALUE_GE,	// Value greater than or equal
VALUE_NO		// No validation
} enumValidateType;

class CMyPropGridItemDouble_validate : public CXTPPropertyGridItemDouble
{
	//private:
	double m_fValidateValue;
	CString m_sMsgCap;
	CString m_sDec;
	CStringArray m_arrMsg;
	enumValidateType m_enumValidateType;
	short m_nNumOfMsg;
	BOOL m_bSetValidateValueIfFail;
	BOOL m_bNegativeNotAllowed;
public:
	CMyPropGridItemDouble_validate(LPCTSTR cap,
		LPCTSTR dec,
		LPCTSTR msgCap,
		CStringArray& msg,
		double validate_value,
		enumValidateType validateType = VALUE_NO,
		BOOL bSetValidateValueIfFail=FALSE,
		BOOL bNegativeNotAllowed=FALSE)
		: CXTPPropertyGridItemDouble(cap,0.0,dec),
		m_sDec(dec),
		m_fValidateValue(validate_value),
		m_sMsgCap(msgCap),
		m_enumValidateType(validateType),
		m_bSetValidateValueIfFail(bSetValidateValueIfFail),
		m_bNegativeNotAllowed(bNegativeNotAllowed)
	{
		m_arrMsg.Append(msg);
		m_nNumOfMsg = m_arrMsg.GetCount();
	}

	virtual ~CMyPropGridItemDouble_validate(void)
	{
		m_arrMsg.RemoveAll();
	}

	virtual void OnValueChanged(CString strValue)
	{
		CString sMsg=__T(""),sTmpMsg;
		BOOL bIsValueOK = TRUE;
		sTmpMsg=_T("");
		// Do the actual validation of entered data.
		// Criteria: if value entered is <= to validate value
		// set this as the new value, otherwise set the old value; 081126 p�d
		double fEnteredValue = _tstof(strValue);
		// Check type of validation
		switch (m_enumValidateType)
		{
		case VALUE_LT :
			if (fEnteredValue >= m_fValidateValue || (m_bNegativeNotAllowed && fEnteredValue<0.0)) // #4663 20151105 J� lagt till koll om negativt v�rde ej �r till�tet
			{
				if (fEnteredValue >= m_fValidateValue)
				{
					if (m_nNumOfMsg > 0)
						sTmpMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(0),m_fValidateValue);
					else
						sTmpMsg.Format(_T("Value must be LT : %.2f"),m_fValidateValue);
					sMsg+=sTmpMsg;
				}
				else
				{
					if(m_bNegativeNotAllowed && fEnteredValue<0.0)
					{
						if (m_nNumOfMsg > 0)
							sTmpMsg.Format(_T("\r\n%s"),m_arrMsg.GetAt(4));
						else
							sTmpMsg.Format(_T("Value must be positive"));
						sMsg+=sTmpMsg;
					}
				}
				bIsValueOK = FALSE;
			}
			break;
		case VALUE_LE :
			if (fEnteredValue > m_fValidateValue || (m_bNegativeNotAllowed && fEnteredValue<0.0))
			{
				if (fEnteredValue > m_fValidateValue )
				{
					if (m_nNumOfMsg > 1)
						sTmpMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(1),m_fValidateValue);
					else
						sTmpMsg.Format(_T("Value must be LE : %.2f"),m_fValidateValue);
					sMsg+=sTmpMsg;
				}
				else
				{
					if(m_bNegativeNotAllowed && fEnteredValue<0.0)
					{
						if (m_nNumOfMsg > 0)
							sTmpMsg.Format(_T("\r\n%s"),m_arrMsg.GetAt(4));
						else
							sTmpMsg.Format(_T("Value must be positive"));
						sMsg+=sTmpMsg;
					}
				}
				bIsValueOK = FALSE;
			}
			break;
		case VALUE_GT :
			if (fEnteredValue <= m_fValidateValue || (m_bNegativeNotAllowed && fEnteredValue<0.0))
			{
				if (m_nNumOfMsg > 2)
					sTmpMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(2),m_fValidateValue);
				else
					sTmpMsg.Format(_T("Value must be GT : %.2f"),m_fValidateValue);
				sMsg+=sTmpMsg;
				bIsValueOK = FALSE;
			}
			break;
		case VALUE_GE :
			if (fEnteredValue < m_fValidateValue || (m_bNegativeNotAllowed && fEnteredValue<0.0))
			{
				if (m_nNumOfMsg > 3)
					sTmpMsg.Format(_T("%s : %.2f"),m_arrMsg.GetAt(3),m_fValidateValue);
				else
					sTmpMsg.Format(_T("Value must be GE : %.2f"),m_fValidateValue);
				sMsg+=sTmpMsg;
				bIsValueOK = FALSE;
			}
			break;

		};

		if (bIsValueOK)
			SetDouble(fEnteredValue);
		else
		{
			if(m_bSetValidateValueIfFail)	// #4663 20151105 J� s�tter valideringsv�rde om kontrollen misslyckas
			{
				SetDouble(m_fValidateValue);
				strValue.Format(m_sDec,m_fValidateValue);
			}
			::MessageBox(0,sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}

		CXTPPropertyGridItemDouble::OnValueChanged(strValue);
	}
};



//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions
void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

typedef void (CXTPPropertyGridItem::*ITEMFUNCTIONPTR)();
void _DoCollapseExpand(CXTPPropertyGridItems* pItems, ITEMFUNCTIONPTR pFunction);

void getTemplateSpecies(LPCTSTR xml_file,vecTransactionSpecies &vec);
void getTemplateFunctionsPerSpcecie(int spc_id,LPCTSTR xml_file,vecUCFunctionList &vec,double *m3sk_m3to,double *m3sk_m3ub_const);
void getTemplateMiscFunctionsPerSpcecie(int spc_id,LPCTSTR xml_file,int *,LPTSTR,double *,double *,double *,int *,int *,double *);
void getTemplateGrotFunctionsPerSpcecie(int spc_id,LPCTSTR xml_file,int *,double *,double *,double *);
void getTemplateTransfersPerSpcecie(int spc_id,LPCTSTR xml_file,vecTransactionTemplateTransfers &);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp);
CView *getFormViewByID(int idd);


BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name);

CString getToolBarResourceFN(void);

void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);
