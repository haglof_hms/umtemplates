
#include "StdAfx.h"
#include "TemplateFrame.h"
#include "MDITemplateFormView.h"

#include "InputDialog.h"

#include "ResLangFileReader.h"

#include "Resource.h"

#include "UMTemplatesDB.h"

#include "ZipArchive.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
	// TODO: add one-time construction code here

}

CMDIFrameDoc::~CMDIFrameDoc()
{
}


BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands


/////////////////////////////////////////////////////////////////////////////
// CMDITemplateTraktFormFrame

IMPLEMENT_DYNCREATE(CMDITemplateTraktFormFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITemplateTraktFormFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITemplateTraktFormFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SYSCOMMAND()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
//	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)

	ON_COMMAND(ID_TBBTN_IMPORT, OnImportStandTemplate)
	ON_COMMAND(ID_TBBTN_EXPORT, OnExportStandTemplate)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_EXPORT, OnUpdateExportTBtn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CMDITemplateTraktFormFrame construction/destruction

XTPDockingPanePaintTheme CMDITemplateTraktFormFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CMDITemplateTraktFormFrame::CMDITemplateTraktFormFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bConnected = FALSE;
	m_bAlreadySaidOkToClose = FALSE;
	m_bEnableExportTBtn = TRUE;
}

CMDITemplateTraktFormFrame::~CMDITemplateTraktFormFrame()
{
}

void CMDITemplateTraktFormFrame::OnDestroy(void)
{
/*
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TEMPLATE_TRAKT_ENTRY_KEY);
	SavePlacement(this, csBuf);


	m_bFirstOpen = TRUE;
}

void CMDITemplateTraktFormFrame::OnClose(void)
{
	BOOL bDoClose = m_bAlreadySaidOkToClose;
	if (!bDoClose)
		bDoClose = isOkToClose();

	if (bDoClose)
	{
		CMDITemplateTraktFormView *pView = (CMDITemplateTraktFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			pView->doOnClose();
		}
		// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
		CMDIChildWnd::OnClose();
	}

}

void CMDITemplateTraktFormFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		if (isOkToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

int CMDITemplateTraktFormFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	EnableDocking(CBRS_ALIGN_ANY);

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sToolTip1 = xml->str(IDS_STRING2501);
			sToolTip2 = xml->str(IDS_STRING2500);
		}
		delete xml;
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_IMPORT,sToolTip1);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_EXPORT,sToolTip2);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	// Send message to HMSShell, please send back the DB connection; 070430 p�d
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setLanguage();

	m_bFirstOpen = TRUE;

	m_bAlreadySaidOkToClose = FALSE;

	return 0; // creation ok
}

int CMDITemplateTraktFormFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return FALSE;
}

BOOL CMDITemplateTraktFormFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDITemplateTraktFormFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

void CMDITemplateTraktFormFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDITemplateTraktFormFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TEMPLATE_TRAKT_ENTRY_KEY);
		LoadPlacement(this, csBuf);

		// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
		// with ID_LPARAM_COMMAND2.
		// The return message holds a _user_msg structure, collected in the
		// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);
  }
}

void CMDITemplateTraktFormFrame::OnSetFocus(CWnd* pWnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);


	CMDITemplateTraktFormView *pView = (CMDITemplateTraktFormView*)GetActiveView();
	if (pView) pView->doSetNavigationBar();

	CMDIChildWnd::OnSetFocus(pWnd);
}

BOOL CMDITemplateTraktFormFrame::isOkToClose(void)
{
	BOOL bDataFormViewClose = TRUE;
	CMDITemplateTraktFormView *pView = (CMDITemplateTraktFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->resetInitialized2();	
		bDataFormViewClose = pView->isHasDataChanged(2);
	}
	return bDataFormViewClose;
}
// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITemplateTraktFormFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CStringArray arr;
	CString sErrSpcMsg;
	CString sErrMsg;
	int nLParmaValue = 1;
	BOOL bIsTemplateOK_MoveOn = TRUE;
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW2,m_sLangFN,lParam);
	}
	else
	{
/*
		// Check if Template is OK; 071121 p�d
		if (wParam == ID_DBNAVIG_START ||
 			  wParam == ID_DBNAVIG_NEXT ||
 			  wParam == ID_DBNAVIG_PREV ||
 			  wParam == ID_DBNAVIG_END)
		{
			bIsTemplateOK_MoveOn = isOkToClose(arr);
		}

		if (!bIsTemplateOK_MoveOn)
		{
			if (arr.GetCount() > 0)
			{
				// Add information on error per specie; 080418 p�d
				for (int i = 0;i < arr.GetCount();i++)
					sErrSpcMsg += arr.GetAt(i) + _T("\n");

				sErrMsg.Format("%s\n%s\n%s\n\n%s\n",
					m_sMsgDataMissing2,
					sErrSpcMsg,
					m_sMsgDataMissing3,
					m_sMsgMoveOn);
			}
			else
			{
				sErrMsg.Format("%s\n\n%s\n\n%s\n",
					m_sMsgDataMissing2,
					m_sMsgDataMissing3,
					m_sMsgMoveOn);
			}

			if (::MessageBox(this->GetSafeHwnd(),_T(sErrMsg),_T(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				nLParmaValue = -1;
				bIsTemplateOK_MoveOn = TRUE;
			}
		}	// if (!bIsTemplateOK_MoveOn)
*/
//		if (bIsTemplateOK_MoveOn)
//		{
			CDocument *pDoc = GetActiveDocument();
			if (pDoc != NULL)
			{
				POSITION pos = pDoc->GetFirstViewPosition();
				while (pos != NULL)
				{
					CView *pView = pDoc->GetNextView(pos);
					pView->SendMessage(MSG_IN_SUITE,wParam,(LPARAM)nLParmaValue);
				}	// while (pos != NULL)
			}	// if (pDoc != NULL)
//		}	// if (bIsTemplateOK_MoveOn)
	}
	return 0L;
}
/*
LRESULT CMDITemplateTraktFormFrame::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}
*/

// CMDITemplateTraktFormFrame diagnostics

#ifdef _DEBUG
void CMDITemplateTraktFormFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITemplateTraktFormFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDITemplateTraktFormFrame::OnPaint()
{
CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

void CMDITemplateTraktFormFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITemplateTraktFormFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// PRIVATE

void CMDITemplateTraktFormFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sMsgOK = xml->str(IDS_STRING210);
			m_sMsgCancel = xml->str(IDS_STRING211);

			m_sMsgCapOrig = (xml->str(IDS_STRING214));
			m_sMsgDataMissing.Format(_T("%s\n%s\n\n%s\n\n%s\n"),
						(xml->str(IDS_STRING2190)),
						(xml->str(IDS_STRING2191)),
						(xml->str(IDS_STRING2192)),
						(xml->str(IDS_STRING2193)));
			m_sMsgDataMissing1.Format(_T("%s\n%s\n\n%s\n\n"),
						(xml->str(IDS_STRING2190)),
						(xml->str(IDS_STRING2191)),
						(xml->str(IDS_STRING2192)));
			m_sMsgDataMissing2.Format(_T("%s\n%s\n\n%s\n%s\n%s\n\n%s\n"),
						(xml->str(IDS_STRING2190)),
						(xml->str(IDS_STRING2191)),
						(xml->str(IDS_STRING2150)),
						(xml->str(IDS_STRING2151)),
						(xml->str(IDS_STRING2152)),
						(xml->str(IDS_STRING110)));
			m_sMsgDataMissing3.Format(_T("%s\n%s\n\n"),
						(xml->str(IDS_STRING2153)),
						(xml->str(IDS_STRING2154)));

			m_sMsgMoveOn = (xml->str(IDS_STRING2194));

			m_sMsgPricelistInArcive1 = xml->str(IDS_STRING2510);
			m_sMsgPricelistInArcive2.Format(_T("%s\n\n%s\n"),
						(xml->str(IDS_STRING2511)),
						(xml->str(IDS_STRING2512)));

			m_sMsgPricelistNewName = xml->str(IDS_STRING2513);


			m_sMsgCostTemplInArcive1 = xml->str(IDS_STRING2520);
			m_sMsgCostTemplInArcive2.Format(_T("%s\n\n%s\n"),
						(xml->str(IDS_STRING2521)),
						(xml->str(IDS_STRING2522)));
			m_sMsgCostTemplNewName = xml->str(IDS_STRING2523);

			m_sMsgTemplInArcive1 = xml->str(IDS_STRING2530);
			m_sMsgTemplInArcive2 = xml->str(IDS_STRING2531);
			m_sMsgTemplNewName = xml->str(IDS_STRING2522);

			m_sMsgInpDlgCap = xml->str(IDS_STRING251);

			// #4582
			m_sMsgErrorPricelist = xml->str(IDS_STRING262);
			m_sMsgErrorCosts = xml->str(IDS_STRING263);
			m_sMsgErrorTemplate = xml->str(IDS_STRING264);
			m_sMsgCapImport = xml->str(265);

			//m_sMsgCostTemplInArcive;


		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))

}

// CMDITemplateTraktFormFrame message handlers

void CMDITemplateTraktFormFrame::OnImportStandTemplate(void)
{
	CString sMsg;
	CString sName;
	CString sFilter;
	int nAction = -1;

	CString sXML;
	CString sXMLCompleted;

	TCHAR szName[127];

	BOOL bPrlNameOK = TRUE;
	CString sPricelistFN;
	CString sPrlName;
	CTransaction_pricelist recPrl;
	vecTransactionPricelist vecPrlInDB;
	short nTypeOfPricelist = -1;
	int nPriceIn = -1;


	BOOL bCostNameOK = TRUE;
	CString sCostTemplateFN;
	CString sCostName;
	CTransaction_costtempl recCost;
	vecTransaction_costtempl vecCostInDB;

	BOOL bTmplNameOK = TRUE;
	CString sStandTemplateFN;
	CString sTmplName;
	vecTransactionTemplate vecTemplates;
	CTransaction_template recTmpl;
	CInputDialog *dlgInput = new CInputDialog();

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING250),TEMPLATE_ARCIVE_FN_EXT,TEMPLATE_ARCIVE_FN_EXT);
		}
		delete xml;
	}
	// Select directory to open template from disk; 081126 p�d
	CFileDialog dlg( TRUE, TEMPLATE_ARCIVE_FN_EXT, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
												sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		CZipFile f;
		TCHAR szFN[MAX_PATH];
		_stprintf(szFN,_T("%s"),dlg.GetPathName());

		if (f.Open(szFN,CZipFile::modeRead,false))
		{
		   // read the contents of the file into the memory file
      int iLen = (int)f.GetLength();    
      BYTE* b = (BYTE*)malloc((UINT)iLen);
      f.Read(b, iLen);
      f.Close();
      CZipMemFile mf;
      mf.Attach(b, iLen);
      // open the archive in memory
      CZipArchive zip;
      zip.Open(mf);
			// Extract arcive to temporary directory; 081127 p�d
			for (ZIP_INDEX_TYPE  i = 0;i < zip.GetCount();i++)
				zip.ExtractFile(i,getTempDir());

			zip.Close();
			free(b);
		}	// if (f.Open(szFN,CZipFile::modeRead,false))

		// After extracting files from arcive, we'll start to add data to database.
		// We add Pricelist,Cost-template and Stand-template.
		// OBS! Need to check if names already exists, ergo; the pricelist is already 
		// in datbase. If so we need to ask the user if he want's to replace pricelist
		// or create a new on. If adding a new on we also need to ask the user to add
		// a new name; 081127 p�d
		// OBS! If new name for Pricelist and/or cost-template, the name an id in Stand-template
		// must be replaced; 081127 p�d

		// Start by collecting info. on Pricelists and Cost-templates from DB; 081127 p�d
		CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getPricelists(vecPrlInDB);
			pDB->getCostTmpls(vecCostInDB);
			pDB->getTemplates(vecTemplates,ID_TEMPLATE_TRAKT);
			delete pDB;
		}	
		// Setup serarchpaths anf filenames of Pricelist,Cost-template and Stand-Template
		// in temporary directory; 081127 p�d
		sPricelistFN.Format(_T("%s%s"),getTempDir(),TEMPORARY_FN_PRICELIST);
		sCostTemplateFN.Format(_T("%s%s"),getTempDir(),TEMPORARY_FN_COST);
		sStandTemplateFN.Format(_T("%s%s"),getTempDir(),TEMPORARY_FN_STAND);
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// Load pricelist from temporary directory, into PricelistParser; 081127 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		PricelistParser prlPars;
		if (prlPars.LoadFromFile(sPricelistFN))
		{
			if(!prlPars.getHeaderName(szName))
			{
				::MessageBox(this->GetSafeHwnd(), m_sMsgErrorPricelist, m_sMsgCapImport, MB_ICONERROR | MB_OK);
				return;	//#4582
			}
			sPrlName = szName;	// As default name of pricelist; 081128 p�d
			nTypeOfPricelist = (prlPars.getHeaderPriceIn(&nPriceIn) ? 1 : 2);	// 1 = Estimate pricelist, 2 = Avg. pricelist
			nAction = 1;	// Pricelist NOT in database; 081127 p�d

			// Check if this name already been used; 081127 p�d
			// -----------------------------------------------------------------
			// Change method; if pricelist(name) in DB do nothing
			// if NOT in DB add; 100416 p�d
			// -----------------------------------------------------------------
			if (vecPrlInDB.size() > 0)
			{
				for (UINT iPrl = 0;iPrl < vecPrlInDB.size();iPrl++)
				{
					recPrl = vecPrlInDB[iPrl];
					if (recPrl.getName().CompareNoCase(szName) == 0)
					{
						nAction = 0;	// Pricelist in database; 081127 p�d
						break;
					}	// if (sName.CompareNoCase(recPrl.getName()) == 0)
				}	// for (UINT iPrl = 0;iPrl < vecPrlInDB.size();iPrl++)
			}
			// Action = 1; Pricelist NOT in database, just add it; 081127 p�d
			if (nAction == 1)	
			{
				sPrlName.Format(_T("%s"),szName); 
				prlPars.getXML(sXML);
				CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
				if (pDB != NULL)
				{
					sXMLCompleted.Format(_T("%s%s"),TAG_FIRST_ROW,sXML);
					pDB->setPricelist(sPrlName,nTypeOfPricelist,sXMLCompleted);
					delete pDB;
				}	// if (pDB != NULL)
			}
		}	// if (prlPars.LoadFromFile(sPricelistFN))

		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// Load cost-template from temporary directory, into CostsTmplParser; 081127 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		ObjectCostsTmplParser costPars;
		szName[0] = '\0';
		if (costPars.LoadFromFile(sCostTemplateFN))
		{
			if(!costPars.getObjCostsTmplName(szName))
			{
				::MessageBox(this->GetSafeHwnd(), m_sMsgErrorCosts, m_sMsgCapImport, MB_ICONERROR | MB_OK);
				return;	// #4582
			}

			sCostName = szName;	// As default name of cost-template; 081128 p�d
			nAction = 1;	// Cost-template NOT in database; 081127 p�d
			// Check if this name already been used; 081127 p�d
			// -----------------------------------------------------------------
			// Change method; if costtempl.(name) in DB do nothing
			// if NOT in DB add; 100416 p�d
			// -----------------------------------------------------------------

			if (vecCostInDB.size() > 0)
			{
				for (UINT iCost = 0;iCost < vecCostInDB.size();iCost++)
				{
					recCost = vecCostInDB[iCost];
					if (recCost.getTemplateName().CompareNoCase(szName) == 0)
					{
						nAction = 0;	// Cost-template in database; 081127 p�d
						break;
					}	// if (recCost.getTemplateName().CompareNoCase(szName) == 0)
				}	// for (UINT iCost = 0;iCost < vecCostInDB.size();iCost++)
			}
			// Action = 1; Cost-template NOT in database, just add it; 081127 p�d
			if (nAction == 1)	
			{
				sCostName.Format(_T("%s"),szName); 
				costPars.getXML(sXML);
				CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
				if (pDB != NULL)
				{
					sXMLCompleted.Format(_T("%s%s"),TAG_FIRST_ROW,sXML);
					pDB->setCostTmpl(sCostName,2,sXMLCompleted);
					delete pDB;
				}	// if (pDB != NULL)
			}
			//}	// if (vecCostInDB.size() > 0)
		}
		delete dlgInput;

		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// Load stand-template from temporary directory, into TemplateParser; 081127 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		TemplateParser standPars;
		if (standPars.LoadFromFile(sStandTemplateFN))
		{
			// We need to reload Pricelists and Cost-templates, to get
			// the latest data; 081124 p�d
			CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
			if (pDB != NULL)
			{
				pDB->getPricelists(vecPrlInDB);
				pDB->getCostTmpls(vecCostInDB);
				delete pDB;
			}
			// Find the Priselist and CostTemplate; 081127 p�d
			if (vecPrlInDB.size() > 0)
			{
				for (UINT i = 0;i < vecPrlInDB.size();i++)
				{
					recPrl = vecPrlInDB[i];
					if (recPrl.getName().CompareNoCase(sPrlName) == 0)
						break;
				}	// for (UINT i = 0;i < vecPrlInDB.size();i++)
			}	// if (vecPrlInDB.size() > 0)

			if (vecCostInDB.size() > 0)
			{
				for (UINT i = 0;i < vecCostInDB.size();i++)
				{
					recCost = vecCostInDB[i];
					if (recCost.getTemplateName().CompareNoCase(sCostName) == 0)
						break;
				}	// for (UINT i = 0;i < vecPrlInDB.size();i++)
			}	// if (vecCostInDB.size() > 0)
		
			//-------------------------------------------------------------------------
			// Check if Templatename already used and in how many instances; 100416 p�d
			int nTmplNameCnt = 0;
			TCHAR szTmplName[127];
			CString sTmplName;
			if(!standPars.getTemplateName(szTmplName))
			{
				::MessageBox(this->GetSafeHwnd(), m_sMsgErrorTemplate, m_sMsgCapImport, MB_ICONERROR | MB_OK);
				return;	// #4582
			}

			for (UINT i1 = 0;i1 < vecTemplates.size();i1++)
			{
				recTmpl = vecTemplates[i1];
				sTmplName = recTmpl.getTemplateName().Left(_tcslen(szTmplName));
				if (sTmplName.Compare(szTmplName) == 0)
				{
					nTmplNameCnt++;
				}
			}
			if (nTmplNameCnt > 0)
				sTmplName.Format(L"%s(%d)",szTmplName,nTmplNameCnt);
			else
				sTmplName.Format(L"%s",szTmplName);
			//-------------------------------------------------------------------------

			standPars.setTemplateName(sTmplName);
			standPars.setTemplatePricelist(recPrl.getID(),recPrl.getName());
			standPars.setTemplateCostsTmpl(recCost.getID(),recCost.getTemplateName());

			standPars.getXML(sXML);
			if (m_bConnected)
			{
				CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
				if (pDB != NULL)
				{
					sXMLCompleted.Format(_T("%s%s"),TAG_FIRST_ROW,sXML);
					pDB->setTemplate(_T(""),ID_TEMPLATE_TRAKT_DEVELOPMENT,sXMLCompleted);
					delete pDB;
				}	// if (pDB != NULL)
			}	// if (m_bConnected)
			CMDITemplateTraktFormView *pView = (CMDITemplateTraktFormView *)getFormViewByID(IDD_FORMVIEW);
			if (pView != NULL)
			{
				pView->doPouplateFromLastEntry();
				pView->saveTemplateToDB(0);
				pView = NULL;
			}
		}	// if (standPars.LoadFromFile(sStandTemplateFN))

		// Remove files from temporary directory; 081127 p�d
		removeFile(sPricelistFN);
		removeFile(sCostTemplateFN);
		removeFile(sStandTemplateFN);

	}	// if(dlg.DoModal() == IDOK)

	vecCostInDB.clear();
	vecPrlInDB.clear();
}

void CMDITemplateTraktFormFrame::OnExportStandTemplate(void)
{
	int nPricelistID;
	TCHAR szPricelistName[127];
	CTransaction_pricelist recPricelist;
	int nCostID;
	TCHAR szCostName[127];
	CTransaction_costtempl recCostTmpl;

	CString sName;
	CString sFilter;
	CString sFolderPath;
	CString sStandXMLPath;
	CString sPricelistXMLPath;
	CString sCostTemplXMLPath;
	
	int nFolderPathLength = 0;

	CTransaction_template recTmpl;
	CDocument *pDoc = GetActiveDocument();


	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING250),TEMPLATE_ARCIVE_FN_EXT,TEMPLATE_ARCIVE_FN_EXT);
		}
		delete xml;
	}
	
	CMDITemplateTraktFormView *pView = (CMDITemplateTraktFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->doSaveAndPouplateLastEntry();
		if (pView->getActiveTemplate(recTmpl))
		{
			sName = recTmpl.getTemplateName();
			scanFileName(sName);
			if (sName.Right(4) != TEMPLATE_ARCIVE_FN_EXT)
				sName += TEMPLATE_ARCIVE_FN_EXT;
			// Select directory to save template on disk; 081126 p�d
			CFileDialog dlg( FALSE, TEMPLATE_ARCIVE_FN_EXT, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , sFilter, this);
	
			if(dlg.DoModal() == IDOK)
			{
				nFolderPathLength = (dlg.GetPathName().GetLength() - (dlg.GetFileName().GetLength() + 1));
				sFolderPath = dlg.GetPathName().Left(nFolderPathLength);
				sStandXMLPath.Format(_T("%s\\%s-templ%s"),sFolderPath,sName,TEMPLATE_FN_EXT);
				TemplateParser *pars = new TemplateParser();
				if (pars != NULL)
				{
					if (pars->LoadFromBuffer(recTmpl.getTemplateFile()))
					{
						pars->getTemplatePricelist(&nPricelistID,szPricelistName);
						pars->getTemplateCostsTmpl(&nCostID,szCostName);
						pars->SaveToFile(sStandXMLPath);
					}	// if (pars->LoadFromBuffer(recTmpl.getTemplateFile()))				
					// Get information on Pricelist and Cost-templates; 081126 p�d
					if (m_bConnected)
					{
						CUMTemplateDB *pDB = new CUMTemplateDB(m_dbConnectionData);
						if (pDB != NULL)
						{
							pDB->getPricelist(nPricelistID,recPricelist);
							pDB->getCostTmpl(nCostID,recCostTmpl);
							sPricelistXMLPath.Format(_T("%s\\%s-price%s"),sFolderPath,recPricelist.getName(),TEMPLATE_FN_EXT);
							sCostTemplXMLPath.Format(_T("%s\\%s-cost%s"),sFolderPath,recCostTmpl.getTemplateName(),TEMPLATE_FN_EXT);
							delete pDB;
						}
					}	
					
					if (pars->LoadFromBuffer(recPricelist.getPricelistFile()))
					{
						pars->SaveToFile(sPricelistXMLPath);
					}				
					if (pars->LoadFromBuffer(recCostTmpl.getTemplateFile() ))
					{
						pars->SaveToFile(sCostTemplXMLPath);
					}				
					delete pars;
				}	// if (pars != NULL)
				CZipArchive* pZIP = new CZipArchive();
				if (pZIP != NULL)
				{
					sName = dlg.GetPathName();
					TCHAR szFN[MAX_PATH];
					if (sName.Right(4) != TEMPLATE_ARCIVE_FN_EXT)
						_stprintf(szFN,_T("%s%s"),sName,TEMPLATE_ARCIVE_FN_EXT);
					else
						_stprintf(szFN,_T("%s"),sName);
					
					pZIP->Open(szFN,CZipArchive::zipCreate);
					
					// Set name in zip-file. These names'll be
					// used on extracting zip-file into a temporary
					// directory (C:\temp); 081127 p�d
					pZIP->AddNewFile(sStandXMLPath,TEMPORARY_FN_STAND,-1,false);
					pZIP->AddNewFile(sPricelistXMLPath,TEMPORARY_FN_PRICELIST,-1,false);
					pZIP->AddNewFile(sCostTemplXMLPath,TEMPORARY_FN_COST,-1,false);

					pZIP->Close();

					delete pZIP;
					// Remove files from disk. Only in zip-file (arcive); 081127 p�d
					removeFile(sStandXMLPath);
					removeFile(sPricelistXMLPath);
					removeFile(sCostTemplXMLPath);
				}
			}	// if(dlg.DoModal() == IDOK)
		}	// if (pView->getActiveTemplate(recTmpl))
		pView = NULL;
	}
}

void CMDITemplateTraktFormFrame::OnUpdateExportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableExportTBtn);
}


/////////////////////////////////////////////////////////////////////////////
// CMDITemplateListFrame


IMPLEMENT_DYNCREATE(CMDITemplateListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITemplateListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITemplateListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDITemplateListFrame::CMDITemplateListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDITemplateListFrame::~CMDITemplateListFrame()
{
}

void CMDITemplateListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TEMPLATE_TRAKT_LIST_ENTRY_KEY);
	SavePlacement(this, csBuf);
}

int CMDITemplateListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDITemplateListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDITemplateListFrame diagnostics

#ifdef _DEBUG
void CMDITemplateListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITemplateListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDITemplateListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDITemplateListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDITemplateListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TEMPLATE_TRAKT_LIST_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDITemplateListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDITemplateListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITemplateListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITemplateListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDITemplateListFrame::setLanguage()
{
}

