// SelectPricelistDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectPricelistDlg.h"

#include "ResLangFileReader.h"

// CSelectPricelistDlg dialog

IMPLEMENT_DYNAMIC(CSelectPricelistDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectPricelistDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CSelectPricelistDlg::OnCbnSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_LIST1, &CSelectPricelistDlg::OnLbnSelchangeList1)
	ON_BN_CLICKED(IDOK, &CSelectPricelistDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CSelectPricelistDlg::CSelectPricelistDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectPricelistDlg::IDD, pParent)
{

}

CSelectPricelistDlg::~CSelectPricelistDlg()
{
}

void CSelectPricelistDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL_EXCHANGE1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_EXCH1, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_PRL1, m_wndLbl3);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_LIST1, m_wndLBox1);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP

}


BOOL CSelectPricelistDlg::OnInitDialog()
{
	CString sWindowMsg;
	CDialog::OnInitDialog();
	// Setup language filename; 080707 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{	
			SetWindowText((xml.str(IDS_STRING240)));

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING2400)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING2401)));
			m_wndLbl3.SetWindowText((xml.str(IDS_STRING2402)));

			m_wndBtnOK.SetWindowText((xml.str(IDS_STRING2403)));
			m_wndBtnCancel.SetWindowText((xml.str(IDS_STRING2404)));

		}
		xml.clean();
	}

	m_wndLBox1.EnableWindow( FALSE );

	m_wndLbl1.SetBkColor(INFOBK);

	// Setup Exchange functions in CmoboBox; 080707 p�d
	getExchangeFunctions(m_vecExchangeFunc);
	if (m_vecExchangeFunc.size() > 0)
	{
		m_wndCBox1.ResetContent();
		for (UINT i = 0;i < m_vecExchangeFunc.size();i++)
		{
			m_recUCFunctions = m_vecExchangeFunc[i];
			m_wndCBox1.AddString(m_recUCFunctions.getName());
		}	// for (UINT i = 0;i < m_vecExchangeFunc.size();i++)
	}	// if (m_vecExchangeFunc.size() > 0)

	m_wndBtnOK.EnableWindow(FALSE);
	return TRUE;
}


// CSelectPricelistDlg message handlers

void CSelectPricelistDlg::OnCbnSelchangeCombo1()
{
	int nIndex = m_wndCBox1.GetCurSel();
	// If there's a Exchange model selcted, add Pricelists
	// accordingly; 080707 p�d
	if (nIndex != CB_ERR && m_vecPricelists.size() > 0)
	{
		m_wndLBox1.EnableWindow( TRUE );
		m_wndLBox1.ResetContent();
		m_nSelectedPrlTypeOf=nIndex+1;
		for (UINT i = 0;i < m_vecPricelists.size();i++)
		{
			m_recPricelist = m_vecPricelists[i];
			if (m_recPricelist.getTypeOf() == nIndex+1)	// 1 = Pricelist 2 = Avg.pricelist; 080707 p�d
			{
				m_wndLBox1.AddString(m_recPricelist.getName());
			}
		}
	}
}

void CSelectPricelistDlg::OnLbnSelchangeList1()
{
	int nIndex = m_wndLBox1.GetCurSel();

	m_wndBtnOK.EnableWindow(nIndex > LB_ERR);
}

void CSelectPricelistDlg::OnBnClickedOk()
{
	int nCBIndex = m_wndCBox1.GetCurSel();
	int nLBIndex = m_wndLBox1.GetCurSel();
	if (nCBIndex != CB_ERR && nLBIndex != LB_ERR)
	{
		m_wndCBox1.GetWindowText(m_sSelectedExch);
		m_wndLBox1.GetText(nLBIndex,m_sSelectedPrl);
	}	// if (nCBIndex != CB_ERR && nLBIndex != LB_ERR)
	OnOK();
}
