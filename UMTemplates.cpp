// UMTemplates.cpp : Defines the initialization routines for the DLL.
//


#include "stdafx.h"
#include "Resource.h"

#include "TemplateFrame.h"
#include "MDITemplateFormView.h"
#include "MDITemplateListFrameView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines

static AFX_EXTENSION_MODULE UMTemplatesDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

// Added: EXPORTED function create tables in database; 080131 p�d
extern "C" AFX_EXT_API void DoDatabaseTables(LPCTSTR);

extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMTemplate.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMTemplatesDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMTemplate.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMTemplatesDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMTemplatesDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDITemplateTraktFormFrame),
			RUNTIME_CLASS(CMDITemplateTraktFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDITemplateListFrame),
			RUNTIME_CLASS(CTemplateListFrameView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW2,suite,sLangFN,TRUE));


	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	// Do a check to see if database tables are created; 080131 p�d
	DoDatabaseTables(_T(""));	// Empty arg = use default database; 081001 p�d
}

void DoDatabaseTables(LPCTSTR db_name)
{
	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{
		runSQLScriptFileEx((TBL_TEMPLATE),(table_Template),(db_name));
/*
		CString sPath;
		sPath.Format("%s%s\\%s\\%s",getProgDir(),SUBDIR_SCRIPTS,SQL_SERVER_SQRIPT_DIRECTORY,TEMPLATE_TABLES);
		runSQLScriptFile(sPath,TBL_TEMPLATE);
*/
	}
}
