#pragma once

#include "Resource.h"

class CCustomItemButton;

class CInplaceButton : public CXTButton
{
public:
	DECLARE_MESSAGE_MAP()
protected:
	CCustomItemButton* m_pItem;
	void OnClicked();

	friend class CCustomItemButton;
};

class CCustomItemButton : public CXTPPropertyGridItem
{
protected:

public:
	CCustomItemButton(CString strCaption, BOOL bValue,CWnd *parent);

	BOOL GetBool();
	void SetBool(BOOL bValue);

	void setButtonCaption(LPCTSTR text)
	{
		m_strButtonText = (text);
	}
		
	inline CInplaceButton& getInplaceButton(void) { return m_wndButton; }

protected:
	virtual BOOL IsValueChanged();
	virtual void SetVisible(BOOL bVisible);
	BOOL OnDrawItemValue(CDC& dc, CRect rcValue);
	virtual void OnIndexChanged();
	void CreateButton();
	virtual void OnValueChanged(CString strValue);

private:
	CInplaceButton m_wndButton;
	BOOL m_bValue;
	CFont m_wndFont;
	CString m_strButtonText;
	CWnd *m_pParent;

	friend class CInplaceButton;
};


//////////////////////////////////////////////////////////////////////////
// CMyPropGridItemBool derived from CXTPPropertyGridItemBool
class CMyPropGridItemBool : public CXTPPropertyGridItemBool
{
public:
	CMyPropGridItemBool(const CString true_text,
											const CString false_text,
											const CString& cap,
											BOOL bValue) : CXTPPropertyGridItemBool(cap,bValue)
	{
		SetTrueFalseText((true_text),(false_text));
	}
};

// CSpecieDataFormView form view

class CSpecieDataFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSpecieDataFormView)

	BOOL m_bInitialized;
	BOOL m_bIsDirty;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sYes;
	CString m_sNo;

	CString m_sHeight;
	CString m_sCalcHgtAs;
	CString m_sVolume;
	CString m_sCalcVolAs;
	CString m_sVolumeUB;
	CString m_sCalcVolUBAs;
	CString m_sBark;
	CString m_sBarkSerie;
	CString m_sCalcGrotAs;
	CString m_sGrotPercent;
	CString m_sGrotPrice;
	CString m_sGrotCost;

	CString m_sTransfersAs;

	CString m_sQDesc;

	CString m_sMiscSettings;
	CString m_sSk_Fub;
	CString m_sFub_To;
	CString m_sSk_Ub;
	CString m_sAtCoast;
	CString m_sSouthEast;
	CString m_sRegion5;
	CString m_sPartOfPlot;
	CString m_sH25;
	CString m_sGreenCrown;

	CString m_sNoTransfers;
	CString m_sIsTransfers;

	CString m_sTransp1;
	CString m_sTransp2;

	CString m_sSpcErrorMsg;
	CString m_sErrHgt;
	CString m_sErrVolume;
	CString m_sErrBark;
	CString m_sErrVolumeUnderBark;
	CString m_sErrM3SkToM3Fub;
	CString m_sErrM3FubToM3To;
	CString m_sErrH25;
	CString m_sErrQualDesc;

	CString m_sMsgCap;
	CStringArray m_sarrValidateMsg;

	CMyPropertyGrid m_wndPropertyGrid;

	vecTransactionTemplateTransfers m_vecTransactionTemplateTransfers;

	vecUCFunctionList m_vecTemplateFunctionsList;

	vecUCFunctions m_vecHgtFunc;
	vecUCFunctionList m_vecHgtFuncList;
	void setupTypeOfHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupHeightfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecVolFunc;
	vecUCFunctionList m_vecVolFuncList;
	void setupTypeofVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupVolumefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecBarkFunc;
	vecUCFunctionList m_vecBarkFuncList;
	void setupTypeOfBarkfunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupBarkseriesInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecVolUBFunc;
	vecUCFunctionList m_vecVolUBFuncList;
	void setupTypeofVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupVolumefunctionsUBInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecUCFunctions m_vecGROTFunc;
	void setupTypeofGROTInPropertyGrid(CXTPPropertyGridItem *pItem);

	void setupTransfersInPropertyGrid(CXTPPropertyGridItem *pItem);

	void setupTranspDataInPropertyGrid(CXTPPropertyGridItem *pItem);

	void setupMiscDataInPropertyGrid(CXTPPropertyGridItem *pItem);

	void setupQualityDescInPropertyGrid(CXTPPropertyGridItem *pItem);

	vecTransactionAssort m_vecAssortmentsPerSpc;	// Holds information on Assortments in selected pricelist; 070810 p�d
/*
	BOOL getActiveTypeOfFunction(int,CString &,UINT*,int*);
	CString getActiveFunction(int);
*/
	// Bind to
	CString m_sHgtTypeOfBindStr;	// Which height function type's selcted (e.g. H25,Regression etc)
	CString m_sHgtBindStr;				// Selected function within the TypeOf (e.g. H25 => �vriga tr�dslag)

	CString m_sVolTypeOfBindStr;	// Which volume function type's selcted (e.g. Brandels,N�rlunds etc)
	CString m_sVolBindStr;				// Selected function within the TypeOf (e.g. Brandels => Mindre,Norra)

	CString m_sBarkTypeOfBindStr;	// Which bark function type's selcted (e.g. Jonsson/Ostlin,Skogforsk etc)
	CString m_sBarkBindStr;				// Selected function within the TypeOf (e.g. Jonsson/Ostlin => Tall,Serie 1)

	CString m_sVolUBTypeOfBindStr;	// Which volume function under bark, type's selcted (e.g. Brandels,N�rlunds etc)
	CString m_sVolUBBindStr;				// Selected function under bark, within the TypeOf (e.g. Brandels => Mindre,Norra)

	CString m_sGrotFuncBindStr;
	double m_fGrotPercent;				// Uttagsprocent
	double m_fGrotPrice;					// Pris (kr/ton)
	double m_fGrotCost;						// Kostnad (kr/ton)

	double m_fBindToSkFub;
	double m_fBindToFubTo;
	double m_fBindToSkUb;
	BOOL m_bBindToIsAtCoast;
	BOOL m_bBindToIsSouthEast;
	BOOL m_bBindToIsRegion5;
	BOOL m_bBindToIsPartOfPlot;
	CString m_sBindToTransfers;
	CString m_sBindToQualDesc;
	double m_fBindToH25;
	double m_fBindToGreenCrown;
	long m_nBindToTranspDist1;
	long m_nBindToTranspDist2;

	CString m_sQDescBindStr;
	CStringArray m_listQDesc;
	int m_nSpecieID;
	CString m_sSpecieName;
	CString m_sTransfers;

	int m_nHgtFunctionIdentifer;
	int m_nVolFunctionIdentifer;
	int m_nBarkFunctionIdentifer;
	int m_nVolUBFunctionIdentifer;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	UCFunctionList m_recVolUBFunc;
	vecUCFunctionList m_vecVolUBFuncList_spc;
	CTransaction_template m_recActiveTmpl;
	CString m_sTemplateXMLFile;

protected:
	CSpecieDataFormView();           // protected constructor used by dynamic creation
	virtual ~CSpecieDataFormView();

public:
	enum { IDD = IDD_FORMVIEW1 };

	BOOL CreateWnd(DWORD style,const RECT &rect,CWnd *pParent,UINT id,CCreateContext *cc,
			CStringArray &list,int spc_id,LPCTSTR spc_name,vecUCFunctionList &func_list,
			vecTransactionAssort &vec,double m3sk_m3ub)
	{
		m_listQDesc.Append(list);
		m_nSpecieID = spc_id;
		m_sSpecieName = spc_name;
		m_vecTemplateFunctionsList = func_list;
		m_vecAssortmentsPerSpc = vec;
		//m_fBindToSkUb = m3sk_m3ub;
		return Create(NULL,NULL,style,rect,pParent,id,cc);
	}

	CString getSpecieName(void)
	{
		return m_sSpecieName;
	}
	int getSpecieID(void)
	{
		return m_nSpecieID;
	}

	// Handle data entered into PropertyGrid; 070809 p�d
	CString getQualityDesc()	{		return m_sBindToQualDesc;	}

	void setQualityDesc(LPCTSTR v)	{		m_sBindToQualDesc = v;	}

	int getQDescIndex(LPCTSTR);

	int getGrotIndex();
	void setGrotIndex(int);
	double getGrotPercent()	{ return m_fGrotPercent; }
	void setGrotPercent(double v)	{ m_fGrotPercent = v; }
	
	double getGrotPrice()		{ return m_fGrotPrice; }
	void setGrotPrice(double v)	{ m_fGrotPrice = v; }
	
	double getGrotCost()		{ return m_fGrotCost; }
	void setGrotCost(double v)	{ m_fGrotCost = v; }

	void getHeightFunction(UCFunctions &hgt_func,UCFunctionList &func_list);
	void getVolumeFunction(UCFunctions &vol_func,UCFunctionList &func_list);
	void getBarkFunction(UCFunctions &bark_func,UCFunctionList &func_list);
	void getVolumeUBFunction(UCFunctions &vol_func,UCFunctionList &func_list,double *m3sk_m3ub);

	int getTranspDist1(void)	{		return m_nBindToTranspDist1;	}

	void setTranspDist1(int v)	{		m_nBindToTranspDist1 = v;	}

	int getTranspDist2(void)	{		return m_nBindToTranspDist2;	}

	void setTranspDist2(int v)	{		m_nBindToTranspDist2 = v;	}

	double getSkToFub(void)	{		return m_fBindToSkFub;	}

	void setSkToFub(double v)	{		m_fBindToSkFub = v;	}

	double getFubToTO(void)	{		return m_fBindToFubTo;	}

	void setFubToTo(double v)	{		m_fBindToFubTo = v;	}

	double getH25(void)	{		return m_fBindToH25;	}

	void setH25(double v)	{		m_fBindToH25 = v;	}

	double getGreenCrown(void)	{		return m_fBindToGreenCrown;	}

	void setGreenCrown(double v)	{		m_fBindToGreenCrown = v;	}

	void handleTransfers(void);

	vecTransactionAssort& getAssortmentsInPricelist(void)	{		return m_vecAssortmentsPerSpc;	}

	void setTransfers(LPCTSTR s)	{		m_sTransfers = s;	}

	CString getTransfer(void)	{		return m_sTransfers;	}

	BOOL getIsDirty(void);
	void resetIsDirty(void);

	void setBindToHgt(LPCTSTR type_of,LPCTSTR function)
	{
		m_sHgtTypeOfBindStr = type_of;
		if (!m_sHgtTypeOfBindStr.IsEmpty())
		{
			m_sHgtBindStr = function;
		}	// if (!m_sHgtTypeOfBindStr.IsEmpty())
		m_wndPropertyGrid.Refresh();

	}

	void setBindToVol(LPCTSTR type_of,LPCTSTR function)
	{
		m_sVolTypeOfBindStr = type_of;
		if (!m_sVolTypeOfBindStr.IsEmpty())
		{
			m_sVolBindStr = function;
		}
		m_wndPropertyGrid.Refresh();
	}

	void setBindToBark(LPCTSTR type_of,LPCTSTR function)
	{
		m_sBarkTypeOfBindStr = type_of;
		if (!m_sBarkTypeOfBindStr.IsEmpty())
		{
			m_sBarkBindStr = function;
		}
		m_wndPropertyGrid.Refresh();
	}

	void setBindToVolUB(LPCTSTR type_of,LPCTSTR function,double m3sk_m3ub)
	{
		m_sVolUBTypeOfBindStr = type_of;
		if (!m_sVolUBTypeOfBindStr.IsEmpty())
		{
			m_sVolUBBindStr = function;
			m_fBindToSkUb = m3sk_m3ub;
		}
		m_wndPropertyGrid.Refresh();
	}

	void setTemplateTransfers(vecTransactionTemplateTransfers &);

	vecTransactionTemplateTransfers& getTemplateTransfers(void)
	{
		return m_vecTransactionTemplateTransfers;
	}

	BOOL isOKToClose(LPCTSTR spc,CStringArray &);

	void setLockSpecieDataView(BOOL lock)	
	{ 
		CXTPPropertyGridItem *pItem = NULL;
		if ((pItem = m_wndPropertyGrid.FindItem(ID_HGT_TYPEOF_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_HGT_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_VOL_TYPEOF_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_VOL_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_BARK_TYPEOF_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_BARK_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_VOL_UB_TYPEOF_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_VOL_UB_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_M3SK_TO_M3UB)) != NULL) pItem->SetReadOnly(lock);

		if ((pItem = m_wndPropertyGrid.FindItem(ID_QDESC_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
	
		if ((pItem = m_wndPropertyGrid.FindItem(ID_GROT_TYPEOF_FUNC_CB)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_GROT_PERCENT)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_GROT_PRICE)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_GROT_COST)) != NULL) pItem->SetReadOnly(lock);
		CCustomItemButton *pCustButton = NULL;
		if ((pCustButton = (CCustomItemButton*)m_wndPropertyGrid.FindItem(ID_TRANSFERS)) != NULL) 
		{
			pCustButton->getInplaceButton().EnableWindow(!lock);
			pCustButton->SetReadOnly(lock);
		}
		
		if ((pItem = m_wndPropertyGrid.FindItem(ID_TRANSP_DIST1)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_TRANSP_DIST2)) != NULL) pItem->SetReadOnly(lock);

		if ((pItem = m_wndPropertyGrid.FindItem(ID_M3FUB_TO_M3TO)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_H25)) != NULL) pItem->SetReadOnly(lock);
		if ((pItem = m_wndPropertyGrid.FindItem(ID_GREENCROWN)) != NULL) pItem->SetReadOnly(lock);
	}



#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL


	//{{AFX_MSG(CMDITemplateTraktFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


