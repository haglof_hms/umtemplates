// InputDialog.cpp : implementation file
//

#include "stdafx.h"
#include "InputDialog.h"


// CInputDialog dialog

IMPLEMENT_DYNAMIC(CInputDialog, CDialog)

BEGIN_MESSAGE_MAP(CInputDialog, CDialog)
END_MESSAGE_MAP()

CInputDialog::CInputDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CInputDialog::IDD, pParent)
{

}

CInputDialog::~CInputDialog()
{
}

void CInputDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_INPDLG, m_wndInput);
}

INT_PTR CInputDialog::DoModal(CString prompt, CString caption,CString ok_btn,CString cancel_btn)
{
	// Save strings and set in OnInitDialog
	m_sPrompt = prompt;
	m_sCaption = caption;
	m_sOKBtn = ok_btn;
	m_sCancelBtn = cancel_btn;
	m_sInput.Empty();
	return( CDialog::DoModal() );
}

BOOL CInputDialog::OnInitDialog()
{
	// Set up window text
	SetWindowText(m_sCaption);
	GetDlgItem(IDC_LBL_INPDLG)->SetWindowText(m_sPrompt);
	GetDlgItem(IDOK)->SetWindowText(m_sOKBtn);
//	GetDlgItem(IDCANCEL)->SetWindowText(m_sCancelBtn);

	return( CDialog::OnInitDialog() );
}

// CInputDlg message handlers

void CInputDialog::OnOK()
{
	CDialog::OnOK();

	// Save input string
	m_wndInput.GetWindowText(m_sInput);
}
