#pragma once

#include "Resource.h"

#include "SpecieDataFormView.h"

/////////////////////////////////////////////////////////////////////////////
// CTransferReportRec

class CTransferReportRec : public CXTPReportRecord
{
	int m_nID;
protected:
	//////////////////////////////////////////////////////////////////////////
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};


	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CTransferReportRec(void)
	{
		m_nID	= -1;	
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
	}

	CTransferReportRec(int id,LPCTSTR from,LPCTSTR to,double m3fub,double percent)
	{
		m_nID	= id;
		AddItem(new CTextItem(from));
		AddItem(new CTextItem(to ));
		AddItem(new CFloatItem(m3fub,sz2dec));
		AddItem(new CFloatItem(percent,sz2dec));
	}

	CTransferReportRec(int id,CTransaction_template_transfers &v)
	{
		m_nID	= id;
		AddItem(new CTextItem(v.getFromAssort()));
		AddItem(new CTextItem(v.getToAssort()));
		AddItem(new CFloatItem(v.getM3Fub(),sz2dec));
		AddItem(new CFloatItem(v.getPercent(),sz2dec));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CTransfersDialog dialog

class CTransfersDialog : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CTransfersDialog)

	CWnd *m_pParent;
	CSpecieDataFormView *pView;
	CXTResizeGroupBox m_wndGrp;

	CMyReportCtrl m_wndReport;

	BOOL m_bIsDirty;

	CButton m_wndBtnAddTransfer;
	CButton m_wndBtnRemoveTransfer;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sTransInXML;

	vecTransactionTemplateTransfers m_vecTransactionTemplateTransfers;

	void addConstraintsToReport(int nColumn = -1);	// populate the columns with assortment. if nColumn == -1 update all, if 0 update source column and if 1 update destination column

	void populateData(void);

	int getFromAssortID(LPCTSTR assort_name);
	int getToAssortID(LPCTSTR assort_name);

public:
	CTransfersDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTransfersDialog();


	CString& getTransInXML(void)
	{
		return m_sTransInXML;
	}

	void setTemplateTransfers(vecTransactionTemplateTransfers &v)
	{
		m_vecTransactionTemplateTransfers = v;
	}

	vecTransactionTemplateTransfers &getTemplateTransfers(void)
	{
		return m_vecTransactionTemplateTransfers;
	}

	BOOL getIsDirty(void)
	{
		return m_bIsDirty;
	}

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	//{{AFX_VIRTUAL(CGeneralInfoDlg)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	protected:
	//}}AFX_VIRTUAL

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnReportTransValueChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportSelChanged(NMHDR* pNMHDR, LRESULT* /*result*/);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedOk();
};
