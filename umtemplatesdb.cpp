#include "StdAfx.h"

#include "UMTemplatesDB.h"

//////////////////////////////////////////////////////////////////////////////////
// CUMTemplateDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CUMTemplateDB::CUMTemplateDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CUMTemplateDB::CUMTemplateDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CUMTemplateDB::CUMTemplateDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PRIVATE

BOOL CUMTemplateDB::templateExist(CTransaction_template &rec)
{
	CString sSQL;
	sSQL.Format(_T("select * from %s where tmpl_id=%d"),
		TBL_TEMPLATE,rec.getID());
	return exists(sSQL);
}


BOOL CUMTemplateDB::pricelistExist(CTransaction_pricelist &rec)
{
	CString sSQL;
	sSQL.Format(_T("select * from %s where id=%d"),
		TBL_PRICELISTS,rec.getID());
	return exists(sSQL);
}


// PUBLIC
BOOL CUMTemplateDB::getTemplates(vecTransactionTemplate &vec,int tmpl_type)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where tmpl_type_of=:1 or tmpl_type_of=:2"),
			TBL_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		if (tmpl_type == ID_TEMPLATE_TRAKT)
		{
			m_saCommand.Param(1).setAsShort()		= ID_TEMPLATE_TRAKT;
			m_saCommand.Param(2).setAsShort()		= ID_TEMPLATE_TRAKT_DEVELOPMENT;
		}
		if (tmpl_type == ID_TEMPLATE_OBJECT)
		{
			m_saCommand.Param(1).setAsShort()		= ID_TEMPLATE_OBJECT;
			m_saCommand.Param(2).setAsShort()		= ID_TEMPLATE_OBJECT_DEVELOPMENT;
		}
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asShort(),
																	(LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
																	(LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	(LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	(LPCTSTR)m_saCommand.Field(6).asString(),
																  (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMTemplateDB::addTemplate(CTransaction_template &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!templateExist(rec))
		{
			sSQL.Format(_T("insert into %s (tmpl_name,tmpl_type_of,tmpl_template,tmpl_notes,created_by) values(:1,:2,:3,:4,:5)"),
									TBL_TEMPLATE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
			m_saCommand.Param(2).setAsShort()		= rec.getTypeOf();
			m_saCommand.Param(3).setAsLongChar()		= rec.getTemplateFile();
			m_saCommand.Param(4).setAsLongChar()		= rec.getTemplateNotes();
			m_saCommand.Param(5).setAsString()	= rec.getCreatedBy();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			m_saConnection.Commit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMTemplateDB::setTemplate(LPCTSTR name,int type,LPCTSTR xml)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (tmpl_name,tmpl_type_of,tmpl_template,tmpl_notes,created_by) values(:1,:2,:3,:4,:5)"),
									TBL_TEMPLATE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= name;
		m_saCommand.Param(2).setAsShort()		= type;
		m_saCommand.Param(3).setAsLongChar()		= xml;
		m_saCommand.Param(4).setAsLongChar()		= _T("");
		m_saCommand.Param(5).setAsString()	= getUserName();
		m_saCommand.Prepare();
		m_saCommand.Execute();
		m_saConnection.Commit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMTemplateDB::updTemplate(CTransaction_template &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (templateExist(rec))
		{

			sSQL.Format(_T("update %s set tmpl_name=:1,tmpl_type_of=:2,created_by=:3,tmpl_template=:4,tmpl_notes=:5 where tmpl_id=:6"),TBL_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
			m_saCommand.Param(2).setAsShort()		= rec.getTypeOf();
			m_saCommand.Param(3).setAsString()	= rec.getCreatedBy();
			m_saCommand.Param(4).setAsLongChar()		= rec.getTemplateFile();
			m_saCommand.Param(5).setAsLongChar()		= rec.getTemplateNotes();

			m_saCommand.Param(6).setAsShort()		= rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMTemplateDB::delTemplate(CTransaction_template &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (templateExist(rec))
		{

			sSQL.Format(_T("delete from %s where tmpl_id=:1"),TBL_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}



BOOL CUMTemplateDB::getPricelists(vecTransactionPricelist &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where type_of>0"),TBL_PRICELISTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_pricelist(m_saCommand.Field(1).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																		m_saCommand.Field(3).asShort(),
																	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMTemplateDB::getPricelist(int id,CTransaction_pricelist &rec)
{

	CString sSQL;
	try
	{
		
		sSQL.Format(_T("select * from %s where id=:1"),TBL_PRICELISTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort() = id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			rec = CTransaction_pricelist(m_saCommand.Field(1).asShort(),
														  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	m_saCommand.Field(3).asShort(),
																 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
														  	 (LPCTSTR)m_saCommand.Field(5).asString(),
															   (LPCTSTR)convertSADateTime(saDateTime));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMTemplateDB::setPricelist(LPCTSTR name,int type,LPCTSTR xml)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (name,type_of,pricelist,created_by) values(:1,:2,:3,:4)"),
									TBL_PRICELISTS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= name;
		m_saCommand.Param(2).setAsShort()		= type;
		m_saCommand.Param(3).setAsLongChar()		= xml;
		m_saCommand.Param(4).setAsString()	= getUserName();
		m_saCommand.Prepare();
		m_saCommand.Execute();
		m_saConnection.Commit();
		bReturn = TRUE;
	}	
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMTemplateDB::getCostTmpls(vecTransaction_costtempl &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where cost_type_of=:1 or cost_type_of=:2"),
							TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= COST_TYPE_1;
		m_saCommand.Param(2).setAsShort()		= COST_TYPE_2;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMTemplateDB::getCostTmpl(int id,CTransaction_costtempl &rec)
{

	CString sSQL;
	try
	{
	
		sSQL.Format(_T("select * from %s where (cost_type_of=:1 or cost_type_of=:2) and cost_id=:3"),
							TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= COST_TYPE_1;
		m_saCommand.Param(2).setAsShort()		= COST_TYPE_2;
		m_saCommand.Param(3).setAsShort()		= id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			rec = CTransaction_costtempl(m_saCommand.Field(1).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMTemplateDB::setCostTmpl(LPCTSTR name,int type,LPCTSTR xml)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (cost_name,cost_type_of,cost_template,cost_notes,created_by) values(:1,:2,:3,:4,:5)"),
									TBL_COSTS_TEMPLATE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= name;
		m_saCommand.Param(2).setAsShort()		= type;
		m_saCommand.Param(3).setAsLongChar()		= xml;
		m_saCommand.Param(4).setAsLongChar()		= _T("");
		m_saCommand.Param(5).setAsString()	= getUserName();
		m_saCommand.Prepare();
		m_saCommand.Execute();

		m_saConnection.Commit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}
