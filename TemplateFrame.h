#if !defined(__TEMPLATEFRAME_H__)
#define __TEMPLATEFRAME_H__

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CMDITemplateTraktFormFrame

class CMDITemplateTraktFormFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDITemplateTraktFormFrame)

//private:
	BOOL m_bAlreadySaidOkToClose;
	BOOL m_bEnableExportTBtn;
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	CXTResizeGroupBox m_wndGeneralInfoGroup;

	CString m_sLangFN;

	CString m_sMsgCapOrig;
	CString m_sMsgCap;
	CString m_sMsgDataMissing;
	CString m_sMsgDataMissing1;
	CString m_sMsgDataMissing2;
	CString m_sMsgDataMissing3;
	CString m_sMsgMoveOn;

	CString m_sMsgOK;
	CString m_sMsgCancel;
	CString m_sMsgInpDlgCap;

	CString m_sMsgPricelistInArcive1;
	CString m_sMsgPricelistInArcive2;
	CString m_sMsgPricelistNewName;
	CString m_sMsgCostTemplInArcive1;
	CString m_sMsgCostTemplInArcive2;
	CString m_sMsgCostTemplNewName;
	CString m_sMsgTemplInArcive1;
	CString m_sMsgTemplInArcive2;
	CString m_sMsgTemplNewName;

	CString m_sMsgErrorPricelist;
	CString m_sMsgErrorCosts;
	CString m_sMsgErrorTemplate;
	CString m_sMsgCapImport;

	void setLanguage(void);

	BOOL m_bFirstOpen;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL isOkToClose(void);

	HICON m_hIcon;
public:
	CMDITemplateTraktFormFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CXTPTabClientWnd m_MTIClientWnd;

	void setEnableTBtns(BOOL enable)
	{
		m_bEnableExportTBtn = enable;
	}

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CMDITemplateTraktFormFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CMDITemplateTraktFormFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
//	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	afx_msg void OnImportStandTemplate(void);
	afx_msg void OnExportStandTemplate(void);
	afx_msg void OnUpdateExportTBtn(CCmdUI* pCmdUI);

	//}}AFX_MSG
DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDITemplateListFrame frame

class CMDITemplateListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDITemplateListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDITemplateListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDITemplateListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif